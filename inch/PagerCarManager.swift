//
//  PagerCarManager.swift
//  inch
//
//  Created by Ahmad Almasri on 1/9/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import Foundation
extension PagerCarController:HttpHelparDelegete{
    
    
    func AddCar(){
        
        let   JSONString = NSString.localizedStringWithFormat("{\"oUserCarDetails\":{\"user_id\":\"%@\",\"car_color\":\"%@\",\"plate_number\":\"%@\",\"year_of_creation\":\"%@\",\"fuel_type_id\":\"%@\",\"car_type_id\":\"%@\",\"car_details_id\":\"%@\",\"Car_VIN\":\"%@\"}}"
            
            ,"\(DBMethod.Shared.GetAll(ofType: UserModel.self).first!.iD)",self.color,plate_number_field.text!,self.year,self.FuelType,self.type,self.category,carVinTextField.text!)
        
        
        Http.PostWithString(Url: UrlHelpar.ADD_CAR, JSONString: JSONString, Tag: 1)
        
    }
    func convertToDictionary(text: Data) -> NSDictionary? {
        do {
            return try JSONSerialization.jsonObject(with: text, options: []) as? NSDictionary as! [String : Any]? as NSDictionary?
        } catch {
            print(error.localizedDescription)
        }
        
        return nil
    }
    
    func receivedErrorWithMessage(message: String) {
        Helpar.Logar(msg: message)
        self.next_button.dismissLoader()
    }
    
    func receivedResponse(dictResponse: NSDictionary, Status: Bool, Tag: Int) {
        Helpar.Logar(msg: "\(dictResponse)")
        self.next_button.dismissLoader()

        if Tag == 1 {
            
            if Status {
                SharedDataHelpar.Shared.SetIsAdd(IsAdd: true)
                
                if isMyCar {
                    Helpar.Shared.ModelView(storyboard: StoryboardName.MyCar, Context: self, Identifier: ScreenName.MyCar)

                   
                    return
                }
            Helpar.Shared.ModelView(storyboard: StoryboardName.CarList, Context: self, Identifier: ScreenName.CarList)
                
            }else{
                
                Helpar.Shared.Alert(Title: "vin exist", Message: nil, BtnOk: Translation.Shared.GetVal(Key: "ok"), Context: self, Actions: { 
                    
                })
                
            }
        }
        
    }
    
    
}

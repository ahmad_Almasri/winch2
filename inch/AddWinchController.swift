//
//  PagerCarController.swift
//  inch
//
//  Created by Ahmad Almasri on 1/1/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import UIKit
import PulsingHalo
import DropDown
import RealmSwift
import DateToolsSwift
import ElValidator
import SWRevealViewController

struct ImageUpload{
    var image = UIImage()
    var type  = 0
    
}

class AddWinchController: UIViewController ,UITextFieldDelegate,UICollectionViewDataSource,HWViewPagerDelegate,UICollectionViewDelegateFlowLayout,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
   
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var viewPager:UICollectionView!
    @IBOutlet weak var winch_type_field:UIButton!
    @IBOutlet weak var next_button:UIButton!
  //  @IBOutlet weak var add_button:UIButton!
   // @IBOutlet weak var add_car_label:UILabel!
    @IBOutlet weak var platNumberTextField: TextFieldValidator!
    
    
    
    var validationBlock:((_: [Error]) -> Void)?
    var activeTextField: TextFieldValidator?
    

    
    var halo:PulsingHaloLayer?
    private var animationsCount = 0
    private var collectionViewLayout: LGHorizontalLinearFlowLayout!
    let fuel_type_DropDown = DropDown()

    var type = ""
    var category = ""
    var color = ""
    var year =  "\(Date().year)"
    var imgPager = [UIImage]()
    var titlePager = [String]()
   // var FuelTypeList = [String]()
    var winchTypeList =  [LookupModel]()
    var imageUploadList = [ImageUpload]()
    var winchType = ""
    var typeImage = 0
    var imageIds = [String]()
    let colorName = DBColorNames()

    var Http = HttpHelpar()
    
    
    var isMyCar = false 
    
    
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField as? TextFieldValidator
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        activeTextField?.resignFirstResponder()
        activeTextField = nil;
        
        return true
    }

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       Http.delegate = self
        
       
        
        imgPager = [#imageLiteral(resourceName: "add_winch"),#imageLiteral(resourceName: "add_winch"),#imageLiteral(resourceName: "add_linc"),#imageLiteral(resourceName: "add_incho")]
        



        
        configureCollectionView()
        
        halo = PulsingHaloLayer()
        
        winch_type_field.BottomLineView(Color: Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.ColorLine, alpha: 1), Size: 1)
        platNumberTextField.BottomLineView(Color: Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.ColorLine, alpha: 1), Size: 1)
        
        next_button.roundingUIView(cornerRadiusParam: 2)
      //  add_button.roundingUIView(cornerRadiusParam: add_button.frame.width/2)
       // self.add_button.backgroundColor = Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.PrimaryColor, alpha: 1)
        
        
        next_button.titleLabel?.font = FontHelpar.Regular(Size: 18)
        winch_type_field.titleLabel?.font = FontHelpar.Regular(Size: 14)
       
      //  add_car_label.font = FontHelpar.Regular(Size: 14)
        
        
        winchTypeList =  [LookupModel(name_en:"Heavy",id:676),LookupModel(name_en:"Small",id:677)]
        
    //    FuelTypeList = FuelTypeObjList.map{$0.fuel_type}
        setupDropDown()
        
        validationBlock = { [weak self] (errors: [Error]) -> Void in
            if let error = errors.first {
                print(error)
                self?.activeTextField?.False()
            } else {
                self?.activeTextField?.True()
            }
        }

        
        
        platNumberTextField.delegate = self
        platNumberTextField.add(validator: LenghtValidator(validationEvent: .perCharacter, min: 6, max: 20))
        platNumberTextField.validationBlock = validationBlock
        
        customSetup()
    }
    
    
    
    
    func setupDropDown(){
        fuel_type_DropDown.anchorView =  winch_type_field
        fuel_type_DropDown.bottomOffset = CGPoint(x: 0, y: winch_type_field.bounds.height)
        // You can also use localizationKeysDataSource instead. Check the docs.
        fuel_type_DropDown.dataSource = winchTypeList.map({$0.name_en})
        // Action triggered on selection
        fuel_type_DropDown.selectionAction = { [unowned self] (index, item) in
            self.winch_type_field.setTitle(item, for: .normal)
            self.winchType = "\(self.winchTypeList[index].id)"
            
            self.winch_type_field.setTitleColor(UIColor.black, for: .normal)
        }
        
        
    }
    @IBAction func saveTapped(_ sender:UIButton){
        
        if imageUploadList.count < 4 {
            
            
            let when = DispatchTime.now() + 0
            DispatchQueue.main.asyncAfter(deadline: when) {
                
                self.viewPager.shake()
                
            }
            return
        }
        
        
        if winchType.isEmpty{
            
            winch_type_field.shake()
            
            return
        }
        
        
        if !platNumberTextField.isValid(){
            
            platNumberTextField.shake()
            
            return
        }
        sender.ShowLoaderSmall(Color: sender.backgroundColor)

        var i = 1
        imageUploadList.forEach { (obj) in
          
            Http.Upload(Url: UrlHelpar.UPLOAD_IMAGE, data: UIImageJPEGRepresentation(obj.image, 0)!  , id: obj.type) { (result) in
                
                print(result)
                if  let result = result as? NSDictionary{
                let result1 = result.object(forKey: "result") as? String
                let result2 = self.Http.convertToDictionary(text: result1!)
                let result3 = result2?.object(forKey: "result") as! NSArray
                
                print(result3)
                
                if result3.count > 0 {
                    self.imageIds.append(result3[0] as! String)
                }
                }
                if i == self.imageUploadList.count{
                    
                    self.save()
                }
                 i = i + 1
                
            }
            
        }
      
        

    }
    
    func save(){
        
        if self.imageIds.count > 3 {
        let   JSONString = NSString.localizedStringWithFormat("{\"oDetails\":{\"front_image\":\"%@\",\"insurance_image\":\"%@\",\"left_image\":\"%@\",\"license_image\":\"%@\",\"plate_image\":\"%@\",\"rear_image\":\"%@\",\"right_image\":\"%@\",\"status_id\":\"%@\",\"type_id\":\"%@\",\"user_id\":\"%@\",\"plate_number\":\"%@\"}}"
            
            ,"\(self.imageIds[0])","\(self.imageIds[2])","0","\(self.imageIds[3])","0","\(self.imageIds[1])","0","673","\(self.winchType)","\( DBMethod.Shared.GetAll(ofType: UserModel.self).first!.iD)",platNumberTextField.text!)
        
        
        Http.PostWithString(Url: UrlHelpar.ADD_WINCH, JSONString: JSONString, Tag: 1)
        }else{
            
            Helpar.Shared.Alert(Title: "please try again", Message: nil, BtnOk: Translation.Shared.GetVal(Key: "ok"), Context: self, Actions: {
                self.next_button.dismissLoader()
                
            })
        }
    }
    @IBAction func changeWinchType(_ sender: UIButton) {
        fuel_type_DropDown.show()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        let bar:UINavigationBar! =  self.navigationController?.navigationBar
        
        bar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        bar.shadowImage = UIImage()
        bar.backgroundColor = UIColor(red: 0.0, green: 0.3, blue: 0.5, alpha: 0.0)
        bar.tintColor = Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.PrimaryColor, alpha: 1)
        navigationItem.backBarButtonItem?.title = ""
        
        
        //  add_button.superview?.layer.insertSublayer(halo!, below: add_button.layer)
        //  halo?.start()
        
        winch_type_field.setTitle(Translation.Shared.GetVal(Key: "winchtype"), for: .normal)
     //   add_car_label.text = Translation.Shared.GetVal(Key: "addnewcar")
        next_button.setTitle(Translation.Shared.GetVal(Key: "next"), for: .normal)
        platNumberTextField.placeholder  = Translation.Shared.GetVal(Key: "platenumber")
        titlePager = [Translation.Shared.GetVal(Key: "winchimage"),Translation.Shared.GetVal(Key: "winchimage"),Translation.Shared.GetVal(Key: "winchlicense"),Translation.Shared.GetVal(Key: "winchinsurance")]

        
        /*   Choose the car type 17 regular
         
         Fuel type 14 regular
         Plate number 14 regular
         
         add new car 14 regular*/
    }
    
    
    func customSetup() {
        let revealViewController: SWRevealViewController? = self.revealViewController()
        if revealViewController != nil {
            menuButton.target = self.revealViewController()
            revealViewController?.rightViewRevealWidth = (UIScreen.main.bounds.width-40)
            revealViewController?.frontViewShadowOffset = CGSize(width: 0, height: 0) //CGSizeMake(0, 0);
            revealViewController?.frontViewShadowOpacity = 0.0
            revealViewController?.frontViewShadowRadius = 0.0
            Config.MenuIsEnable = SharedDataHelpar.Shared.GetIsPay()

            
            menuButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            //  navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    
    @IBAction func NextTapped(_ sender:UIButton){
        
      
        sender.ShowLoaderSmall(Color: sender.backgroundColor)
       //  AddCar()
      
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
       // self.halo?.position = self.add_button.center
        self.halo?.haloLayerNumber = 6
        self.halo?.radius=80
        self.halo?.animationDuration=5.0
        self.halo?.backgroundColor = Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.PrimaryColor, alpha: 1).cgColor
    }
    
    
    
    private func configureCollectionView() {
        self.collectionViewLayout = LGHorizontalLinearFlowLayout.init(configuredWith: self.viewPager, itemSize: CGSize(width: 270, height: 270), minimumLineSpacing: 0, size:250)
        
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.isDragging || collectionView.isDecelerating || collectionView.isTracking {
            return
        }
        if indexPath.row == 0 {
         
            typeImage  = 686
        }else if indexPath.row == 1 {
            
            typeImage  = 687

        }else if indexPath.row == 2 {
            
          typeImage =   683
        }else {
            typeImage = 682
        }
        
        let sheet = UIAlertController(title: Translation.Shared.GetVal(Key: "selectphoto"), message: nil, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: Translation.Shared.GetVal(Key: "camera"), style: .default)
        {(alert) in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: nil)
            }
            
        }
        
        let GalleryAction = UIAlertAction(title: Translation.Shared.GetVal(Key: "gallery"), style: .default)
        {
            (alert) in
            
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                let imagePicker = UIImagePickerController()
               imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                // imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: nil)
            }
            
            
        }
        
        let cancelAction = UIAlertAction(title: Translation.Shared.GetVal(Key: "cancel"), style: .cancel)
        {
            (alert) in
        }
        
        sheet.addAction(GalleryAction)
        sheet.addAction(cameraAction)
        sheet.addAction(cancelAction)
        
        
        self.present(sheet, animated: true, completion: nil)

    }
    

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
          let img = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        if let index = imageUploadList.map({$0.type}).index(of: self.typeImage){
            
            imageUploadList.remove(at: index)
            imageUploadList.append(ImageUpload(image:img,type:self.typeImage))
        }else{
            imageUploadList.append(ImageUpload(image:img,type:self.typeImage))

        }
        
        picker.dismiss(animated: true , completion: nil)

    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true , completion: nil)
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titlePager.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PagerCell", for: indexPath) as! PagerCell
        
        cell.icon_image.image = imgPager[indexPath.row]
        cell.title_label.text = titlePager[indexPath.row].uppercased()
         cell.icon_view.backgroundColor = Helpar.Shared.hexStringToUIColor(hex:  ColorHelpar.NavTitleColorDark, alpha: 1)
//        if indexPath.row == 1 {
//            
//            if !self.color.isEmpty{
//                
//              cell.title_label.text = colorName.name(for: Helpar.Shared.hexStringToUIColor(hex: self.color, alpha: 1))
//                
//            cell.icon_view.backgroundColor = Helpar.Shared.hexStringToUIColor(hex:  self.color, alpha: 1)
//                
//            }
//        }
//        
//        if indexPath.row == 2 {
//            
//            cell.icon_image.image = generateImageWithText(text:self.year)
//        }
        
        return cell
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 270, height: 270)
    }
    func pagerDidSelectedPage(_ selectedPage: Int) {
        print(selectedPage)
    }
    
    func generateImageWithText(text: String) -> UIImage
    {
        let image = UIImage(named: "cardate")!
        
        let imageView = UIImageView(image: image)
        imageView.backgroundColor = UIColor.clear
        imageView.frame = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.width) //CGRect(0, 0, image.size.width, image.size.height)
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height))
        label.font = FontHelpar.Black(Size: 35)
        label.backgroundColor = UIColor.clear
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.text = text
        
        UIGraphicsBeginImageContextWithOptions(label.bounds.size, false, 0);
        imageView.layer.render(in: UIGraphicsGetCurrentContext()!)
        label.layer.render(in: UIGraphicsGetCurrentContext()!)
        let imageWithText = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        
        return imageWithText!
    }
}



extension AddWinchController:HttpHelparDelegete{
    
    func receivedErrorWithMessage(message: String) {
        
    }
    
    func receivedResponse(dictResponse: NSDictionary, Status: Bool, Tag: Int) {
        self.next_button.dismissLoader()
        delayWithSeconds(0.5) {
            self.navigationController?.popViewController(animated: false)
            
        }
        print(dictResponse)
    }
}

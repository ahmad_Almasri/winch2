//
//  UserCarModel.swift
//  inch
//
//  Created by Ahmad Almasri on 1/10/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import Foundation

class CarListModel:NSObject{
    
    
    var user_id	 = 0
    var car_color	= ""
    var plate_number = ""
    var year_of_creation = 0
    var  fuel_type_id = 0
    var car_type_id	= 0
    var car_details_id	= 0
    var user_car_id	= 0
    var fuel_type	= ""
    var make       	 = ""
    var model	 = ""
    var membership_expiry_string = ""
    var isSelected = false
    var is_paid = false
    var car_service_fees = 150
    var is_active = false

    
    override init() {
        super.init()
    }
    
    init(JSON:String){
        super.init()
        Helpar.Shared.Parser(JSONString: JSON, Obj: self)
    }
    
    
}



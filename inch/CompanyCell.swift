//
//  CompanyCell.swift
//  inch
//
//  Created by Macbook Pro on 10/14/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import Foundation
import UIKit
class CompanyCell:UITableViewCell{
    
    @IBOutlet weak var deleteLabel: UILabel!
    @IBOutlet weak var platNumberLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var winchImageLabel: UILabel!
    @IBOutlet weak var linceusImageLabel: UILabel!
    @IBOutlet weak var insuranceImageLabel: UILabel!
}

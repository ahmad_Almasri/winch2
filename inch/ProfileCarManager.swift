//
//  ChooseCarManager.swift
//  inch
//
//  Created by Ahmad Almasri on 3/25/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import Foundation
import CryptoSwift
import CocoaSecurity
extension ProfileController:HttpHelparDelegete{
    
    
    
    
    func getProfile(UserId:Int){
        
        let params = ["id":UserId]
        Http.Get(Url: UrlHelpar.GET_PROFILE, parameters: params, Tag: 2)

    }
    
    func UpdateProfile(UserId:Int){

        let   JSONString = NSString.localizedStringWithFormat("{\"nUserID\":\"%@\",\"oDetails\":{\"id\":\"%@\",\"name_en\":\"%@\",\"username\":\"%@\",\"email\":\"%@\",\"password\":\"%@\",\"address\":\"%@\",\"phone\":\"%@\",\"name_ar\":\"%@\",\"cnt_id\":\"%@\",\"new_password\":\"%@\"}}"
            
            ,"\(UserId)","\(UserId)",nameTextField.text!,nameTextField.text!,emailTextField.text!,User.password,"",phoneTextFiled.text!,nameTextField.text!,"\(self.CuntryId)",passwordTextField.text!.md5())
        

        Http.PostWithString(Url: UrlHelpar.UPDATE_PROFILE, JSONString: JSONString, Tag: 3)
        
    }
    
    func FindCarList(UserId:Int){
        let Param = ["user_id":UserId]
        Http.Get(Url: UrlHelpar.GET_CAR, parameters: Param, Tag: 1)
    }
    func receivedErrorWithMessage(message: String) {
        self.view.dismissLoader()
        Helpar.Logar(msg: message)
    }
    func receivedResponse(dictResponse: NSDictionary, Status: Bool, Tag: Int) {
        
        Helpar.Logar(msg: "\(Helpar.Shared.DictToJson(JSON: dictResponse))")
        
        if Tag == 2 {
            
            if let result =  dictResponse.object(forKey: "result") as? NSArray{
                
                
                if result.count > 0 {
                    
                    
                    DBMethod.Shared.Delete(obj: DBMethod.Shared.GetAll(ofType: UserModel.self))
                    
                }
                
                
                for i in 0..<result.count{
                    
                    let data = result[i] as! NSDictionary
                    let JSON = Helpar.Shared.DictToJson(JSON: data)
                    
                     User = UserModel(JSON: JSON as String)
                    
                    if User.user_group_id == 3 {
                        
                        car_table_view.isHidden = true
                        cartitle.isHidden = true
                    }
                    
                    if let Id = data.object(forKey: "id") as? Int {
                        
                     let r =    DBMethod.Shared.update(ofType: User, value: Id as AnyObject, key: "iD")
                    }
                    
                    
                    DBMethod.Shared.Delete(obj: DBMethod.Shared.GetAll(ofType: UserModel.self))
                    
                    DBMethod.Shared.save(obj: User)
                    
                      countryButton.setTitle(User.Country, for: .normal)
                    if User.Country != "" {
                        
                        countryButton.setTitleColor(UIColor.black, for: .normal)
                    }
                     nameTextField.text = User.username
                    emailTextField.text = User.email
                    

                    phoneTextFiled.text = User.phone
                    self.CuntryId = User.cnt_id
                    nationalIdTextField.text = User.national_id
                    
                    print("User.username",User.username)
                    print("User.email",User.email)
                    print("User.phon",User.phone)

                    
                
                    passwordTextField.text =   User.password.md5().isEmpty ? "" : "1234567"
                    

                    
                    break
                    
                }
                
                
                
            }

            
            return
        }
        
        
        if Tag == 1 {
            self.view.dismissLoader()

            if Status{
                
                
                if let result =  dictResponse.object(forKey: "result") as? NSArray{
                    
                    for i in 0..<result.count{
                        let JSON = Helpar.Shared.DictToJson(JSON: result[i] as! NSDictionary)
                        
                        let User = CarListModel(JSON: JSON as String)
                        
                        if User.is_paid{
                        UserCarList.append(User)
                        }
                        
                    }
                    
                   
                    car_table_view.reloadData()
                    
                }
                
            }
            
            
        }
                
    }
}

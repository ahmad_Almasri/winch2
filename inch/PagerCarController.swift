//
//  PagerCarController.swift
//  inch
//
//  Created by Ahmad Almasri on 1/1/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import UIKit
import PulsingHalo
import DropDown
import RealmSwift
import DateToolsSwift
import ElValidator
import SDWebImage
import SWRevealViewController
class PagerCarController: UIViewController ,UITextFieldDelegate,UICollectionViewDataSource,HWViewPagerDelegate,UICollectionViewDelegateFlowLayout{
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var viewPager:UICollectionView!
    @IBOutlet weak var fuel_type_field:UIButton!
    @IBOutlet weak var plate_number_field:TextFieldValidator!
    @IBOutlet weak var next_button:UIButton!
  //  @IBOutlet weak var add_button:UIButton!
   // @IBOutlet weak var add_car_label:UILabel!
    @IBOutlet weak var carVinTextField: TextFieldValidator!
    
    
    
    var validationBlock:((_: [Error]) -> Void)?
    var activeTextField: TextFieldValidator?
    

    
    var halo:PulsingHaloLayer?
    private var animationsCount = 0
    private var collectionViewLayout: LGHorizontalLinearFlowLayout!
    let fuel_type_DropDown = DropDown()

    var type = ""
    var category = ""
    var color = ""
    var year =  "\(Date().year)"
    var imgPager = [UIImage]()
    var titlePager = [String]()
    var FuelTypeList = [String]()
    var FuelTypeObjList =  [FuelModel]()
    var url  = ""
    var FuelType = ""
    
    let colorName = DBColorNames()

    var Http = HttpHelpar()
    
    
    var isMyCar = false 
    
    
    func GetCarType(notification:Notification){
        
        if let color = notification.userInfo?["color"] as? String{
            self.color = color
            viewPager.reloadData()

        }
        
        if let type = notification.userInfo?["type"] as? String{
           self.type = type
        }
        
        if let category = notification.userInfo?["category"] as? String{
            self.category = category
            titlePager[0] = "\(DBMethod.Shared.GetById(ofType: MakeModel.self, predicate: "car_details_id == \(self.category)").first!.model)"
            
            let make =   "\(DBMethod.Shared.GetById(ofType: MakeModel.self, predicate: "car_details_id == \(self.category)").first!.make)"
             url = UrlHelpar.IMAGES+make+".png"
              url = url.addingPercentEscapes(using: String.Encoding.utf8)!
            
             titlePager[0]  =  titlePager[0]  + " , " + make
            viewPager.reloadData()
        
          
        }
        
        if let year = notification.userInfo?["year"] as? String{
            
          self.year = year
            viewPager.reloadData()

        }
    }
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField as? TextFieldValidator
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        activeTextField?.resignFirstResponder()
        activeTextField = nil;
        
        return true
    }

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Http.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.GetCarType(notification:)), name:NSNotification.Name(rawValue: "GetCarType"), object: nil)
        
        imgPager = [#imageLiteral(resourceName: "chooesCar"),#imageLiteral(resourceName: "SelectColor"),#imageLiteral(resourceName: "cardate")]
        



        
        configureCollectionView()
        
        halo = PulsingHaloLayer()
        
        fuel_type_field.BottomLineView(Color: Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.ColorLine, alpha: 1), Size: 1)
        plate_number_field.BottomLineView(Color: Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.ColorLine, alpha: 1), Size: 1)
        carVinTextField.BottomLineView(Color: Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.ColorLine, alpha: 1), Size: 1)
        
        next_button.roundingUIView(cornerRadiusParam: 2)
      //  add_button.roundingUIView(cornerRadiusParam: add_button.frame.width/2)
       // self.add_button.backgroundColor = Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.PrimaryColor, alpha: 1)
        
        
        next_button.titleLabel?.font = FontHelpar.Regular(Size: 18)
        fuel_type_field.titleLabel?.font = FontHelpar.Regular(Size: 14)
        plate_number_field.font = FontHelpar.Regular(Size: 14)
      //  add_car_label.font = FontHelpar.Regular(Size: 14)
        
        
        FuelTypeObjList =  Array(DBMethod.Shared.GetAll(ofType: FuelModel.self))
        
        FuelTypeList = FuelTypeObjList.map{$0.fuel_type}
        setupDropDown()
        
        validationBlock = { [weak self] (errors: [Error]) -> Void in
            if let error = errors.first {
                print(error)
                self?.activeTextField?.False()
            } else {
                self?.activeTextField?.True()
            }
        }

        
        plate_number_field.delegate = self
//        plate_number_field.add(validator: LenghtValidator(validationEvent: .perCharacter, min: 4, max: 15))
        plate_number_field.add(validator: PatternValidator(customPattern: "^[a-zA-Z0-9~@#$%^&:,\"]{4,15}$"))

        plate_number_field.validationBlock = validationBlock
        
        
        
        carVinTextField.delegate = self
//        carVinTextField.add(validator: LenghtValidator(validationEvent: .perCharacter, min: 6, max: 20))
        carVinTextField.add(validator: PatternValidator(customPattern: "^[a-zA-Z0-9~@#$%^&:,\"]{6,20}$"))

        carVinTextField.validationBlock = validationBlock
        
        customSetup()
    }
    
    
    
    
    func setupDropDown(){
        fuel_type_DropDown.anchorView =  fuel_type_field
        fuel_type_DropDown.bottomOffset = CGPoint(x: 0, y: fuel_type_field.bounds.height)
        // You can also use localizationKeysDataSource instead. Check the docs.
        fuel_type_DropDown.dataSource = FuelTypeList
        // Action triggered on selection
        fuel_type_DropDown.selectionAction = { [unowned self] (index, item) in
            self.fuel_type_field.setTitle(item, for: .normal)
            self.FuelType = "\(self.FuelTypeObjList[index].id)"
            
            self.fuel_type_field.setTitleColor(UIColor.black, for: .normal)
        }
        
        
    }
    
    @IBAction func changeFuelType(_ sender: UIButton) {
        fuel_type_DropDown.show()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        let bar:UINavigationBar! =  self.navigationController?.navigationBar
        
        bar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        bar.shadowImage = UIImage()
        bar.backgroundColor = UIColor(red: 0.0, green: 0.3, blue: 0.5, alpha: 0.0)
        bar.tintColor = Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.PrimaryColor, alpha: 1)
        navigationItem.backBarButtonItem?.title = ""
        
        
        //  add_button.superview?.layer.insertSublayer(halo!, below: add_button.layer)
        //  halo?.start()
        
        fuel_type_field.setTitle(Translation.Shared.GetVal(Key: "fueltype"), for: .normal)
        plate_number_field.placeholder = Translation.Shared.GetVal(Key: "platenumber")
     //   add_car_label.text = Translation.Shared.GetVal(Key: "addnewcar")
        next_button.setTitle(Translation.Shared.GetVal(Key: "next"), for: .normal)
        carVinTextField.placeholder  = Translation.Shared.GetVal(Key: "carvin")
        titlePager = [Translation.Shared.GetVal(Key: "choosethecartype"),Translation.Shared.GetVal(Key: "choosethecolor"),Translation.Shared.GetVal(Key: "yearofcreation")]

        
        /*   Choose the car type 17 regular
         
         Fuel type 14 regular
         Plate number 14 regular
         
         add new car 14 regular*/
    }
    
    
    func customSetup() {
        let revealViewController: SWRevealViewController? = self.revealViewController()
        if revealViewController != nil {
            menuButton.target = self.revealViewController()
            revealViewController?.rightViewRevealWidth = (UIScreen.main.bounds.width-40)
            revealViewController?.frontViewShadowOffset = CGSize(width: 0, height: 0) //CGSizeMake(0, 0);
            revealViewController?.frontViewShadowOpacity = 0.0
            revealViewController?.frontViewShadowRadius = 0.0
            Config.MenuIsEnable = SharedDataHelpar.Shared.GetIsPay()

            
            menuButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            //  navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    
    @IBAction func NextTapped(_ sender:UIButton){
        
        if self.color == "" || self.category == "" {

         
            let when = DispatchTime.now() + 0
            DispatchQueue.main.asyncAfter(deadline: when) {
                
                 self.viewPager.shake()
                
            }
                return
        }
        
        
        if FuelType.isEmpty {
          
            fuel_type_field.shake()
            
            return
        }
        
        if !plate_number_field.isValid() || plate_number_field.isSqlInject() {
            
            plate_number_field.shake()
            
            return
        }
        
        if !carVinTextField.isValid()  || carVinTextField.isSqlInject(){
            
            carVinTextField.shake()
            
            return
          }
        sender.ShowLoaderSmall(Color: sender.backgroundColor)
         AddCar()
      
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
       // self.halo?.position = self.add_button.center
        self.halo?.haloLayerNumber = 6
        self.halo?.radius=80
        self.halo?.animationDuration=5.0
        self.halo?.backgroundColor = Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.PrimaryColor, alpha: 1).cgColor
    }
    
    
    
    private func configureCollectionView() {
        self.collectionViewLayout = LGHorizontalLinearFlowLayout.init(configuredWith: self.viewPager, itemSize: CGSize(width: 270, height: 270), minimumLineSpacing: 0, size:250)
        
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.isDragging || collectionView.isDecelerating || collectionView.isTracking {
            return
        }
        
        
        
        switch(indexPath.row)  {
            
        case 0 :
            CellItemTapped(Screen: ScreenName.addCarType)
            break
            
        case 1:
            CellItemTapped(Screen: ScreenName.SelectColor)
            break
            
        case 2 :
            
            CellItemTapped(Screen: ScreenName.SelecteYear)

            break
        default:
            
            print("")
        }
    }
    
    
    
    func CellItemTapped(Screen:ScreenName){
        
        let storyboard = UIStoryboard.init(name: StoryboardName.Car.rawValue, bundle: nil)
        let next = storyboard.instantiateViewController(withIdentifier: Screen.rawValue)
        next.modalPresentationStyle=UIModalPresentationStyle.overFullScreen
        

        self.present(next, animated: true, completion: nil)
    }
    
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PagerCell", for: indexPath) as! PagerCell
        
        cell.icon_image.image = imgPager[indexPath.row]
        cell.title_label.text = titlePager[indexPath.row].uppercased()
         cell.icon_view.backgroundColor = Helpar.Shared.hexStringToUIColor(hex:  ColorHelpar.NavTitleColorDark, alpha: 1)
        if indexPath.row == 0 {
            
            
            if !url.isEmpty {
                  cell.icon_image.contentMode = .center
                cell.icon_image.sd_setImage(with: URL(string:url), completed: { (image, error, cache, url) in
                    if image != nil {
                    let image = self.resizeImage(image: image!, targetSize: CGSize(width: 100, height: 100))
                     cell.icon_image.image = image
                    }
                })
            }else{
                
                cell.icon_image.image = imgPager[indexPath.row]
            }
            
        }
        
        if indexPath.row == 1 {
            
            if !self.color.isEmpty{
                
              cell.title_label.text = colorName.name(for: Helpar.Shared.hexStringToUIColor(hex: self.color, alpha: 1))
                
            cell.icon_view.backgroundColor = Helpar.Shared.hexStringToUIColor(hex:  self.color, alpha: 1)
              
            }
        }
        
        if indexPath.row == 2 {
            
            cell.icon_image.image = generateImageWithText(text:self.year)
        }
        
        return cell
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 270, height: 270)
    }
    func pagerDidSelectedPage(_ selectedPage: Int) {
        print(selectedPage)
    }
    
    func generateImageWithText(text: String) -> UIImage
    {
        let image = UIImage(named: "cardate")!
        
        let imageView = UIImageView(image: image)
        imageView.backgroundColor = UIColor.clear
        imageView.frame = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.width) //CGRect(0, 0, image.size.width, image.size.height)
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height))
        label.font = FontHelpar.Black(Size: 35)
        label.backgroundColor = UIColor.clear
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.text = text
        
        UIGraphicsBeginImageContextWithOptions(label.bounds.size, false, 0);
        imageView.layer.render(in: UIGraphicsGetCurrentContext()!)
        label.layer.render(in: UIGraphicsGetCurrentContext()!)
        let imageWithText = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        
        return imageWithText!
    }
}

//
//  SyncDataModel.swift
//  inch
//
//  Created by alaa  on 12/18/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import UIKit
import FirebaseDatabase

protocol FirebaseHelparDelegete {
    func receivedResponseFromFirebase(dictResponse:NSDictionary,Status:Bool,Tag:Int)
    func receivedDriverData(dictResponse:NSDictionary)

}
class SyncDataModel: NSObject {
    
    var delegate: FirebaseHelparDelegete?
    var ah = 0
    
    func readAllDataFromFirebase() {
        
        let userRef = Database.database().reference().child("drivers")
        userRef.observe(.value, with: { (snapshot) in
            
            let value = snapshot.value as? NSDictionary
            userRef .removeAllObservers()
            userRef .keepSynced(true)
            self.delegate?.receivedResponseFromFirebase(dictResponse: value! ,Status:  true , Tag: 1)

        })
   
}

    func writeDataToFirebase(updatedValue:String,dataDic:NSMutableDictionary,requestID:Any){
        var ref: DatabaseReference!
        ref = Database.database().reference()
        
        let childUpdates = ["/\(updatedValue)/\(requestID)": dataDic]
        
        
        ref.updateChildValues(childUpdates)
        
    }
    
    
    func listenerForDriver(driverID:String,listenerStatus:Bool) {
        
        if(listenerStatus){
        let userRef = Database.database().reference().child("drivers").child(driverID)
        userRef.observe(.value, with: { (snapshot) in
            
            let value = snapshot.value as? NSDictionary
            self.delegate?.receivedDriverData(dictResponse: value!)
            
        })
        }else{
            let userRef = Database.database().reference().child("drivers").child(driverID)
            userRef.observe(.value, with: { (snapshot) in
                
                userRef .removeAllObservers()
                userRef .keepSynced(true)

            })
            
        }
        
    }







    
}

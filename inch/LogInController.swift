//
//  ViewController.swift
//  inch
//
//  Created by Ahmad Almasri on 12/29/16.
//  Copyright © 2016 Snapics. All rights reserved.
//

import UIKit
import RealmSwift
import ElValidator
import FirebaseDatabase

class LogInController: UIViewController,UITextFieldDelegate {
    
    // MARK: @IBOutlet UI Component
    @IBOutlet weak var user_name_faild:TextFieldValidator!
    @IBOutlet weak var password_faild:TextFieldValidator!
    @IBOutlet weak var sign_in_button:UIButton!
    @IBOutlet weak var forget_password_button:UIButton!
    @IBOutlet weak var sign_up_msg_label:UILabel!
    @IBOutlet weak var sign_up_button:UIButton!
    
    /// new **Object** user login insert in to database
    var  User = UserModel()

    
    /// create validationBlock return valid or not input
    var validationBlock:((_: [Error]) -> Void)?
    /// create gloabel text valedation
    var activeTextField: TextFieldValidator?

    /// **Object** from http helper using call get or post ..etc
    let Http = HttpHelpar()
    
 
    override func viewDidLoad() {
        super.viewDidLoad()
        Http.delegate = self

        user_name_faild.BottomLineView(Color: Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.ColorLineBg, alpha: 1), Size: 1)
        password_faild.BottomLineView(Color: Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.ColorLineBg, alpha: 1), Size: 1)
        sign_in_button.roundingUIView(cornerRadiusParam: 5)
        
        
        user_name_faild.font = FontHelpar.Regular(Size: 14)
        password_faild.font = FontHelpar.Regular(Size: 14)
        sign_in_button.titleLabel?.font = FontHelpar.Bold(Size: 17)
        forget_password_button.titleLabel?.font = FontHelpar.Regular(Size: 14)
        sign_up_msg_label.font = FontHelpar.Regular(Size: 14)
        sign_up_button.titleLabel?.font = FontHelpar.Semibold(Size: 14)
        
//        user_name_faild.text = "r@test.com"
        user_name_faild.text = "test1010@mailinator.com"
        password_faild.text = "123456789"
//                user_name_faild.text = "beboo@mailinator.com"
//                password_faild.text = "Admin123"

        
        validationBlock = { [weak self] (errors: [Error]) -> Void in
            if let error = errors.first {
                print(error)
                self?.activeTextField?.False()
            } else {
                self?.activeTextField?.True()
            }
        }
       

         user_name_faild.delegate = self
        user_name_faild.add(validator:PatternValidator(pattern: .mail))
        user_name_faild.validationBlock = validationBlock
        
        //user_name_faild.add(validator: PatternValidator(customPattern: "[^<>;'[\\]?!{}()--*]*$"))

        
        
        password_faild.delegate = self
        password_faild.add(validator: LenghtValidator(validationEvent: .perCharacter, min: 5, max: 20))
        password_faild.validationBlock = validationBlock
        
      //  password_faild.add(validator: PatternValidator(customPattern: ""))

        
        //let regex = try! NSRegularExpression(pattern: "[^<>;'[\\]?!{}()*])*$", options: NSRegularExpression.Options.caseInsensitive)
      //  let range = NSMakeRange(0, password_faild.text!.count)
       // let modString = regex.stringByReplacingMatches(in: password_faild.text!, options: [], range: range, withTemplate: "22")
        //print(modString)
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField as? TextFieldValidator
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        activeTextField?.resignFirstResponder()
        activeTextField = nil;
        
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        user_name_faild.placeholder = Translation.Shared.GetVal(Key: "username")
        password_faild.placeholder = Translation.Shared.GetVal(Key: "password")
        sign_in_button.setTitle(Translation.Shared.GetVal(Key: "signin").uppercased(), for: .normal)
        forget_password_button.setTitle(Translation.Shared.GetVal(Key: "forgetpassword"), for: .normal)
        sign_up_msg_label.text = Translation.Shared.GetVal(Key: "don'thaveanaccount")
        sign_up_button.setTitle(Translation.Shared.GetVal(Key: "signup"), for: .normal)
        
    }

    @IBAction func SignUpTapped(sender:UIButton){
        
        Helpar.Shared.PushView(storyboard: StoryboardName.Authentication, Context: self, Identifier: ScreenName.SignUp)
    
    }
    
    @IBAction func SignInTapped(sender:UIButton){
        if !user_name_faild.isValid()  || user_name_faild.isSqlInject(){
            
            user_name_faild.shake()
            return
        }
        
        if !password_faild.isValid()  || password_faild.isSqlInject(){
            
            password_faild.shake()
            
            return
        }
        LogIn()
        
        
    }
    
    @IBAction func ForgetTapped(_ sender:UIButton){
        Helpar.Shared.PushView(storyboard: StoryboardName.Authentication, Context: self, Identifier: ScreenName.Forget)
     
    }
    

}


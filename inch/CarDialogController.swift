//
//  CarDialogController.swift
//  inch
//
//  Created by Ahmad Almasri on 1/6/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import UIKit
import DropDown
import Toast_Swift
class CarDialogController: UIViewController,CacheHelparDelegete,UITextFieldDelegate {
    
    @IBOutlet weak var perant_view:UIView!
    @IBOutlet weak var car_type_button:UIButton!
    @IBOutlet weak var car_category_button:UIButton!
    @IBOutlet weak var searchInTypeTextField:UITextField!
    @IBOutlet weak var  title_label:UILabel!
    @IBOutlet weak var submit_button:UIButton!
    
    @IBOutlet weak var searchCategoryTextField: UITextField!
    
    let Car_Type_DropDown = DropDown()
    let Car_Category_DropDown = DropDown()
    var CarModelSeleted = CarModel()
    var CarTypeList = [String]()
    var CarCategoryList = [String]()
    var  CarTypeListFilter:[String]  = [String]()
    var CarCategoryListFilter:[String]  = [String]()
    
    var type = ""
    var category = ""
    var cache = CacheHelpar()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.75)
        self.perant_view.roundingUIView(cornerRadiusParam: 4)
        self.car_type_button.AddBordar(Color: Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.ColorLineBg, alpha: 1), Width: 1)
        self.car_category_button.AddBordar(Color: Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.ColorLineBg, alpha: 1), Width: 1)
        
        
        title_label.font = FontHelpar.Regular(Size: 15)
        car_category_button.titleLabel?.font = FontHelpar.Light(Size: 15)
        car_type_button.titleLabel?.font = FontHelpar.Light(Size: 15)
        submit_button.titleLabel?.font = FontHelpar.Bold(Size: 15)
        
        title_label.text = Translation.Shared.GetVal(Key: "choosecartype").uppercased()
        car_type_button.setTitle("   ", for: .normal)
        
        
        searchInTypeTextField.placeholder = "   " + Translation.Shared.GetVal(Key: "cartype")
        
        car_category_button.setTitle("   ", for: .normal)
        
        
        searchCategoryTextField.placeholder = "   " + Translation.Shared.GetVal(Key: "carcategory")
        
        submit_button.setTitle(Translation.Shared.GetVal(Key: "submit").uppercased(), for: .normal)
        
        searchInTypeTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)),
                                        for: UIControlEvents.editingChanged)
        searchCategoryTextField.addTarget(self, action: #selector(self.textFieldDidChange2(_:)),
                                          for: UIControlEvents.editingChanged)
        searchInTypeTextField.addTarget(self, action: #selector(self.ShouldBeginEditingType(_:)),
                                          for: UIControlEvents.editingDidBegin)
        
        searchCategoryTextField.addTarget(self, action: #selector(self.ShouldBeginEditingCategory(_:)),
                                        for: UIControlEvents.editingDidBegin)
        
        
        
        cache.Delegete = self
        
        if DBMethod.Shared.IsEmpty(ofType:  CarModel.self){
            perant_view.ShowLoader(Color: perant_view.backgroundColor)
            cache.GetCarModel()
            
        }else{
            CarTypeList = DBMethod.Shared.GetAll(ofType: CarModel.self).map{$0.make}
            CarTypeList = DBMethod.Shared.uniq(source: CarTypeList)
            setupDropDown()
            
        }
        
    }
    
    
    
    func setupDropDown(){
        Car_Type_DropDown.anchorView =  car_type_button
        Car_Type_DropDown.bottomOffset = CGPoint(x: 0, y: car_type_button.bounds.height)
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        Car_Type_DropDown.dataSource = CarTypeList
        // Action triggered on selection
        Car_Type_DropDown.selectionAction = { [unowned self] (index, item) in
            //self.car_type_button.setTitle(item, for: .normal)
            self.searchInTypeTextField.text = "     "+item
            self.CarModelSeleted = DBMethod.Shared.GetById(ofType: CarModel.self, predicate: "make== '\(item)'").first!
            self.CarCategoryList = DBMethod.Shared.GetById(ofType: MakeModel.self, predicate: "car_type_id == \(self.CarModelSeleted.car_type_id)").map{$0.model}
            
            self.CarCategoryList  = DBMethod.Shared.uniq(source:  self.CarCategoryList)
            
            self.type = "\(self.CarModelSeleted.car_type_id)"
            
            Helpar.Logar(msg:  self.CarModelSeleted.make)
            
            self.setupCategoryDropDown()
            self.car_type_button.setTitleColor(UIColor.black, for: .normal)
        }
        
        
        
    }
    
    func setupCategoryDropDown(){
        Car_Category_DropDown.anchorView = car_category_button
        Car_Category_DropDown.bottomOffset = CGPoint(x: 0, y: car_category_button.bounds.height)
        
        Car_Category_DropDown.dataSource = CarCategoryList
        
        Car_Category_DropDown.selectionAction = { [unowned self] (index, item) in
            // self.car_category_button.setTitle(item, for: .normal)
            self.searchCategoryTextField.text = "     "+item
            self.category = "\(DBMethod.Shared.GetById(ofType: MakeModel.self, predicate: "model == '\(item)'").first!.car_details_id)"
            // Helpar.Logar(msg:  self.CarModelSeleted.car_details_id)
            
            self.car_category_button.setTitleColor(UIColor.black, for: .normal)
        }
        
    }
    
    func ShouldBeginEditingType(_ textField: UITextField)  {
        
            Car_Type_DropDown.show()
    
    
        
    }
    
    func ShouldBeginEditingCategory(_ textField: UITextField)  {
        
        Car_Category_DropDown.show()
        
        
        
    }

    func textFieldDidChange(_ textField: UITextField) {
        
        
        
            CarTypeListFilter =    CarTypeList.filter { term in
                
                
                
                return term.lowercased().contains(searchInTypeTextField.text!.lowercased())
            }
            
            Car_Type_DropDown.dataSource = CarTypeListFilter
            Car_Type_DropDown.reloadAllComponents()
            Car_Type_DropDown.show()
       
        

        
    }
    
    
    func textFieldDidChange2(_ textField: UITextField) {
        
        
        
        
        CarCategoryListFilter =  CarCategoryList.filter { term in
            
            
            
            return term.lowercased().contains(searchCategoryTextField.text!.lowercased())
        }
        
        Car_Category_DropDown.dataSource = CarCategoryListFilter
        Car_Category_DropDown.reloadAllComponents()
        Car_Category_DropDown.show()
        
    }
    
    @IBAction func SubmitTapped(_ sender:UIButton){
        
        if self.type == "" {
            
            car_type_button.shake()
            searchInTypeTextField.shake()
            return
            
        }
        
        
        if self.category == "" {
            
            car_category_button.shake()
            
            return
            
        }
        
        
        
        let imageDataDict:[String: String] = ["category": self.category,"type":self.type]
        
        // Post a notification
        NotificationCenter.default.post(name: NSNotification.Name("GetCarType"), object: nil, userInfo: imageDataDict)
        
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func changeCarType(_ sender: UIButton) {
        Car_Type_DropDown.show()
    }
    
    @IBAction func changeCarCategory(_ sender: UIButton) {
        
        if self.CarModelSeleted.car_type_id == 0 {
            
            car_type_button.shake()
            
            return
        }
        Car_Category_DropDown.show()
    }
    
    @IBAction func CancelTapped(sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    func EndCache() {
        
        perant_view.dismissLoader()
        CarTypeList = DBMethod.Shared.GetAll(ofType: CarModel.self).map{$0.make}
        CarTypeList = DBMethod.Shared.uniq(source: CarTypeList)
        setupDropDown()
    }
}


//
//  AddDriverController.swift
//  inch
//
//  Created by Macbook Pro on 9/5/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import UIKit
import ElValidator
import SWRevealViewController
class AddDriverController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var menuButton: UIBarButtonItem!

    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var phoneTextFiled: TextFieldValidator!
    @IBOutlet weak var nationalIdTextFiled: TextFieldValidator!
    @IBOutlet weak var fullNameTextFiled: TextFieldValidator!
    var http = HttpHelpar()
    
    var validationBlock:((_: [Error]) -> Void)?
    var activeTextField: TextFieldValidator?
    
    func customSetup() {
        let revealViewController: SWRevealViewController? = self.revealViewController()
        if revealViewController != nil {
            menuButton.target = self.revealViewController()
            revealViewController?.rightViewRevealWidth = (UIScreen.main.bounds.width-40)
            revealViewController?.frontViewShadowOffset = CGSize(width: 0, height: 0) //CGSizeMake(0, 0);
            revealViewController?.frontViewShadowOpacity = 0.0
            revealViewController?.frontViewShadowRadius = 0.0
            Config.MenuIsEnable = SharedDataHelpar.Shared.GetIsPay()
            
            
            menuButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            //  navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customSetup()
http.delegate = self
        nationalIdTextFiled.BottomLineView(Color: Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.ColorLine, alpha: 1), Size: 1)
        phoneTextFiled.BottomLineView(Color: Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.ColorLine, alpha: 1), Size: 1)
        fullNameTextFiled.BottomLineView(Color: Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.ColorLine, alpha: 1), Size: 1)

        
        validationBlock = { [weak self] (errors: [Error]) -> Void in
            if let error = errors.first {
                print(error)
                self?.activeTextField?.False()
            } else {
                self?.activeTextField?.True()
            }
        }

        
        fullNameTextFiled.delegate = self
        fullNameTextFiled.add(validator: LenghtValidator(validationEvent: .perCharacter, min: 5, max: 20))
        fullNameTextFiled.validationBlock = validationBlock
        
        phoneTextFiled.delegate = self
        phoneTextFiled.add(validator:PatternValidator(pattern: .phone))
        phoneTextFiled.add(validator: LenghtValidator(validationEvent: .perCharacter, min: 10, max: 14))
        phoneTextFiled.validationBlock = validationBlock
        
        phoneTextFiled.delegate = self
        phoneTextFiled.add(validator:PatternValidator(pattern: .numeric))
        phoneTextFiled.add(validator: LenghtValidator(validationEvent: .perCharacter, min: 10, max: 14))
        phoneTextFiled.validationBlock = validationBlock
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField as? TextFieldValidator
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        activeTextField?.resignFirstResponder()
        activeTextField = nil;
        
        return true
    }
    
    @IBAction func addDriverTapped(_ sender:UIButton){
        
        if !fullNameTextFiled.isValid(){
            
            fullNameTextFiled.shake()
            
            return
        }
        
        if !nationalIdTextFiled.isValid(){
            
            nationalIdTextFiled.shake()
            
            return
        }
        
        if !phoneTextFiled.isValid(){
            
            phoneTextFiled.shake()
            
            return
        }
        
         sender.ShowLoaderSmall(Color: sender.backgroundColor)
        AddDriver()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        nationalIdTextFiled.placeholder =  Translation.Shared.GetVal(Key: "nationalid")
        phoneTextFiled.placeholder =  Translation.Shared.GetVal(Key: "phonenumber")
        fullNameTextFiled.placeholder =  Translation.Shared.GetVal(Key: "fullname")
    }

}

extension AddDriverController:HttpHelparDelegete{
    
    func AddDriver(){
        //oDetails
      //  self.next_button.ShowLoaderSmall(Color: self.next_button.backgroundColor)
        
        let   JSONString = NSString.localizedStringWithFormat("{\"oDetails\":{\"name_en\":\"%@\",\"username\":\"%@\",\"national_id\":\"%@\",\"cr\":\"%@\",\"email\":\"%@\",\"password\":\"%@\",\"user_group_id\":\"%@\",\"company_id\":\"%@\",\"branch_id\":\"%@\",\"address\":\"%@\",\"phone\":\"%@\",\"name_ar\":\"%@\",\"cnt_id\":\"%@\",\"confirm_send\":\"%@\"}}"
            
            ,fullNameTextFiled.text!,fullNameTextFiled.text!,nationalIdTextFiled.text!,"","","","\(3)","\( DBMethod.Shared.GetAll(ofType: UserModel.self).first!.iD)","0","",phoneTextFiled.text!,"name","\(Config.getCurrencyId())","\(true)")
        http.PostWithString(Url: UrlHelpar.ADD_DRIVER, JSONString: JSONString, Tag: 1)
        
    }

    
    func receivedErrorWithMessage(message: String) {
        
    }
    func receivedResponse(dictResponse: NSDictionary, Status: Bool, Tag: Int) {
        nextButton.dismissLoader()
        delayWithSeconds(0.5) { 
            self.navigationController?.popViewController(animated: false)
   
        }
        print(dictResponse)
    }
}

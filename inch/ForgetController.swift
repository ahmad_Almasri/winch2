//
//  ForgetController.swift
//  inch
//
//  Created by Ahmad Almasri on 1/12/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import UIKit
import ElValidator
class ForgetController: UIViewController,HttpHelparDelegete,UITextFieldDelegate {
    
    /// new **Object** user login insert in to database
    @IBOutlet weak var email_field:TextFieldValidator!
    @IBOutlet weak var forget_button:UIButton!
    let Http = HttpHelpar()
    
    var validationBlock:((_: [Error]) -> Void)?
    var activeTextField: TextFieldValidator?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Http.delegate = self
        forget_button.roundingUIView(cornerRadiusParam: 5)

        email_field.BottomLineView(Color: Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.ColorLineBg, alpha: 1), Size: 1)
        email_field.font = FontHelpar.Regular(Size: 14)
        forget_button.titleLabel?.font = FontHelpar.Bold(Size: 17)

        validationBlock = { [weak self] (errors: [Error]) -> Void in
            if let error = errors.first {
                print(error)
              //  self?.activeTextField?.False()
            } else {
                self?.activeTextField?.True()
            }
        }

        
        email_field.delegate = self
        email_field.add(validator:PatternValidator(pattern: .mail))
        email_field.validationBlock = validationBlock
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        let bar:UINavigationBar! =  self.navigationController?.navigationBar
        
        bar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        bar.shadowImage = UIImage()
        bar.backgroundColor = UIColor(red: 0.0, green: 0.3, blue: 0.5, alpha: 0.0)
        bar.tintColor = Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.PrimaryColor, alpha: 1)
        email_field.placeholder = Translation.Shared.GetVal(Key: "forgetfield")
        forget_button.setTitle(Translation.Shared.GetVal(Key: "forgetpassword"), for: .normal)

    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField as? TextFieldValidator
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        activeTextField?.resignFirstResponder()
        activeTextField = nil;
        
        return true
    }

    
    @IBAction func ForgetTapped(_ sender:UIButton){
        if ((email_field.text ?? "" ).isEmpty ) || email_field.isSqlInject(){

            email_field.shake()

            return
        }
        sender.ShowLoaderSmall(Color:sender.backgroundColor)
        
        
        
        let Params = ["userEmail":email_field.text!,"nationalIdcr":email_field.text!,"CR":email_field.text!]
        Http.Post(Url: UrlHelpar.FORGET_PASSWORD, parameters: Params, Tag: 1)
    }
    
    func receivedErrorWithMessage(message: String) {
        Helpar.Logar(msg: message)
        self.forget_button.dismissLoader()
    }
  
    func receivedResponse(dictResponse: NSDictionary, Status: Bool, Tag: Int) {
       
        self.forget_button.dismissLoader()
        
        if Tag == 1 {
            
            if Status {
                Helpar.Shared.Alert(Title: Translation.Shared.GetVal(Key: "alert"), Message: Translation.Shared.GetVal(Key: "checkemil"), BtnOk: Translation.Shared.GetVal(Key: "ok"), Context: self, Actions: {
                })
            }else{
                Helpar.Shared.Alert(Title: Translation.Shared.GetVal(Key: "alert"), Message: Translation.Shared.GetVal(Key: "invaledemail"), BtnOk: Translation.Shared.GetVal(Key: "ok"), Context: self, Actions: {
                })

            }
        }
        
    }
}

//
//  MapController.swift
//  inch
//
//  Created by Ahmad Almasri on 1/23/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import UIKit
import GoogleMaps
//import GoogleMapsCore
import SocketIO
import RealmSwift
import GoogleMapsDirections
import SWRevealViewController
import FirebaseDatabase



class MapController: UIViewController,CLLocationManagerDelegate,GMSMapViewDelegate ,URLSessionDelegate,HttpHelparDelegete,FirebaseHelparDelegete{
    
    @IBOutlet weak var TowingBtn: UIButton!
    
    @IBOutlet weak var dismissBtn: UIButton!
    
    func receivedResponse(dictResponse: NSDictionary, Status: Bool, Tag: Int) {
        
        
        writeData(requestData: dictResponse)
        
        
    }
    
    func receivedErrorWithMessage(message: String) {
        menuButton.isEnabled = true
    }
    
    @IBOutlet weak var  map_view:GMSMapView!
    @IBOutlet weak var  serviceView:UIView!
    @IBOutlet weak var  menuButton:UIBarButtonItem!
    @IBOutlet weak var driverInfoShowConstrints: NSLayoutConstraint!
    @IBOutlet weak var driverNameLabel: UILabel!
    @IBOutlet weak var driverPlateNumber: UILabel!
    @IBOutlet weak var requestIdNumber: UILabel!
    
    
    @IBOutlet weak var driverView:UIView!
    
    //             "driver_name" = "Tareq Driver";
    //           "driver_phone" = 0096279;
    //           "plate_number" = 77117711;
    
    
    
    
    
    var originMarker: GMSMarker!
    var destinationMarker: GMSMarker!
    var locationManager = CLLocationManager()
    
    var didFindMyLocation = false
    var socket:SocketIOClient!
    var infoView:MarkerInfoView?
    var  origin: GoogleMapsService.Place!
    var destination: GoogleMapsService.Place!
    var isDrow = false
    var isFirstLocation = true
    var notDrowLocation = false
    var path:GMSPath!
    var  polyline:GMSPolyline!
    var ServiceId = 0
    var userId = 0
    var duration = ""
    var phoneNumber:String?
    var myLocation: CLLocation!
    let http = HttpHelpar()
    let syncToFirebase = SyncDataModel()
    var isShowAlert = false


    var DriverList = [DriverListFirebaseModel]()
    var DriverListToDelete = [DriverListFirebaseModel]()

    
    var selectedDriverIndex = 0
    var deleteSelectedDriverIndex : Int?

    var requestUserData: NSMutableDictionary = [:]
    var arrayOfDeleteDriver:[Int] = []

    var isArrived = true

    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        completionHandler(URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
        
    }
    
    
    func receivedResponseFromFirebase(dictResponse: NSDictionary, Status: Bool, Tag: Int) {
        print(dictResponse)
        
        
        var  componentArray = dictResponse.allValues
        self.DriverList.removeAll()
        
        if let result =  componentArray as? NSArray{
            
            for i in 0..<result.count{
                print(result[i])
                
                let JSON = Helpar.Shared.DictToJson(JSON: result[i] as! NSDictionary)
                
                let DriverListM = DriverListFirebaseModel(JSON: JSON as String)
               
                if(DriverListM.isAvailable != 0){
                    print(DriverListM.id)
                    
                }
                var deleteDriver = true
                
                print(arrayOfDeleteDriver.count)
                if(arrayOfDeleteDriver.count > 0){
                for idToDelete in arrayOfDeleteDriver {
                    if(DriverListM.id == idToDelete){
                        deleteDriver = false
                    }
                }
                }
                
                let myString = String(ServiceId)

                var supportedServices = false

                print(myString)
                print(DriverListM.supportedServices)

                
                if DriverListM.supportedServices.range(of:myString) != nil {
                    supportedServices = true
                }

                
                if(deleteDriver && DriverListM.isAvailable != 0 && supportedServices){
                    self.DriverList.append(DriverListM)

                }
                
            }
            
        }
        
        
        print(self.DriverList.count)
        
        if(self.DriverList.count > 0){
            
       
        var closestLocation: CLLocation?
        var smallestDistance: CLLocationDistance?
        
        var index = 0
    
            
        
        for driverData in self.DriverList {
            
            
            let location = CLLocation(latitude: driverData.lat, longitude: driverData.lng)
            let distance = self.myLocation.distance(from: location)
            
            if smallestDistance == nil || distance < smallestDistance! {
                closestLocation = location
                smallestDistance = distance
                selectedDriverIndex = index
            }
            index+=1
        }
        
        
        
        let userDefults=UserDefaults()

        if let requestID = userDefults.value(forKey: "requestID") {
            
            print("self.DriverList[arrayInde].id ==   \(self.DriverList[selectedDriverIndex].id)")
            print("requestID ==   \(requestID)")

        
        
        let dict2: NSMutableDictionary? = ["supportedServices" : self.DriverList[selectedDriverIndex].supportedServices,
                                           "accept" : 0 ,
                                           "arrived" : 0,
                                           "companyId"  : self.DriverList[selectedDriverIndex].companyId,
                                           "countryId" :  self.DriverList[selectedDriverIndex].countryId,
                                           "dissmis" : 0 ,
                                           "id" : self.DriverList[selectedDriverIndex].id,
                                           "isAvailable" : 0,
                                           "lat" : self.DriverList[selectedDriverIndex].lat,
                                           "lng" : self.DriverList[selectedDriverIndex].lng,
                                           "name" : self.DriverList[selectedDriverIndex].name,
                                           "phone" : self.DriverList[selectedDriverIndex].phone,
                                           "plate" : self.DriverList[selectedDriverIndex].plate,
                                           "request_id" : requestID,
                                           "servicesEnded" : 0,
                                           "winchId" : self.DriverList[selectedDriverIndex].winchId,
                                           "winchTypeId" : self.DriverList[selectedDriverIndex].winchTypeId,
                                           ]
            
            syncToFirebase.writeDataToFirebase(updatedValue: "drivers", dataDic: dict2!, requestID: self.DriverList[selectedDriverIndex].id)

            syncToFirebase.listenerForDriver(driverID: String(self.DriverList[selectedDriverIndex].id), listenerStatus: true)
            //self.DriverList[selectedDriverIndex].phone
//            let Param = ["DriverPhoneNumber":self.DriverList[selectedDriverIndex].phone]
            
            let Param = ["DriverPhoneNumber":self.DriverList[selectedDriverIndex].phone]
            http.PostSMS(Url: UrlHelpar.SendSMSDriverRequest, parameters: Param, Tag: 1)

            
            
        }

        }else{
            // No available drivers
            menuButton.isEnabled = true
//            self.dismiss(animated: true, completion: nil)
            isShowAlert = true;
                Helpar.Shared.Alert(Title: "There Is No Available Drivers" , Message: nil, BtnOk: Translation.Shared.GetVal(Key: "ok"), Context: self, Actions: {
                    self.isShowAlert = false
                })
        }
      
    }
    
    func writeData(requestData: NSDictionary){
        print(requestData["requestID"] ?? "")

        let requestID = (requestData["requestID"] as! NSString).intValue
        
        
//        let requestID = requestData["requestID"] as! String
        
        

        let userDefaults = UserDefaults.standard
        userDefaults.setValue(requestID, forKey: "requestID")
        userDefaults.synchronize() // don't forget this!!!!


        
 
        requestUserData = ["car_user_id" : SharedDataHelpar.Shared.GetUserCarId(),
                                           "countryId:" : DBMethod.Shared.GetAll(ofType: UserModel.self).first!.cnt_id,
                                           "driver_id"  : "",
                                           "endTimeStamp" :  "0000",
                                           "id" : requestID,
                                           "isEnded" : 0,
                                           "lat" : myLocation.coordinate.latitude,
                                           "lng" : myLocation.coordinate.longitude,
                                           "serviceId" : ServiceId,
                                           "startTimeStamp" : "0000000",
                                           "userId" : self.userId,
                                           "user_dismiss" : 0,
                                           
                                           ]
        

        syncToFirebase.writeDataToFirebase(updatedValue: "request", dataDic: requestUserData, requestID: requestID)
        
        syncToFirebase.readAllDataFromFirebase()
//        readDataFromFirebase()


    }
    
    func receivedDriverData(dictResponse:NSDictionary){
        print(dictResponse)
        let accept = dictResponse["accept"] as? Int ?? 0
        
                let arrived = dictResponse["arrived"] as? Int  ?? 0
        //        let companyId = dictResponse["companyId"] as? String ?? ""
        //        let countryId = dictResponse["countryId"] as? String ?? ""
                let dissmis = dictResponse["dissmis"] as? Int ?? 0
        //        let id = dictResponse["id"] as? String ?? ""
        //        let isAvailable = dictResponse["isAvailable"] as? String ?? ""
                let lat = dictResponse["lat"] as? Double ?? 0.0
                let lng = dictResponse["lng"] as? Double ?? 0.0
                let name = dictResponse["name"] as? String ?? ""
                let phone = dictResponse["phone"] as? String ?? ""
                let plate = dictResponse["plate"] as? String ?? ""
                let requestId = dictResponse["request_id"] as? Int ?? 0
                let driverId = dictResponse["id"] as? Int ?? 0
                let servicesEnded = dictResponse["servicesEnded"] as? Int ?? 0

        
        let userDefults=UserDefaults()
        
        let requestID222 = userDefults.value(forKey: "requestID") as! Int 

        
        //        let servicesEnded = dictResponse["servicesEnded"] as? String ?? ""
        //        let winchId = dictResponse["winchId"] as? String ?? ""
        //        let winchTypeId = dictResponse["winchTypeId"] as? String ?? ""

        if(accept == 0 && dissmis == 0 && servicesEnded == 0){


        }else if(accept == 1 && dissmis == 0 && arrived != 1){
            
            menuButton.isEnabled = true

            if self.infoView != nil {
                self.infoView?.isHidden = true
            }
            
            self.notDrowLocation = false
            Helpar.Logar(msg: dictResponse)
            self.driverInfoShowConstrints.constant = 100
            self.driverView.isHidden = false
            
            self.driverNameLabel.text = name
            self.driverPlateNumber.text =  plate
            self.requestIdNumber.text = String(describing: requestId)
            self.phoneNumber =  phone
            
            if self.infoView != nil {
                
                self.infoView?.isHidden = true
            }
            
            if !self.notDrowLocation{
                self.setLocation(lat: lat , long: lng )
            }
            
            
        }else if(accept == 1 && dissmis == requestId){
            
                        if self.infoView != nil {
                            self.infoView?.isHidden = false
                        }
            
                        self.driverInfoShowConstrints.constant = 0
                        self.driverView.isHidden = true
                        self.map_view.clear()
                        self.isDrow = false
                        self.setuplocationMarker(coordinate: self.myLocation.coordinate)
                        menuButton.isEnabled = true
            
                        Helpar.Shared.Alert(Title: "the driver dismissed the order" , Message: nil, BtnOk: Translation.Shared.GetVal(Key: "ok"), Context: self, Actions: {
            
                        })


        }else if(accept == 0 && dissmis == requestID222){
            
            //driver dissmis without accept
            
            if self.infoView != nil {
                self.infoView?.isHidden = false
            }
            
            menuButton.isEnabled = true


            self.driverInfoShowConstrints.constant = 0
            self.driverView.isHidden = true
            self.map_view.clear()
            
            self.isDrow = false
            self.setuplocationMarker(coordinate: self.myLocation.coordinate)
            
//            deleteSelectedDriverIndex = selectedDriverIndex
            arrayOfDeleteDriver .append(driverId)

            syncToFirebase.listenerForDriver(driverID: String(driverId), listenerStatus: false)
            self.syncToFirebase.readAllDataFromFirebase()
//
//            Helpar.Shared.Alert(Title: "the driver ignor the order" , Message: "pleese order new driver", BtnOk: Translation.Shared.GetVal(Key: "ok"), Context: self, Actions: {
//
//            })
        }else if(arrived == 1 && servicesEnded == 0){
            

            if(dismissBtn.isEnabled){
            Helpar.Shared.Alert(Title: "The driver arrived " , Message: "", BtnOk: Translation.Shared.GetVal(Key: "ok"), Context: self, Actions: {
            })
            }
            dismissBtn.setTitleColor(.gray, for: .normal)

            dismissBtn.isEnabled = false


        } else if(servicesEnded == 1){
            if self.infoView != nil {
                self.infoView?.isHidden = false
            }
            
            menuButton.isEnabled = true
            
            self.driverInfoShowConstrints.constant = 0
            self.driverView.isHidden = true
            self.map_view.clear()
            
            self.isDrow = false
            self.setuplocationMarker(coordinate: self.myLocation.coordinate)
            syncToFirebase.listenerForDriver(driverID: String(driverId), listenerStatus: false)

            Helpar.Shared.Alert(Title: "The driver End job" , Message: "", BtnOk: Translation.Shared.GetVal(Key: "ok"), Context: self, Actions: {
                self.openCarList()

                })

        }
//        let arrived = dictResponse["arrived"] as? String ?? ""
//        let companyId = dictResponse["companyId"] as? String ?? ""
//        let countryId = dictResponse["countryId"] as? String ?? ""
//        let dissmis = dictResponse["dissmis"] as? String ?? ""
//        let id = dictResponse["id"] as? String ?? ""
//        let isAvailable = dictResponse["isAvailable"] as? String ?? ""
//        let lat = dictResponse["lat"] as? String ?? ""
//        let lng = dictResponse["lng"] as? String ?? ""
//        let name = dictResponse["name"] as? String ?? ""
//        let phone = dictResponse["phone"] as? String ?? ""
//        let plate = dictResponse["plate"] as? String ?? ""
//        let requestId = dictResponse["request_id"] as? String ?? ""
//        let servicesEnded = dictResponse["servicesEnded"] as? String ?? ""
//        let winchId = dictResponse["winchId"] as? String ?? ""
//        let winchTypeId = dictResponse["winchTypeId"] as? String ?? ""

        print(accept)
//        print(arrived)
//        print(companyId)
//        print(countryId)
//        print(dissmis)
//        print(id)
//        print(isAvailable)
//        print(lat)
//        print(lng)
//        print(name)
//        print(phone)
//        print(plate)
//        print(requestId)
//        print(servicesEnded)
//        print(winchId)
//        print(winchTypeId)

        
        
        


    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        menuButton.isEnabled = true

        driverInfoShowConstrints.constant = 0
        driverView.isHidden = true
        
        
        openCarList()
        //if !SharedDataHelpar.Shared.GetIsOpenMap(){
        //  Helpar.Shared.ModelView(storyboard: StoryboardName.CarList, Context: self, Identifier: ScreenName.ChooseCar)
        
        //}
        
        
        
   

        map_view.delegate = self
        http.delegate = self
        syncToFirebase.delegate = self
        GoogleMapsDirections.provide(apiKey: Config.MAP_API_KEY)
        
        // Connect Socket iO
//        socket  = SocketIOClient(socketURL: URL(string: UrlHelpar.SOKET_URL)!, config: [.log(true),.reconnects(true),.forceNew(true) ,.forcePolling(true),.sessionDelegate(self)])
        
        
        if let userId = DBMethod.Shared.GetAll(ofType: UserModel.self).first?.iD{
            self.userId = userId
            //                self.socket.emit("storeClientInfo", with: [["user_id":userId,"user_type_id":3]])
            
        }
//        socket.on("connect") {data, ack in
//            print("socket connected")
//
//
//            if let userId = DBMethod.Shared.GetAll(ofType: UserModel.self).first?.iD{
//                self.userId = userId
////                self.socket.emit("storeClientInfo", with: [["user_id":userId,"user_type_id":3]])
//
//            }
//        }
        
//        socket.on("messageSuccess") { (data, ack) in
//            Helpar.Logar(msg: data)
//
//        }
//
//        socket.connect()
        
        
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        //  map_view.addObserver(self, forKeyPath: "myLocation", options: NSKeyValueObservingOptions.new, context: nil)
        self.map_view.isIndoorEnabled = false
        self.map_view.isBuildingsEnabled = false
        self.map_view.accessibilityElementsHidden = true
        self.map_view.settings.myLocationButton = false
        
        DispatchQueue.main.async( execute: {
            self.map_view.isMyLocationEnabled = true
        })
        
        do {
            
            
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "MapStyle", withExtension: "json") {
                
                map_view.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                
            } else {
                
                Helpar.Logar(msg: "Unable to find MapStyle.json")
            }
        } catch {
            
            Helpar.Logar(msg:"One or more of the map styles failed to load. \(error)")
        }
        
        
        customSetup()
    }
    
    
    
    func customSetup() {
        let revealViewController: SWRevealViewController? = self.revealViewController()
        if revealViewController != nil {
            menuButton.target = self.revealViewController()
            revealViewController?.rightViewRevealWidth = (UIScreen.main.bounds.width-40)
            revealViewController?.frontViewShadowOffset = CGSize(width: 0, height: 0) //CGSizeMake(0, 0);
            revealViewController?.frontViewShadowOpacity = 0.0
            revealViewController?.frontViewShadowRadius = 0.0
            Config.MenuIsEnable = true
            
            menuButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            //  navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.title = Translation.Shared.GetVal(Key: "map")
        
     checkLocation()
        
        

    }
    
    
    func sendRequast(){
        
        menuButton.isEnabled = false

        arrayOfDeleteDriver = []
        
        dismissBtn.isEnabled = true
        dismissBtn.setTitleColor(.red, for: .normal)

        
        Helpar.Shared.Alert(Title: "Looking For  Driver" , Message: nil, BtnOk: Translation.Shared.GetVal(Key: "ok"), Context: self, Actions: {
            if(self.isShowAlert){
                self.isShowAlert = false

                Helpar.Shared.Alert(Title: "There Is No Available Drivers" , Message: nil, BtnOk: Translation.Shared.GetVal(Key: "ok"), Context: self, Actions: {
                    self.isShowAlert = false
                })

            }
            
        })
        
        self.map_view.selectedMarker = nil
        
        if SharedDataHelpar.Shared.GetUserCarId() == 0 {
            
            openCarList()
            
            return
        }
        
        if infoView != nil {
            self.infoView?.isHidden = true
        }
        // self.socket.connect()
        
        
        
        
        
        
        let paramsDic = ["nLang":"1",
                       "user_id":self.userId,
                       "user_lng":myLocation.coordinate.longitude,
                       "user_lat": myLocation.coordinate.latitude,
                       "service_id":ServiceId,
                       "user_car_id":SharedDataHelpar.Shared.GetUserCarId(),
                       "CNT_id":DBMethod.Shared.GetAll(ofType: UserModel.self).first!.cnt_id
            ] as [String : Any]

        let dicODetails = ["oDetails":paramsDic]
        
        http.PostRequestID(Url: UrlHelpar.GetRequestID_WINCH, parameters: dicODetails, Tag: 1)

        
//        var closestLocation: CLLocation?
//        var smallestDistance: CLLocationDistance?
//
//        let coord1 = CLLocation(latitude: 32.004115, longitude: 35.9666283)
//        let coord2 = CLLocation(latitude: 31.9648909, longitude: 35.8836452)
//        let coord3 = CLLocation(latitude: 32.004115, longitude: 35.9666283)
//        let coord4 = CLLocation(latitude: 32.088457216285484, longitude: 36.098218098289784)
//
//
//
//        var myArray = [coord1, coord2, coord3,coord4]
//
//        print(myLocation.coordinate.longitude)
//        for location in myArray {
//            let distance = myLocation.distance(from: location)
//            if smallestDistance == nil || distance < smallestDistance! {
//                closestLocation = location
//                smallestDistance = distance
//
//            }
//        }
//        print(closestLocation?.coordinate.latitude)
//        print(closestLocation?.coordinate.longitude)


        
//        let params = ["user_id":self.userId, "user_type_id": 3,"user_lng":myLocation.coordinate.longitude, "user_lat": myLocation.coordinate.latitude,"request_Date":"\(Date().timeIntervalSince1970)", "service_Id":ServiceId, "status": "673", "user_car_id":SharedDataHelpar.Shared.GetUserCarId(),"country_id":DBMethod.Shared.GetAll(ofType: UserModel.self).first!.cnt_id] as [String : Any]
        
        // self.socket.emit("send-request", params)
//        self.socket.emit("send-request", with: [params])
        
        
//        self.socket.on("failed-request") { (data, ack) in
//
//
//            if self.infoView != nil {
//                self.infoView?.isHidden = false
//            }
//
//
//            self.driverInfoShowConstrints.constant = 0
//            self.driverView.isHidden = true
//            self.map_view.clear()
//
//            self.isDrow = false
//            self.setuplocationMarker(coordinate: self.myLocation.coordinate)
//
//
//            Helpar.Shared.Alert(Title: "\(data[0])" , Message: nil, BtnOk: Translation.Shared.GetVal(Key: "ok"), Context: self, Actions: {
//
//            })
//
//        }
        
        
        
//        self.socket.on("end-track-driver") { (data, ack) in
//
//
//            print("nnjjj")
//
//            // rating
//
//        }
        
        
//        self.socket.on("driver-info") { (data, ack) in
//
//
//            if self.infoView != nil {
//                self.infoView?.isHidden = true
//            }
//
//            self.notDrowLocation = false
//            Helpar.Logar(msg: data)
//            if let driverInfo = data[0] as? [[String: Any]] /*self.parseData("\(data[0] as! [String:Any])")*/ {
//                self.driverInfoShowConstrints.constant = 100
//                self.driverView.isHidden = false
//                let driverInfo  =  driverInfo[0] as NSDictionary
//                self.driverNameLabel.text = driverInfo.object(forKey: "driver_name") as? String
//                self.driverPlateNumber.text =  driverInfo.object(forKey: "plate_number") as? String
//                self.requestIdNumber.text = "#\(driverInfo.object(forKey: "request_id") as! String)"
//                self.phoneNumber =  driverInfo.object(forKey: "driver_phone") as? String
//
//
//
//
//            }
//
//
//            //            //{
//            //            "driver_name" = "Tareq Driver";
//            //            "driver_phone" = 0096279;
//            //            "plate_number" = 77117711;
//            //            "request_id" = 860;
//            //        }
//        }
        
//        self.socket.on("get-location") { (data, ack) in
//            Helpar.Logar(msg: data)
//            
//            
//            if self.infoView != nil {
//                
//                self.infoView?.isHidden = true
//            }
//            
//            if let locationInfo =  self.parseData("\(data[0])"){
//                
//                if (locationInfo.object(forKey: "Driver_Lat") as? Double) != nil{
//                    
//                    if !self.notDrowLocation{
//                        self.setLocation(lat: locationInfo.object(forKey: "Driver_Lat") as! Double, long: locationInfo.object(forKey: "Driver_Lng") as! Double)
//                    }
//                    
//                }
//            }
//            
//            //{"user_id":"1150","Driver_Lng":35.9007686,"Driver_Lat":31.991390000000003}
//            //[{"user_id":"1150","Driver_Lng":35.900711199999996,"Driver_Lat":31.991373199999998}]
//        }
        
    }
    
    func parseData(_ dataString:String)-> NSDictionary?{
        
        
        
        let dataNewNow = dataString.data(using: String.Encoding.utf8, allowLossyConversion: false)!
        
        do {
            let json = try JSONSerialization.jsonObject(with: dataNewNow, options: []) as! [String: AnyObject]
            return json as NSDictionary
            
        } catch let error as NSError {
            print("Failed to load: \(error.localizedDescription)")
        }
        
        return nil
    }
    
    @IBAction func callTapped(_ sender:UIButton){
        let phone = "tel://\(self.phoneNumber!)";
        let url:NSURL = NSURL(string:phone)!;
        UIApplication.shared.openURL(url as URL);
    }
    
    @IBAction func dismissTapped(_ sender:UIButton){
        
        
        Helpar.Shared.confirmationAlert(Title: "are you sure you want to dismiss this service", Message: nil, BtnOk: "Ok", BtnCancel: "Cancel", Context: self, ActionOk: {
            
            self.driverInfoShowConstrints.constant = 0
            self.driverView.isHidden = true
            
            self.map_view.clear()
            self.isDrow = false
            self.notDrowLocation = true
            self.setuplocationMarker(coordinate: self.myLocation.coordinate)
            
            self.menuButton.isEnabled = true
            
            
            
            let userDefults=UserDefaults()
            
            if let requestID222 = userDefults.value(forKey: "requestID") {
                
                self.requestUserData["user_dismiss"] = 1
                
                print(self.requestUserData)
                
                self.self.syncToFirebase.writeDataToFirebase(updatedValue: "request", dataDic: self.requestUserData, requestID: requestID222)
                self.syncToFirebase.listenerForDriver(driverID: String(self.DriverList[self.selectedDriverIndex].id), listenerStatus: false)
                
                let Param = ["DriverPhoneNumber":self.DriverList[self.selectedDriverIndex].phone]
                
                self.http.PostSMS(Url: UrlHelpar.SendSMSDriverDismiss, parameters: Param, Tag: 1)

                self.openCarList()
                
            }
        }, ActionCancel: {
            
            
        })
      

        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.authorizedWhenInUse ||   status == CLAuthorizationStatus.authorizedAlways {
            map_view.isMyLocationEnabled = true
        }else{
            
            _ =  checkLocation()
        }
    }
    
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            //            mapView.viewDidLoad(latitude: (location.coordinate.latitude), longitude: (location.coordinate.longitude),isMarker: true)
            
            
            didFindMyLocation = true
            
            setuplocationMarker(coordinate: location.coordinate)
            
            myLocation = location
            map_view.camera = GMSCameraPosition.camera(withTarget: location.coordinate, zoom: 17.0)
            map_view.settings.myLocationButton = false
            map_view.settings.myLocationButton = false
            locationManager.stopUpdatingLocation()
            locationManager.delegate = nil

        }
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        if ServiceId != 0 {
            
            if marker.isEqual(originMarker){
                
                self.sendRequast()
            }
        }
    }
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        
        
        
        
        
        if marker.isEqual(originMarker){
            
            let   infoView = MarkerInfoView(frame: CGRect(x: 0, y: 0, width: 225, height: 60))
            
            infoView.titleLabel.text = Translation.Shared.GetVal(Key: "confirmlocation")
            
            infoView.serviceImage.image = getServiceImageUnSelected(tag: ServiceId)
            
            infoView.isUserInteractionEnabled = true
            
            let action  = UITapGestureRecognizer(target: self, action: #selector(self.sendRequast))
            
            infoView.addGestureRecognizer(action)
            
            
            return infoView
        }else{
            
            let   infoView = MarkerTimeView(frame: CGRect(x: 0, y: 0, width: 100, height: 30))
            
            infoView.titleLabel.text = duration //Translation.Shared.GetVal(Key: "confirmlocation")
            
            
            
            
            return infoView
            
        }
    }
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        //        if ServiceId != 0 {
        //
        //            self.sendRequast()
        //        }
        if marker.isEqual(originMarker){
            
            map_view.selectedMarker = self.originMarker
        }
        return true
    }
    //    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
    //        if !didFindMyLocation {
    //            myLocation = change![NSKeyValueChangeKey.newKey] as! CLLocation
    //            map_view.camera = GMSCameraPosition.camera(withTarget: myLocation.coordinate, zoom: 17.0)
    //            map_view.settings.myLocationButton = false
    //
    //            didFindMyLocation = true
    //
    //            setuplocationMarker(coordinate: myLocation.coordinate)
    //
    //        }
    //    }
    
    
    func mapView(_ mapView: GMSMapView, didTapAt didTapAtCoordinate:CLLocationCoordinate2D){
        
        
        self.map_view.clear()

        origin = GoogleMapsDirections.Place.coordinate(coordinate:  GoogleMapsService.LocationCoordinate2D(latitude: didTapAtCoordinate.latitude, longitude: didTapAtCoordinate.longitude))
        
        map_view.isMyLocationEnabled = false
        originMarker = GMSMarker(position: didTapAtCoordinate)
        originMarker.map = map_view
        originMarker.appearAnimation = GMSMarkerAnimation.pop
        originMarker.icon = #imageLiteral(resourceName: "markar")
        originMarker.opacity = 1
        originMarker.isFlat = true
        
        let location = CLLocation(latitude: didTapAtCoordinate.latitude, longitude: didTapAtCoordinate.longitude)

        print(myLocation.coordinate.latitude)
        print(myLocation.coordinate.longitude)

        myLocation = location
        print(myLocation.coordinate.latitude)
        print(myLocation.coordinate.longitude)

        }
    

    func setuplocationMarker(coordinate: CLLocationCoordinate2D) {
        
        
        origin = GoogleMapsDirections.Place.coordinate(coordinate:  GoogleMapsService.LocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude))
        
        map_view.isMyLocationEnabled = false
        originMarker = GMSMarker(position: coordinate)
        originMarker.map = map_view
        originMarker.appearAnimation = GMSMarkerAnimation.pop
        originMarker.icon = #imageLiteral(resourceName: "markar")
        originMarker.opacity = 1
        originMarker.isFlat = true
        //  originMarker.title = "gggg"
        
        
        
        /* let lat = 51.5099758
         let long =  -0.1343893
         
         
         self.destination = GoogleMapsDirections.Place.coordinate(coordinate:  GoogleMapsService.LocationCoordinate2D(latitude: lat, longitude: long))
         
         // check is drow or not
         self.destinationMarker = GMSMarker(position: CLLocationCoordinate2D(latitude: lat, longitude: long ))
         self.destinationMarker.map = self.map_view
         self.destinationMarker.title =  "ff"
         self.destinationMarker.appearAnimation = GMSMarkerAnimation.pop
         self.destinationMarker.icon = #imageLiteral(resourceName: "ic_track")
         self.destinationMarker.opacity = 1
         self.destinationMarker.isFlat = true
         self.destinationMarker.snippet = "The best place on earth."
         
         
         self.destinationMarker.position =  CLLocationCoordinate2D(latitude: lat, longitude: long )
         
         self.DrowRout()*/
    }
    
    
    func setLocation(lat:Double,long:Double){
        
        
        
        self.destination = GoogleMapsDirections.Place.coordinate(coordinate:  GoogleMapsService.LocationCoordinate2D(latitude: lat, longitude: long))
        
        
        
        // check is drow or not
        if destinationMarker == nil {
            
            self.destinationMarker = GMSMarker(position: CLLocationCoordinate2D(latitude: lat, longitude: long ))
            
        }
        
        self.destinationMarker.map = self.map_view
        self.destinationMarker.title =  "\(duration)"
        self.destinationMarker.appearAnimation = GMSMarkerAnimation.pop
        self.destinationMarker.icon = #imageLiteral(resourceName: "ic_track")
        self.destinationMarker.opacity = 1
        self.destinationMarker.isFlat = true
        
        
//        self.map_view.selectedMarker = destinationMarker
        
        self.destinationMarker.position =  CLLocationCoordinate2D(latitude: lat, longitude: long )
        
//        self.DrowRout()
    }
    
    func DrowRout(){
        
        
        //GoogleMapsDirections.TravelMode = .driving
        
        GoogleMapsDirections.direction(fromOrigin: origin, toDestination: destination) { (response, error) -> Void in
            // Check Status Code
            guard response?.status == GoogleMapsDirections.StatusCode.ok else {
                // Status Code is Not OK
                debugPrint(response?.errorMessage! ?? "")
                return
            }
            
            // Use .result or .geocodedWaypoints to access response details
            // response will have same structure as what Google Maps Directions API returns
            // debugPrint("it has \(response?.routes[0].overviewPolylinePoints ?? 0) routes")
            
            
            for route in (response?.routes)!{
                
                if let  duration = route.legs[0].duration?.text{
                    
                    
                    self.duration = duration
                }
                self.path = GMSPath.init(fromEncodedPath: route.overviewPolylinePoints!)
                if !self.isDrow{
                    self.isDrow = true
                    self.polyline = GMSPolyline.init(path: self.path)
                    self.polyline.strokeWidth = 5
                    
                    self.polyline.strokeColor = Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.PrimaryColor, alpha: 1)
                    
                    self.polyline.map = self.map_view
                }else{
                    self.polyline.path = self.path
                }
            }
        }
    }
    
    
    
    func openCarList(){
        
        let storyboard = UIStoryboard.init(name:StoryboardName.CarList.rawValue, bundle: nil)
        let next = storyboard.instantiateViewController(withIdentifier: ScreenName.ChooseCar.rawValue)
        next.modalPresentationStyle=UIModalPresentationStyle.overFullScreen
        
        
        self.present(next, animated: true, completion: nil)
        
        
    }
    
    func checkLocation() -> Bool{
        
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                
                Helpar.Shared.confirmationAlert(Title: "Location services are not enabled", Message: nil, BtnOk: "Go to Settings?", BtnCancel: "Cancel", Context: self, ActionOk: {
                    
                    UIApplication.shared.openURL(NSURL(string: UIApplicationOpenSettingsURLString) as! URL)
                    
                }, ActionCancel: {
                    
                    
                })
                
                return true
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                
                return false
                
            }
        } else {
            // print("Location services are not enabled")
            
            
            Helpar.Shared.confirmationAlert(Title: "Location services are not enabled", Message: nil, BtnOk: "Go to Settings?", BtnCancel: "Cancel", Context: self, ActionOk: {
                
                UIApplication.shared.openURL(NSURL(string: UIApplicationOpenSettingsURLString) as! URL)
                
            }, ActionCancel: {
                
                
            })
            return false
            
        }
    }
}

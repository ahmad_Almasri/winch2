//
//  LanguegeController.swift
//  inch
//
//  Created by Ahmad Almasri on 4/19/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import UIKit
import SWRevealViewController
class LanguegeController: UIViewController {

    
    @IBOutlet weak var  menuButton:UIBarButtonItem!
    @IBOutlet weak var  imageAr:UIImageView!
    @IBOutlet weak var  imageEn:UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         customSetup()
        checkSeclected()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.title = Translation.Shared.GetVal(Key: "language").uppercased()
    }
    
    func customSetup() {
        let revealViewController: SWRevealViewController? = self.revealViewController()
        if revealViewController != nil {
            menuButton.target = self.revealViewController()
            revealViewController?.rightViewRevealWidth = (UIScreen.main.bounds.width-40)
            revealViewController?.frontViewShadowOffset = CGSize(width: 0, height: 0) //CGSizeMake(0, 0);
            revealViewController?.frontViewShadowOpacity = 0.0
            revealViewController?.frontViewShadowRadius = 0.0
            
            
            menuButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            //navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    
    func checkSeclected(){
        if  SharedDataHelpar.Shared.GetLang() == 0 {
            imageEn.image = #imageLiteral(resourceName: "selected")
            imageAr.image = #imageLiteral(resourceName: "unSelected")
        }else{
            
            imageEn.image = #imageLiteral(resourceName: "unSelected")
            imageAr.image = #imageLiteral(resourceName: "selected")
            
        }
    }
    
    @IBAction func SelectLangTapped(_ sender:UIButton){
        
        SharedDataHelpar.Shared.SetLang(iskilled: sender.tag)
        
     checkSeclected()
        
        self.navigationItem.title = Translation.Shared.GetVal(Key: "language").uppercased()

    }
   
}

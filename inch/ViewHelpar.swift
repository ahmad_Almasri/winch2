//
//  ViewHelpar.swift
//  inch
//
//  Created by Ahmad Almasri on 12/30/16.
//  Copyright © 2016 Snapics. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView
extension UIView{
    
    
    func AddBordar(Color:UIColor,Width:CGFloat){
        self.layer.borderColor = Color.cgColor
        self.layer.borderWidth = Width
        
    }
    
    func BottomLineView(Color:UIColor,Size:CGFloat){
        let width = Size
        let border = CALayer()
        border.backgroundColor = Color.cgColor
        self.setNeedsLayout()
         self.layoutIfNeeded()
        border.frame = CGRect(x: 0, y: self.frame.height-width, width: self.frame.width, height: width)
        self.layer.addSublayer(border)
        
    }
    
    func roundingUIView(cornerRadiusParam: CGFloat!){
        self.layer.cornerRadius = cornerRadiusParam
        self.clipsToBounds = true
    }
    
    func ShowLoader(Color:UIColor?){
        let LoaderView  = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
        LoaderView.tag = -1000
        LoaderView.backgroundColor = Color
        let Loadar = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 60, height: 30))
        Loadar.type = .lineScale
        Loadar.center = LoaderView.center
        Loadar.color = Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.PrimaryColor, alpha: 1)
        Loadar.startAnimating()
        LoaderView.addSubview(Loadar)
        
        self.addSubview(LoaderView)
        
    }
    
    func ShowLoaderSmall(Color:UIColor?){
        let LoaderView  = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
        LoaderView.tag = -1000
        LoaderView.backgroundColor = Color
        let Loadar = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 30, height: 20))
        Loadar.type = .lineScalePulseOut
        Loadar.center = LoaderView.center
        Loadar.color = UIColor.white//Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.PrimaryColor, alpha: 1)
        Loadar.startAnimating()
        LoaderView.addSubview(Loadar)
        
        self.addSubview(LoaderView)
        
    }
    func dismissLoader(){
        delayWithSeconds(1) {
            self.viewWithTag(-1000)?.isHidden = true
            self.viewWithTag(-1000)?.removeFromSuperview()
        }
        
        
    }
    
    func shake(){
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x:self.center.x - 10, y:self.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x:self.center.x + 10,y:self.center.y))
        self.layer.add(animation, forKey: "position")
    }
}
extension UITextField{
    
    func True(){
        self.rightViewMode = .always
        self.rightView = UIImageView(image: #imageLiteral(resourceName: "ic_true"))
    }
    func False(){
        self.rightViewMode = .always
        self.rightView = UIImageView(image: #imageLiteral(resourceName: "ic_false"))
    }
    
}

class ViewHelpar{
    
    static var Shared = ViewHelpar()
    
    
}

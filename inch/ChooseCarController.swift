//
//  ChooseCarController.swift
//  inch
//
//  Created by Ahmad Almasri on 3/25/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import UIKit

class ChooseCarController: UIViewController {
    
    @IBOutlet weak var car_table_view:UITableView!
    @IBOutlet weak var submitButton:UIButton!
    @IBOutlet weak var messageRepairLabel:UILabel!
    
    let Http = HttpHelpar()
    
    
    var UserCarList = [CarListModel]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Http.delegate = self
        car_table_view.delegate = self
        car_table_view.dataSource = self
         self.view.roundingUIView(cornerRadiusParam: 5)
        self.view.ShowLoader(Color: self.view.backgroundColor)
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.75)
        self.FindCarList(UserId: DBMethod.Shared.GetAll(ofType: UserModel.self).first!.iD)
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //"Chooserepair"submit
        
        submitButton.setTitle(Translation.Shared.GetVal(Key: "submit").uppercased(), for: .normal)
        messageRepairLabel.text = Translation.Shared.GetVal(Key: "chooserepair")
        
    }
    @IBAction func closeTapped(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func submitTapped(_ sender:UIButton){
        
        SharedDataHelpar.Shared.SetIsOpenMap(IsOpenMap: true)
        
        self.dismiss(animated: true, completion: nil)

    }
    
}

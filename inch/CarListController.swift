//
//  CarListController.swift
//  inch
//
//  Created by Ahmad Almasri on 1/10/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import UIKit
import PulsingHalo
import SWRevealViewController
class CarListController: UIViewController {
    @IBOutlet weak var car_table_view:UITableView!
    @IBOutlet weak var total_price_caption_label:UILabel!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var total_price_value_label:UILabel!
    @IBOutlet weak var i_agree_the_button:UIButton!
    @IBOutlet weak var terms_and_conditions_label:UILabel!
    @IBOutlet weak var pay_button:UIButton!
    @IBOutlet weak var add_button:UIButton!
    @IBOutlet weak var add_new_car_label:UILabel!
    let Http = HttpHelpar()
    
    var halo:PulsingHaloLayer?

    var UserCarList = [CarListModel]()
    var UserCarPayList = [CarListModel]()

    var  PayFort:PayFortController!

    var isAgree = true

    var merchant_reference = ""
    
    var fort_id = ""
    
    var token_name = ""
    
    var priceCount = 0 
        
    override func viewDidLoad() {
        super.viewDidLoad()
        Http.delegate = self

        
         
        PayFort = PayFortController.init(enviroment: KPayFortEnviromentSandBox)
        
        
        self.view.ShowLoader(Color: self.view.backgroundColor)
        self.FindCarList(UserId: DBMethod.Shared.GetAll(ofType: UserModel.self).first!.iD)
        
        pay_button.roundingUIView(cornerRadiusParam: 5)
        add_button.roundingUIView(cornerRadiusParam: add_button.frame.size.height/2)
        
        total_price_caption_label.font = FontHelpar.Semibold(Size: 20)
        total_price_value_label.font = FontHelpar.Regular(Size: 20)
        i_agree_the_button.titleLabel?.font = FontHelpar.Regular(Size: 9)
        terms_and_conditions_label.font = FontHelpar.Regular(Size: 9)
        pay_button.titleLabel?.font = FontHelpar.Regular(Size: 17)
        add_new_car_label.font = FontHelpar.Regular(Size: 14)
        

    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
       // self.navigationController?.setNavigationBarHidden(true, animated: animated)
        

        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        let bar:UINavigationBar! =  self.navigationController?.navigationBar
        
        bar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        bar.shadowImage = UIImage()
        bar.backgroundColor = UIColor(red: 0.0, green: 0.3, blue: 0.5, alpha: 0.0)
        bar.tintColor = Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.PrimaryColor, alpha: 1)
        navigationItem.backBarButtonItem?.title = ""
        customSetup()
        
     
        total_price_caption_label.text = Translation.Shared.GetVal(Key: "totalprice")
        i_agree_the_button.setTitle(Translation.Shared.GetVal(Key: "iagreethe"), for: .normal)
        terms_and_conditions_label.text = Translation.Shared.GetVal(Key: "termsandconditios")
        pay_button.setTitle(Translation.Shared.GetVal(Key: "pay"), for: .normal)
        add_new_car_label.text = Translation.Shared.GetVal(Key: "addnewcar")
        halo = PulsingHaloLayer()

          add_button.superview?.layer.insertSublayer(halo!, below: add_button.layer)
          halo?.start()
        
        self.halo?.position = self.add_button.center
        self.halo?.haloLayerNumber = 6
        self.halo?.radius = 60
        self.halo?.animationDuration = 5.0
        self.halo?.backgroundColor = Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.PrimaryColor, alpha: 1).cgColor
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
       
    }
    
    
    func customSetup() {
        let revealViewController: SWRevealViewController? = self.revealViewController()
        if revealViewController != nil {
            menuButton.target = self.revealViewController()
            revealViewController?.rightViewRevealWidth = (UIScreen.main.bounds.width-40)
            revealViewController?.frontViewShadowOffset = CGSize(width: 0, height: 0) //CGSizeMake(0, 0);
            revealViewController?.frontViewShadowOpacity = 0.0
            revealViewController?.frontViewShadowRadius = 0.0
            Config.MenuIsEnable = false
            
            menuButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            //  navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
  
    
    @IBAction func addCarTapped(_ sender:UIButton){
        Helpar.Shared.PushView(storyboard: StoryboardName.Car, Context: self, Identifier: ScreenName.PagerCar)
    }
    
    @IBAction func payTapped(_ sender:UIButton){
        
        if UserCarPayList.count > 0 {
            
            
            if UserCarPayList.count > 0 {
                priceCount = UserCarPayList.count <  5 ? UserCarPayList[0].car_service_fees * UserCarPayList.count   :  (UserCarPayList[0].car_service_fees * 5 )
                total_price_value_label.text  = "\(priceCount) \(Config.CurrencyCode)"
                
            }else{
                total_price_value_label.text  = "\(0) \(Config.CurrencyCode)"
                
                
                
        }
        
              //  sender.ShowLoaderSmall(Color:sender.backgroundColor)
            
                    self.view.ShowLoader(Color: self.view.backgroundColor?.withAlphaComponent(0.5))
                    self.startPayment()
            

            
            return
        
    }
        
        toastView(messsage: Translation.Shared.GetVal(Key: "pleaseselectcare"), view: self.view)
       

    }
    @IBAction func iAgreeTapped(_ sender:UIButton){
        isAgree = !isAgree
        if isAgree{
            sender.setImage(#imageLiteral(resourceName: "checkbox"), for: .normal)
            pay_button.isEnabled = true
            pay_button.alpha = 1.0
        }else{
            sender.setImage(#imageLiteral(resourceName: "UnCheckbox"), for: .normal)
            pay_button.isEnabled = false
            pay_button.alpha = 0.5
 
        }
        
    }
    
    
}

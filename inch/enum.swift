//
//  enum.swift
//  inch
//
//  Created by Ahmad Almasri on 12/30/16.
//  Copyright © 2016 Snapics. All rights reserved.
//

import Foundation
enum StoryboardName:String {
    
    case Authentication = "Authentication"
    case Car = "Car"
    case CarList = "CarList"
    case Map = "Map"
    case Languages = "Languages"
    case Profile = "Profile"
    case MyCar = "MyCar"
    case Company = "Company"

}
enum ScreenName:String{
    case SignUp = "SignUp"
    case Login = "Login"
    case Cars = "Cars"
    case PagerCar = "PagerCar"
    case addCarType = "addCarType"
    case SelectColor = "SelectColor"
    case SelecteYear = "SelecteYear"
    case CarList = "CarList"
    case Forget = "Forget"
    case Map = "Map"
    case ChooseCar = "ChooseCar"
    case Languege = "Languege"
    case MapCont = "MapCont"
    case Profile = "Profile"
    case ProfileController = "ProfileController"
    case MyCar = "MyCar"
    case CompanyHome = "CompanyHome"
    case AddDriver =  "AddDriver"
    case DriverList = "DriverList"
    case CompanyListController = "CompanyListController"
    case AddWinch =  "AddWinch"
     case MyCompany = "MyCompany"
}
enum Valedation:String{
    case Exist = "Exist" 
}

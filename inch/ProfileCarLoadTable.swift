//
//  ChooseCarLoadTable.swift
//  inch
//
//  Created by Ahmad Almasri on 3/25/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import Foundation
extension ProfileController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        Helpar.Shared.emptyTableView(view: tableView, Msg: Translation.Shared.GetVal(Key: "nopayed"), Count: UserCarList.count)
        return UserCarList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCarListCell", for: indexPath) as! ProfileCarListCell
        cell.selectionStyle = .none
        if UserCarList.count > 0 {
            
            cell.FellCell(CarObj: UserCarList[indexPath.row])
            
            }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
      
    }
    
    
    
   
}

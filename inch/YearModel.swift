//
//  YearModel.swift
//  inch
//
//  Created by Ahmad Almasri on 1/9/17.
//  Copyright © 2017 Snapics. All rights reserved.
//


import Foundation
import RealmSwift
class YearModel:Object{
    
    dynamic var year = 0

    
    
    convenience init(JSON:String){
        self.init()
        Helpar.Shared.Parser(JSONString: JSON, Obj: self)
    }
}

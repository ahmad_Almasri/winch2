//
//  SharedDataHelpar.swift
//  inch
//
//  Created by Ahmad Almasri on 12/30/16.
//  Copyright © 2016 Snapics. All rights reserved.
//

import Foundation
class SharedDataHelpar{
    
    static let Shared = SharedDataHelpar()
    let defaults = UserDefaults.standard
    
    let LANG = "LANG"
    let IS_CACHEING = "IS_CACHEING"
    let IS_ADD = "IS_Add"
    let IS_PAY = "IS_PAY"
    let IS_OPEN_MAP = "IS_OPEN_MAP"
    let USER_CAR_ID = "USER_CAR_ID"
    
    
    
    func SetUserCarId(userCarId:Int){
        defaults.set(userCarId, forKey: USER_CAR_ID)
        
    }
    func GetUserCarId()->Int{
        if (UserDefaults.standard.object(forKey: USER_CAR_ID) != nil) {
            return defaults.integer(forKey: USER_CAR_ID)
            
        }else{
            return 0
        }
    }

    
    func SetIsOpenMap(IsOpenMap:Bool){
        defaults.set(IsOpenMap, forKey: IS_OPEN_MAP)
        
    }
    func GetIsOpenMap()->Bool{
        if (UserDefaults.standard.object(forKey: IS_OPEN_MAP) != nil) {
            return defaults.bool(forKey: IS_OPEN_MAP)
            
        }else{
            return false
        }
    }
    func SetIsPay(IsPay:Bool){
        defaults.set(IsPay, forKey: IS_PAY)
        
    }
    func GetIsPay()->Bool{
        if (UserDefaults.standard.object(forKey: IS_PAY) != nil) {
            return defaults.bool(forKey: IS_PAY)
            
        }else{
            return false
        }
    }
    
    func SetIsAdd(IsAdd:Bool){
        defaults.set(IsAdd, forKey: IS_ADD)
        
    }
    func GetIsAdd()->Bool{
        if (UserDefaults.standard.object(forKey: IS_ADD) != nil) {
            return defaults.bool(forKey: IS_ADD)
            
        }else{
            return false
        }
    }
    
    
    func SetIsCache(IsCache:Bool){
    defaults.set(IsCache, forKey: IS_CACHEING)
        
    }
    func GetIsCache()->Bool{
        if (UserDefaults.standard.object(forKey: IS_CACHEING) != nil) {
            return defaults.bool(forKey: IS_CACHEING)
            
        }else{
            return false
        }
    }
    
    
    func SetLang(iskilled:Int){
        defaults.set(iskilled, forKey: LANG)
        
    }
    func GetLang()->Int{
        if (UserDefaults.standard.object(forKey: LANG) != nil) {
            return defaults.integer(forKey: LANG)
            
        }else{
            return 0
        }
    }

}

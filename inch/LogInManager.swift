//
//  LogInManager.swift
//  inch
//
//  Created by Ahmad Almasri on 12/30/16.
//  Copyright © 2016 Snapics. All rights reserved.
//

import Foundation
import CryptoSwift
import RealmSwift
extension LogInController:HttpHelparDelegete{
    
    func FindCarList(UserId:Int){
        let Param = ["user_id":UserId]
        Http.Get(Url: UrlHelpar.GET_CAR, parameters: Param, Tag: 3)
    }
    func LogIn(){
        self.sign_in_button.ShowLoaderSmall(Color: self.sign_in_button.backgroundColor)
        let Param = ["sEmailAddress":user_name_faild.text!,
            "sPassword":(password_faild.text?.md5())!,
            "sPhoneNumber":"",
            "rememberMe":"true"] as [String : Any]
        
        Helpar.Logar(msg: Param)

        
        Http.Post(Url: UrlHelpar.LOGIN, parameters: Param, Tag: 1)
    }
    
    func FindFuel(country_id:Int){
        let Param = ["country_id":country_id]
        Http.Get(Url: UrlHelpar.FIND_FUEL, parameters: Param, Tag: 2)
    }
    
    func receivedErrorWithMessage(message: String) {
        Helpar.Logar(msg: message)
        self.sign_in_button.dismissLoader()

        Helpar.Shared.Alert(Title: Translation.Shared.GetVal(Key: "userorpassword"), Message: nil, BtnOk: Translation.Shared.GetVal(Key: "ok"), Context: self) {
            
        }
        
        
    }
    
    func receivedResponse(dictResponse: NSDictionary, Status: Bool, Tag: Int) {
        
        
        
            Helpar.Logar(msg: "\(Helpar.Shared.DictToJson(JSON: dictResponse))")
            
                if Tag == 1 {
                
                    if Status{
                        
                        if let result =  dictResponse.object(forKey: "result") as? NSArray{
                            if result.count > 0 {
                                
                                
                                DBMethod.Shared.Delete(obj: DBMethod.Shared.GetAll(ofType: UserModel.self))
                                
                            }
                            for i in 0..<result.count{
                                let JSON = Helpar.Shared.DictToJson(JSON: result[i] as! NSDictionary)
                                
                                  User = UserModel(JSON: JSON as String)
                                
                                DBMethod.Shared.save(obj: User)
                                _  = DBMethod.Shared.update(ofType: User, value: User.id as AnyObject, key: "iD")
                            }
                            
                        }
                        
                      FindFuel(country_id: User.cnt_id)
                    }else{
                        self.sign_in_button.dismissLoader()
                        Helpar.Shared.Alert(Title: Translation.Shared.GetVal(Key: "alert"), Message: Translation.Shared.GetVal(Key: "errorlogin"), BtnOk: Translation.Shared.GetVal(Key: "ok"), Context: self, Actions: {
                        })
                    }
                    
                
                }
            
    
        if Tag == 2 {
            
            if Status {
                if let result =  dictResponse.object(forKey: "result") as? NSArray{
                    if result.count > 0 {
                        
                        
                        DBMethod.Shared.Delete(obj: DBMethod.Shared.GetAll(ofType: FuelModel.self))
                        
                    }
                    for i in 0..<result.count{
                        let JSON = Helpar.Shared.DictToJson(JSON: result[i] as! NSDictionary)
                        
                        let  Fuel = FuelModel(JSON: JSON as String)
                        
                        
                        DBMethod.Shared.save(obj: Fuel)
                        
                    }
                    FindCarList(UserId: User.iD)
               // Helpar.Shared.ModelView(storyboard: StoryboardName.Car, Context: self, Identifier: ScreenName.Cars)

                }
            }
        }
        
        if Tag == 3 {
            
            if Status{
                
                
                if let result =  dictResponse.object(forKey: "result") as? NSArray{
                    
                    var UserCarList = [CarListModel]()
                    
                    if result.count > 0 {
                        SharedDataHelpar.Shared.SetIsAdd(IsAdd: true)
                        for i in 0..<result.count{
                            let JSON = Helpar.Shared.DictToJson(JSON: result[i] as! NSDictionary)
                            
                            let User = CarListModel(JSON: JSON as String)
                            
                            
                            
                           UserCarList.append(User)
                            
                        }
                        
                        let isPaid = UserCarList.map({$0.is_paid})
                        
                        
                        if self.User.user_group_id == 3 {
                            //CompanyHome
                            Helpar.Shared.ModelView(storyboard: StoryboardName.Company, Context: self, Identifier: ScreenName.CompanyHome)
                            
                            return
                            
                        }
                        
                        if isPaid.contains(true){
                            
                            
                            SharedDataHelpar.Shared.SetIsPay(IsPay: true)

                            Helpar.Shared.ModelView(storyboard: StoryboardName.Map, Context: self, Identifier: ScreenName.Map)

                            return
                        }
                        
                      Helpar.Shared.ModelView(storyboard: StoryboardName.CarList, Context: self, Identifier: ScreenName.CarList)
                        return

                    }
                }
                if self.User.user_group_id == 3 {
                    //CompanyHome
                    Helpar.Shared.ModelView(storyboard: StoryboardName.Company, Context: self, Identifier: ScreenName.CompanyHome)
                    
                    return
                    
                }

                Helpar.Shared.ModelView(storyboard: StoryboardName.Car, Context: self, Identifier: ScreenName.Cars)
                
            }
        }
        
    }
    
}

//
//  StatusCodeModel.swift
//  inch
//
//  Created by Ahmad Almasri on 12/31/16.
//  Copyright © 2016 Snapics. All rights reserved.
//

import Foundation
class StatusCodeModel:NSObject{
    var Code = 0
    var message = ""
    override init() {
        super.init()
    }
    
    init(JSON:String){
        super.init()
        Helpar.Shared.Parser(JSONString: JSON, Obj: self)
    }

}

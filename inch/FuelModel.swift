//
//  FuelModel.swift
//  inch
//
//  Created by Ahmad Almasri on 1/9/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import Foundation
import RealmSwift
class FuelModel:Object{
    
    dynamic var year = 0
    
    dynamic var id	 = 0
    dynamic var country_id	= 0
    dynamic var  country	 = ""
    dynamic var fuel_type = ""
    
    convenience init(JSON:String){
        self.init()
        Helpar.Shared.Parser(JSONString: JSON, Obj: self)
    }
}

//
//  MapTapped.swift
//  inch
//
//  Created by Ahmad Almasri on 3/25/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import Foundation

extension MapController{
    
   
    
    @IBAction func selectServiceTapped(_ sender:UIButton){
        
        
        if   checkLocation() {
            
            return
        }
        
        for view in serviceView.subviews{
            if !(view is UILabel){
                view.backgroundColor = Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.ServiceColor, alpha: 1)

  
            }
            for subview in view.subviews{
                if  subview is UIImageView {
                    let img = subview as! UIImageView
                    img.image = getServiceImageUnSelected(tag: view.tag)
                    
                }
            }

            if view.tag == sender.tag{
                view.backgroundColor = Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.PrimaryColor, alpha: 1)
                for subview in view.subviews{
                if  subview is UIImageView {
                    let img = subview as! UIImageView
                    img.image = getServiceImageSelected(tag: view.tag)
                    
                }
            }
            }
        }
        
        print(sender.tag)
        
        switch sender.tag {
        case 1:
            ServiceId = 27 // Towing 27
            break
        case 2:
            ServiceId = 31 //  Tire 31
            break
        case 3:
            ServiceId =   28//  loock out 28
            break
        case 4:
            ServiceId = 30 // fule
            break
        case 5:
            ServiceId = 29 //batary
            break
        default:
            break
        }
        
        self.map_view.selectedMarker = originMarker
         map_view.settings.myLocationButton = false
    }
    
    
    func getServiceImageSelected(tag:Int)->UIImage{
        
        switch tag {
        case 1:
            return #imageLiteral(resourceName: "TowingSelected")
        case 2:
            return #imageLiteral(resourceName: "TireSelected")
        case 3:
            return #imageLiteral(resourceName: "lockSelected")
        case 4:
            return #imageLiteral(resourceName: "fuelSelected")
        case 5:
            return #imageLiteral(resourceName: "batterySelected")
        default:
           return #imageLiteral(resourceName: "TowingSelected")
        }
    }
    func getServiceImageUnSelected(tag:Int)->UIImage{
        
        switch tag {
        case 1:
            return #imageLiteral(resourceName: "Towing")
        case 2:
            return #imageLiteral(resourceName: "Tire")
        case 3:
            return #imageLiteral(resourceName: "lock")
        case 4:
            return #imageLiteral(resourceName: "Fuel")
        case 5:
            return #imageLiteral(resourceName: "Battery")
        default:
            return #imageLiteral(resourceName: "Towing")
        }
    }
    
}

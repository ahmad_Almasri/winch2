//
//  ChooseCarManager.swift
//  inch
//
//  Created by Ahmad Almasri on 3/25/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import Foundation
extension ChooseCarController:HttpHelparDelegete{
    
    
    
    func FindCarList(UserId:Int){
        let Param = ["user_id":UserId]
        Http.Get(Url: UrlHelpar.GET_CAR, parameters: Param, Tag: 1)
    }
    func receivedErrorWithMessage(message: String) {
        self.view.dismissLoader()
        Helpar.Logar(msg: message)
    }
    func receivedResponse(dictResponse: NSDictionary, Status: Bool, Tag: Int) {
        
        Helpar.Logar(msg: "\(Helpar.Shared.DictToJson(JSON: dictResponse))")
        self.view.dismissLoader()
        if Tag == 1 {
            
            if Status{
                
                
                if let result =  dictResponse.object(forKey: "result") as? NSArray{
                    
                    for i in 0..<result.count{
                        let JSON = Helpar.Shared.DictToJson(JSON: result[i] as! NSDictionary)
                        
                        let User = CarListModel(JSON: JSON as String)
                        
                        if User.is_active{
                        UserCarList.append(User)
                        }
                        
                    }
                    
                   
                    car_table_view.reloadData()
                    
                }
                
            }
            
            
        }
                
    }
}

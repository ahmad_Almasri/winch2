//
//  SignUpController.swift
//  inch
//
//  Created by Ahmad Almasri on 12/30/16.
//  Copyright © 2016 Snapics. All rights reserved.
//

import Foundation
import UIKit
import DropDown
import ElValidator

class SignUpController:UIViewController,UITextFieldDelegate{
    @IBOutlet weak var create_accout_msg_label:UILabel!
    @IBOutlet weak var couyntryCode: UILabel!
    @IBOutlet weak var user_button:UIButton!
    @IBOutlet weak var service_provider_button:UIButton!
    @IBOutlet weak var country_button:UIButton!
    @IBOutlet weak var full_name_field:TextFieldValidator!
    @IBOutlet weak var national_id_field:TextFieldValidator!
    @IBOutlet weak var phone_number_field:TextFieldValidator!
    @IBOutlet weak var email_address_field:TextFieldValidator!
    @IBOutlet weak var password_field:TextFieldValidator!
    @IBOutlet weak var confirm_send_button:UIButton!
    @IBOutlet weak var next_button:UIButton!
    
    
    
    var validationBlock:((_: [Error]) -> Void)?
    var activeTextField: TextFieldValidator?
    
    
    let Http = HttpHelpar()
    let CountriesDropDown = DropDown()
    var Countries = [String]()
    var CuntryId = 0
    var confirm_send = false
    
    var userGroupId = 2
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Http.delegate = self
        
        service_provider_button.isHidden = true
        
        country_button.BottomLineView(Color: Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.ColorLine, alpha: 1), Size: 1)
        
        full_name_field.BottomLineView(Color: Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.ColorLine, alpha: 1), Size: 1)
        national_id_field.BottomLineView(Color: Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.ColorLine, alpha: 1), Size: 1)
        phone_number_field.BottomLineView(Color: Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.ColorLine, alpha: 1), Size: 1)
        email_address_field.BottomLineView(Color: Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.ColorLine, alpha: 1), Size: 1)
        password_field.BottomLineView(Color: Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.ColorLine, alpha: 1), Size: 1)
        
        country_button.titleLabel?.font = FontHelpar.Regular(Size: 14)
        full_name_field.font = FontHelpar.Regular(Size: 14)
        national_id_field.font = FontHelpar.Regular(Size: 14)
        phone_number_field.font = FontHelpar.Regular(Size: 14)
        email_address_field.font = FontHelpar.Regular(Size: 14)
        password_field.font = FontHelpar.Regular(Size: 14)
        
        create_accout_msg_label.font = FontHelpar.Light(Size: 18)
        user_button.titleLabel?.font = FontHelpar.Semibold(Size: 15)
        service_provider_button.titleLabel?.font = FontHelpar.Semibold(Size: 15)
        confirm_send_button.titleLabel?.font = FontHelpar.Regular(Size: 9)
        next_button.titleLabel?.font = FontHelpar.Regular(Size: 18)
        next_button.roundingUIView(cornerRadiusParam: 2)
        
        Countries =    DBMethod.Shared.GetAll(ofType: CountriesModel.self).map{$0.name_en}
       
        Helpar.Logar(msg: Countries)
        setupDropDown()
        valedationView()
    }
    
    
    
    func valedationView(){
        
        validationBlock = { [weak self] (errors: [Error]) -> Void in
            if let error = errors.first {
                print(error)
                self?.activeTextField?.False()
            } else {
                self?.activeTextField?.True()
            }
        }
        
        email_address_field.delegate = self
        email_address_field.add(validator:PatternValidator(pattern: .mail))
       // email_address_field.add(validator: PatternValidator(customPattern: "[^<>;'[\\]?!{}()--*]*$"))
        email_address_field.validationBlock = validationBlock
       // email_address_field.add(validator: LenghtValidator(validationEvent: .perCharacter, min: 5, max: 20))

        password_field.delegate = self
        password_field.add(validator: LenghtValidator(validationEvent: .perCharacter, min: 6, max: 20))
     //   password_field.add(validator: PatternValidator(customPattern: "[^<>;'[\\]?!{}()--*]*$"))

        password_field.validationBlock = validationBlock
        
        full_name_field.delegate = self
        full_name_field.add(validator: LenghtValidator(validationEvent: .perCharacter, min: 2, max: 50))
        full_name_field.validationBlock = validationBlock
       // full_name_field.add(validator: PatternValidator(customPattern: "[^<>;'[\\]?!{}()--*]*$"))

        
        phone_number_field.delegate = self
        phone_number_field.add(validator:PatternValidator(pattern: .phone))
        phone_number_field.validationBlock = validationBlock
       // phone_number_field.add(validator: PatternValidator(customPattern: "[^<>;'[\\]?!{}()--*]*$"))

        
        phone_number_field.delegate = self
        phone_number_field.add(validator:PatternValidator(pattern: .phone))
        phone_number_field.add(validator: LenghtValidator(validationEvent: .perCharacter, min: 8, max: 20))
        phone_number_field.validationBlock = validationBlock

        
        national_id_field.delegate = self
       // national_id_field.add(validator:PatternValidator(pattern: .alphaNumeric))
//        national_id_field.add(validator: LenghtValidator(validationEvent: .perCharacter, min: 5, max: 50))
        national_id_field.add(validator: PatternValidator(customPattern: "^[a-zA-Z0-9~@#$%^&:,\"]{4,17}$"))
        //~@#$%^&:,
        //       " ~`!@#$%^&()+=-/;:\"\'{}[]<>^?,";
//        ~@#$%^&:^,
        full_name_field.delegate = self
       // full_name_field.add(validator: PatternValidator(customPattern: "[^<>;'[\\]?!{}()--*]*$"))

        national_id_field.validationBlock = validationBlock
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField as? TextFieldValidator
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        activeTextField?.resignFirstResponder()
        activeTextField = nil;
        
        return true
    }

    
    func setupDropDown(){
        CountriesDropDown.anchorView =  country_button
        
        CountriesDropDown.bottomOffset = CGPoint(x: 0, y: country_button.bounds.height)
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        CountriesDropDown.dataSource = Countries
        // Action triggered on selection
        CountriesDropDown.selectionAction = { [unowned self] (index, item) in
            self.country_button.setTitle(item, for: .normal)
           self.CuntryId =  (DBMethod.Shared.GetById(ofType: CountriesModel.self, predicate: "name_en='\(item)'").first!.id)
            
            self.couyntryCode.text = getCountryPhonceCode( (DBMethod.Shared.GetById(ofType: CountriesModel.self, predicate: "name_en='\(item)'").first!.Code))
            Helpar.Logar(msg: self.CuntryId )
             self.country_button.setTitleColor(UIColor.black, for: .normal)
        }
    }
    
    
    @IBAction func userTypeTapped(_ sender:UIButton){
        
        self.userGroupId = sender.tag
        if sender.tag == 3 {
            
            user_button.setImage(#imageLiteral(resourceName: "unSelected"), for: .normal)
            service_provider_button.setImage(#imageLiteral(resourceName: "selected"), for: .normal)
            full_name_field.placeholder = Translation.Shared.GetVal(Key: "companyname")
            national_id_field.placeholder = Translation.Shared.GetVal(Key: "companycr")


        }else{
         
            user_button.setImage(#imageLiteral(resourceName: "selected"), for: .normal)
            service_provider_button.setImage(#imageLiteral(resourceName: "unSelected"), for: .normal)
            full_name_field.placeholder = Translation.Shared.GetVal(Key: "fullname")
            national_id_field.placeholder = Translation.Shared.GetVal(Key: "nationalid")

        }
        
    }
    
    @IBAction func changeCountries(_ sender: UIButton) {
        CountriesDropDown.show()
    }
    
    
    @IBAction func SigUpTapped(sender:UIButton){
        
        
        if CuntryId == 0 {
            
            country_button.shake()
            
            return
        }
        
        if !full_name_field.isValid()  || full_name_field.isSqlInject(){
            
            full_name_field.shake()
            
            return
        }

        if !national_id_field.isValid() || national_id_field.isSqlInject(){
            
            national_id_field.shake()
            
            return
        }
        
        if !phone_number_field.isValid() || phone_number_field.isSqlInject(){
            
            phone_number_field.shake()
            
            return
        }
        
        if !email_address_field.isValid() || email_address_field.isSqlInject(){
            
            email_address_field.shake()
            
            return
        }
        
        if !password_field.isValid()  || password_field.isSqlInject(){
            
            password_field.shake()
            
            return
        }


        
        SignUp()
    }
    
    @IBAction func ConfirmTapped(_ sender:UIButton){
        
        if self.confirm_send{
            self.confirm_send = false
            sender.setImage(#imageLiteral(resourceName: "UnCheckbox"), for: .normal)
        }else{
            self.confirm_send = true
            sender.setImage(#imageLiteral(resourceName: "checkbox"), for: .normal)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        let bar:UINavigationBar! =  self.navigationController?.navigationBar
        
        bar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        bar.shadowImage = UIImage()
        bar.backgroundColor = UIColor(red: 0.0, green: 0.3, blue: 0.5, alpha: 0.0)
        bar.tintColor = Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.PrimaryColor, alpha: 1)
        
        full_name_field.placeholder = Translation.Shared.GetVal(Key: "fullname")
        national_id_field.placeholder = Translation.Shared.GetVal(Key: "nationalid")
        phone_number_field.placeholder = Translation.Shared.GetVal(Key: "phonenumber")
        email_address_field.placeholder = Translation.Shared.GetVal(Key: "emailaddress")
        password_field.placeholder = Translation.Shared.GetVal(Key: "password")
        
        create_accout_msg_label.text = Translation.Shared.GetVal(Key: "createyouraccount").uppercased()
        
        user_button.setTitle(Translation.Shared.GetVal(Key: "user"), for: .normal)
        service_provider_button.setTitle(Translation.Shared.GetVal(Key: "serviceprovider"), for: .normal)
        confirm_send_button.setTitle(Translation.Shared.GetVal(Key: "confirmsendsms/email"), for: .normal)
        next_button.setTitle(Translation.Shared.GetVal(Key: "next").uppercased(), for: .normal)
        country_button.setTitle(Translation.Shared.GetVal(Key: "country"), for: .normal)
        
        
        
        
        
    }
    
    
   
    

    func makePrefix( _ text:String) {
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: NSMakeRange(0,3))
        phone_number_field.attributedText = attributedString
    }
}

//
//  File.swift
//  inch
//
//  Created by Ahmad Almasri on 3/25/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import Foundation

import UIKit
class ChooseCarCell: UITableViewCell {
    @IBOutlet weak var selectedImageView:UIImageView!
    @IBOutlet weak var nameLabel:UILabel!
    @IBOutlet weak var plotNumberLabel:UILabel!
    
}

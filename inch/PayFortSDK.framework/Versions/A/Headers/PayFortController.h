//
//  testController.h
//  PayFortProject
//
//  Created by PayFort on 4/15/16.
//  Copyright © 2016 PayFort. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PayFortDelegate
@required
- (void)sdkResult:(id)response;

@end

@interface PayFortController : UIViewController<UINavigationControllerDelegate>

typedef enum : NSUInteger {
    KPayFortEnviromentSandBox,
    KPayFortEnviromentProduction,
} KPayFortEnviroment;

@property (nonatomic, assign) BOOL IsShowResponsePage;

@property (nonatomic, weak) id <PayFortDelegate> delegate;

- (instancetype)initWithEnviroment:(KPayFortEnviroment)enviroment;
- (void)setPayFortCustomViewNib:(NSString *)customPayFortViewNib;

- (void)setPayFortRequest:(NSMutableDictionary*)RequestFort;
- (void)callPayFort:(id)CurrentPreviousViewController;
- (NSString*)getUDID;

@end

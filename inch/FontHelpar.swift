//
//  FontHelpar.swift
//  inch
//
//  Created by Ahmad Almasri on 12/30/16.
//  Copyright © 2016 Snapics. All rights reserved.
//

import Foundation
import UIKit
class FontHelpar{
   
 private static func Font(Name:String , Size:CGFloat)->UIFont{
        
       return UIFont(name: "SourceSansPro-\(Name)", size: Size)!
    }
    
   static func Black(Size:CGFloat)->UIFont{
    return Font(Name: "Black", Size: Size)
    }
    static func Blacklt(Size:CGFloat)->UIFont{
        return Font(Name: "Blacklt", Size: Size)
    }
    static func Bold(Size:CGFloat)->UIFont{
        return Font(Name: "Bold", Size: Size)
    }
    static func Boldlt(Size:CGFloat)->UIFont{
        return Font(Name: "Boldlt", Size: Size)
    }
    static func ExtraLight(Size:CGFloat)->UIFont{
        return Font(Name: "ExtraLight", Size: Size)
    }
    static func ExtraLightlt(Size:CGFloat)->UIFont{
        return Font(Name: "ExtraLightlt", Size: Size)
    }
    static func lt(Size:CGFloat)->UIFont{
        return Font(Name: "lt", Size: Size)
    }
    static func Light(Size:CGFloat)->UIFont{
        return Font(Name: "Light", Size: Size)
    }
    static func Lightlt(Size:CGFloat)->UIFont{
        return Font(Name: "Lightlt", Size: Size)
    }
    static func Regular(Size:CGFloat)->UIFont{
        return Font(Name: "Regular", Size: Size)
    }
    static func Semibold(Size:CGFloat)->UIFont{
        return Font(Name: "Semibold", Size: Size)
    }
    static func Semiboldlt(Size:CGFloat)->UIFont{
        return Font(Name: "Semiboldlt", Size: Size)
    }
}

//
//  PaymentCarsManager.swift
//  inch
//
//  Created by Ahmad Almasri on 2/8/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import Foundation


extension CarListController:PayFortDelegate{
    
    func endPaymentRequest(SDK_Tocken:String){
        
        
        
        let userCarId =  UserCarPayList.count > 5 ?  UserCarPayList[0..<5].map{"\($0.user_car_id)"} :  UserCarPayList.map{"\($0.user_car_id)"}
        merchant_reference = "w6-"+userCarId.joined(separator: "-")
        DispatchQueue.main.async(){
            self.pay_button.dismissLoader()
            self.LunchPay(merchantReference: self.merchant_reference, sdkTocken: SDK_Tocken)
        }
    }
    
    func errorGetSdkTocken(){
        
        Helpar.Logar(msg: "Error Payment ....")
    }
    
    
    func sdkResult(_ response: Any) {
        Helpar.Logar(msg: response)
        
        if let response = response as? NSDictionary{
            if let result = response.object(forKey: "response_message") as? String{
                
                
                if result == "Success"{
                    
                    //SharedDataHelpar.Shared.SetIsPay(IsPay: true)
                    // Helpar.Shared.ModelView(storyboard: StoryboardName.Map, Context: self, Identifier: ScreenName.Map)
                    
                    if let fort_id = response.object(forKey: "fort_id") as? String{
                        
                        self.fort_id = fort_id
                    }
                    
                    if let token_name = response.object(forKey: "token_name") as? String{
                        
                        self.token_name = token_name
                    }
                    
                    self.addPayment()
                    
                }else{
                    
                    self.view.dismissLoader()
                    
                    Helpar.Shared.Alert(Title: result, Message: nil, BtnOk: Translation.Shared.GetVal(Key: "ok"), Context: self, Actions: {
                        
                    })
                }
                
                return
            }
            
        }
        Helpar.Shared.Alert(Title: Translation.Shared.GetVal(Key: "payment"), Message: Translation.Shared.GetVal(Key: "paymentfaild"), BtnOk: Translation.Shared.GetVal(Key: "ok"), Context: self) {
            
        }
        
        //    {
        //        amount = 1000;
        //        "authorization_code" = 324488;
        //        "card_number" = "400555******0001";
        //        command = PURCHASE;
        //        currency = AED;
        //        "customer_email" = "ahmed2.almasri@ymail.com";
        //        "customer_ip" = "92.241.32.195";
        //        eci = ECOMMERCE;
        //        "expiry_date" = 1705;
        //        "fort_id" = 148680532500032259;
        //        language = en;
        //        "merchant_reference" = "w6-2127-2128-2129";
        //        "payment_option" = VISA;
        //        "response_code" = 14000;
        //        "response_message" = Success;
        //        "sdk_token" = 47DBA89C38C90466E053321E320A49A3;
        //        status = 14;
        //        "token_name" = 47DD37C888FF05E6E053321E320A51F0;
        //        }
        
    }
    
    func LunchPay(merchantReference:String,sdkTocken:String){
        PayFort?.delegate = self
        let request = NSMutableDictionary.init()
        request.setValue("PURCHASE", forKey: "command")
        request.setValue("\(Config.CurrencyCode)", forKey: "currency")
        request.setValue("\(priceCount * 100)", forKey: "amount")
        request.setValue("\(DBMethod.Shared.GetAll(ofType: UserModel.self).first!.email)", forKey: "customer_email")
        request.setValue("", forKey: "installments")
        request.setValue("en", forKey: "language")
        request.setValue(merchantReference, forKey: "merchant_reference")
        request.setValue(sdkTocken, forKey:  "sdk_token")
        request.setValue("", forKey: "payment_option")
        PayFort?.setPayFortRequest(request)
        PayFort?.isShowResponsePage = true
        PayFort?.callPayFort(self)
        
    }
    
    func  startPayment() {
        
        var post = String()
        post += "WinchHelpReqaccess_code=bLSgvSugJtKi19EA4N7h"
        post += "device_id=\(PayFort.getUDID()!)"
        post += "language=en"
        post += "merchant_identifier=bIUyybgQ"
        post += "service_command=SDK_TOKENWinchHelpReq"
        
        let Params =   [
            "service_command" : "SDK_TOKEN",
            "access_code" : "bLSgvSugJtKi19EA4N7h",
            "merchant_identifier" : "bIUyybgQ",
            "language" : "en",
            "signature" : (post.sha256()),
            "device_id" : PayFort.getUDID()!]
        
        
        PostWithString(Url: UrlHelpar.PAYMENT, JSONString: Params as NSDictionary)
    }
    
    
    
    func PostWithString(Url:String,JSONString:NSDictionary){
        
        var request = URLRequest(url: URL(string:  Url)!)
        request.httpMethod = "POST"
        // let JSON = JSONString.data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)
        let JSON: Data? = try? JSONSerialization.data(withJSONObject: JSONString, options: [])
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.httpBody = JSON
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            print("Entered the completionHandler")
            do{
                let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! NSDictionary
                print(json)
                
                if let token = json.object(forKey: "sdk_token") as? String{
                    //  self.SDK_Tocken = token
                    self.endPaymentRequest(SDK_Tocken: token)
                    
                    
                    
                }else{
                    
                    self.errorGetSdkTocken()
                    
                }
                
                //self.LunchPay()
                
                
                
            }catch _ as NSError{
                self.errorGetSdkTocken()
                
            }
            
            }.resume()
        
    }
}

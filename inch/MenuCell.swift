//
//  MenuCell.swift
//  inch
//
//  Created by Ahmad Almasri on 4/8/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import Foundation

import UIKit
class MenuCell:UITableViewCell{
    
    @IBOutlet weak var titleLabel:UILabel!
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        
        
        if titleLabel != nil {
        if selected  && Config.MenuIsEnable{
            self.contentView.backgroundColor = UIColor.white
            titleLabel.textColor = UIColor.black
        }else{
            self.contentView.backgroundColor = UIColor.clear
            titleLabel.textColor = UIColor.white
        }
        }
    }
}

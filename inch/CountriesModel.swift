//
//  CountriesModel.swift
//  inch
//
//  Created by Ahmad Almasri on 12/31/16.
//  Copyright © 2016 Snapics. All rights reserved.
//

import Foundation
import RealmSwift
class CountriesModel:Object{

  dynamic var id = 0
  dynamic var name_en = ""
  dynamic var name_ar = ""
  dynamic  var Code = ""
  dynamic var currency_code  = ""
    
  
   convenience init(JSON:String){
        self.init()
        Helpar.Shared.Parser(JSONString: JSON, Obj: self)
    }
}

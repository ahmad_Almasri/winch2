    //
//  HttpHelpar.swift
//  mp3quran
//
//  Created by Ahmad Almasri on 12/22/16.
//  Copyright © 2016 Snapics. All rights reserved.
//

import Foundation
import Alamofire
protocol HttpHelparDelegete {
    func receivedResponse(dictResponse:NSDictionary,Status:Bool,Tag:Int)
    func receivedErrorWithMessage(message:String)
    
}

class HttpHelpar{
    static var Shared = HttpHelpar()
    
    var delegate: HttpHelparDelegete?
    
    func Upload(Url:String,data:Data,id:Int , Completion:@escaping (AnyObject)->Void){
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(data, withName: "file", fileName: "photos.png", mimeType: "image/png")
            multipartFormData.append("\(id)".data(using: String.Encoding.utf8)!, withName: "image_type_id")

        }, to: Url, encodingCompletion:{ (result) in
            
            switch result {
            case .success(let upload, _, _):
                upload.responseString { response in
                    debugPrint("SUCCESS RESPONSE: \(String(describing: response.response?.statusCode))")
                    
                    
                    
                    
                    
                    let hed = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<string xmlns=\"http://tempuri.org/\">"
                    let bod = "</string>"
                    
                    var dd =  response.value
                    
                    dd = dd?.replacingOccurrences(of: hed, with: "")
                    dd = dd?.replacingOccurrences(of: bod, with: "")
                    
                  //   debugPrint("SUCCESS RESPONSE: \(self.convertToDictionary(text: dd))")]
                    Completion(self.convertToDictionary(text: dd ?? "" ) as AnyObject)

                }
            case .failure(let encodingError):
                print(encodingError)
            }
        })
    }
    
    
    
    func Get(Url:String,parameters:Parameters,Tag:Int){
        Alamofire.request(Url, method: .get,parameters: parameters).responseJSON { (response) in
          
            let hed = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<string xmlns=\"http://tempuri.org/\">"
            let bod = "</string>"
            
            var dd =    String(data: response.data!, encoding: String.Encoding.utf8)   // server data
            
            dd = dd?.replacingOccurrences(of: hed, with: "")
            dd = dd?.replacingOccurrences(of: bod, with: "")
            
            if response.response == nil {
                self.delegate?.receivedErrorWithMessage(message: "No internet connection ")
                
                return

            }
            
            if response.response!.statusCode == 200 {
                if let JSON = dd {
                    Helpar.Logar(msg: "JSON: \(String(describing: self.convertToDictionary(text: JSON)))")
                    
                    var json = self.convertToDictionary(text: JSON)
                    let d = json?.object(forKey: "d") as? NSDictionary
                    
                    if d != nil {
                        json = d!
                    }
                    
                    let StatusCode = json?.object(forKey: "statusCode") as? NSDictionary
                    
                    let Json = Helpar.Shared.DictToJson(JSON: StatusCode!)
                    
                    let StatusObj = StatusCodeModel(JSON: Json as String)
                    
self.delegate?.receivedResponse(dictResponse: json ?? [:] ,Status: StatusObj.Code > 0 , Tag: Tag)
                }
            }else{
               
                self.delegate?.receivedErrorWithMessage(message: String(response.response!.statusCode))
            }
        }
    }
    
    
    
    
    func Post(Url:String,parameters:Parameters,Tag:Int){
        
        
        Helpar.Logar(msg: Url)
        Helpar.Logar(msg: parameters)
        
        
//        let headers: HTTPHeaders = [
//          "Content-Type":"application/json; charset=utf-8"
//            //Optional
//        ]
        
        Alamofire.request(Url, method: .post,parameters: parameters)
            
           //  .validate(contentType: ["application/json"])
            .responseJSON { (response) in
        
            let hed = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<string xmlns=\"http://tempuri.org/\">"
            let bod = "</string>"
            
         var dd =    String(data: response.data!, encoding: String.Encoding.utf8)   // server data
            
            dd = dd?.replacingOccurrences(of: hed, with: "")
            dd = dd?.replacingOccurrences(of: bod, with: "")
        
                
                if response.response == nil {
                    self.delegate?.receivedErrorWithMessage(message: "No internet connection ")
                   return
                }
                

                
            if response.response!.statusCode == 200 {
                if let JSON = dd {
                    
                    Helpar.Logar(msg: "JSON: \(String(describing: self.convertToDictionary(text: JSON)))")
                    
                    var json = self.convertToDictionary(text: JSON)
                    let d = json?.object(forKey: "d") as? NSDictionary
                    
                    if d != nil {
                        json = d!
                    }

                    let StatusCode = json?.object(forKey: "statusCode") as? NSDictionary
                    if StatusCode == nil {
                        self.delegate?.receivedResponse(dictResponse: [:] ,Status: dd?.lowercased() == "Email Sent".lowercased() , Tag: Tag)
                        return
                    }
                    let Json = Helpar.Shared.DictToJson(JSON: StatusCode!)
                    
                    let StatusObj = StatusCodeModel(JSON: Json as String)
                    self.delegate?.receivedResponse(dictResponse: json! ,Status: StatusObj.Code > 0 , Tag: Tag)
                }
            }else{
                
                self.delegate?.receivedErrorWithMessage(message: String(response.response!.statusCode))
            }
        }
    }
    
    
    
    func PostFees(Url:String,parameters:Parameters,Tag:Int){
        
        
        Helpar.Logar(msg: Url)
        Helpar.Logar(msg: parameters)
        
        
        Alamofire.request(Url, method: .post,parameters: parameters)
            
            //  .validate(contentType: ["application/json"])
            .responseJSON { (response) in
                
                let hed = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<string xmlns=\"http://tempuri.org/\">"
                let bod = "</string>"
                
                var dd =    String(data: response.data!, encoding: String.Encoding.utf8)   // server data
                
                dd = dd?.replacingOccurrences(of: hed, with: "")
                dd = dd?.replacingOccurrences(of: bod, with: "")
                
                
                if response.response == nil {
                    self.delegate?.receivedErrorWithMessage(message: "No internet connection ")
                    return
                }
                
                
                
                if response.response!.statusCode == 200 {
                    if let JSON = dd {
                        
                        let dic = ["fees":JSON as String]
                        
                        self.delegate?.receivedResponse(dictResponse: dic as NSDictionary ,Status: false , Tag: Tag)
                    }
                }else{
                    
                    self.delegate?.receivedErrorWithMessage(message: String(response.response!.statusCode))
                }
        }
    }
    
    func PostRequestID(Url:String,parameters:Parameters,Tag:Int){


        Helpar.Logar(msg: Url)
        Helpar.Logar(msg: parameters)

                Alamofire.request(Url, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .responseJSON { response in
                        print(response)
                        
                        
                        do{
                            var json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as? NSDictionary
                            
                            let d = json?.object(forKey: "d") as? String
                            
                            if d != nil {
                                json = self.convertToDictionary(text: d!) ?? [:]
                            }
                            let result = json?.object(forKey: "result") as? NSArray
                            
                            if result != nil {
                                
                                let dicODetails = ["requestID":result?.object(at: 0) as! String] as Dictionary

                                self.delegate?.receivedResponse(dictResponse: dicODetails as NSDictionary ,Status: true, Tag: 1)

                            }else {
                                

                                self.delegate?.receivedErrorWithMessage(message: "erooor")

                            }

                            
                        }catch let error as NSError{
                            self.delegate?.receivedErrorWithMessage(message: error.description)
                            
                        }
                }

    }
    
    
    func PostSMS(Url:String,parameters:Parameters,Tag:Int){
        
        Helpar.Logar(msg: Url)
        Helpar.Logar(msg: parameters)
        
        Alamofire.request(Url, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response)
        }
        
    }
    
    func PostGetFees(Url:String,parameters:Parameters){
        
        Helpar.Logar(msg: Url)
        Helpar.Logar(msg: parameters)
        
        Alamofire.request(Url, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON { response in
                
                do{
                    var json = try JSONSerialization.jsonObject(with: response.data!, options:.allowFragments) as? NSDictionary
                    
                    let d = json?.object(forKey: "d") as? String
                    
                    if d != nil {
                        json = self.convertToDictionary(text: d!) ?? [:]
                    }
                    let result = json?.object(forKey: "result") as? NSArray
                    
                    if result != nil {
                        
                        let dicODetails = ["requestID":result?.object(at: 0) as! String] as Dictionary
                        
                        self.delegate?.receivedResponse(dictResponse: dicODetails as NSDictionary ,Status: true, Tag: 1)
                        
                    }else {
                        
                        
                        self.delegate?.receivedErrorWithMessage(message: "erooor")
                        
                    }
                    
                    
                }catch let error as NSError{
                    self.delegate?.receivedErrorWithMessage(message: error.description)
                    
                }
                
        }
        
    }
    
    
    func PostWithString(Url:String,JSONString:NSString,Tag:Int){
        
        var request = URLRequest(url: URL(string:  Url)!)
        request.httpMethod = "POST"
        let JSON = JSONString.data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.httpBody = JSON
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            print("Entered the completionHandler")
            do{
                var json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as? NSDictionary
                print(json ?? "")
                
                
                let d = json?.object(forKey: "d") as? String
                
                if d != nil {
                    json = self.convertToDictionary(text: d!) ?? [:]
                }
                let StatusCode = json?.object(forKey: "statusCode") as? NSDictionary
                var  Json = ""

                if StatusCode != nil {
                    
                  Json = Helpar.Shared.DictToJson(JSON: StatusCode!) as String
                
                }
                let StatusObj = StatusCodeModel(JSON: Json as String)
                self.delegate?.receivedResponse(dictResponse: json  ?? [:] ,Status: StatusObj.Code > 0 , Tag: Tag)
                
            }catch let error as NSError{
                self.delegate?.receivedErrorWithMessage(message: error.description)
    
            }
            
            }.resume()

    }
    
    func convertToDictionary(text: String) -> NSDictionary? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary as! [String : Any]? as NSDictionary?
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }}

//
//  AddComanyController.swift
//  inch
//
//  Created by Macbook Pro on 9/3/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import UIKit
import PulsingHalo
import SWRevealViewController
class AddComanyController: UIViewController {
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    @IBOutlet weak var titleWinchLabel: UILabel!
    @IBOutlet weak var winchConnectionView: UICollectionView!
    @IBOutlet weak var driverCollectionView: UICollectionView!
    @IBOutlet weak var titleDriverLabel: UILabel!
    
    var halo:PulsingHaloLayer?

    var driverList = [UserModel]()
    var winchList = [WichModel]()
    
    var http = HttpHelpar()
    
    override func viewDidLoad() {
        super.viewDidLoad()
          driverCollectionView.delegate = self
        driverCollectionView.dataSource = self
        winchConnectionView.delegate = self
        winchConnectionView.dataSource = self

        http.delegate = self
        
        customSetup()
    }
    func customSetup() {
        let revealViewController: SWRevealViewController? = self.revealViewController()
        if revealViewController != nil {
            menuButton.target = self.revealViewController()
            revealViewController?.rightViewRevealWidth = (UIScreen.main.bounds.width-40)
            revealViewController?.frontViewShadowOffset = CGSize(width: 0, height: 0) //CGSizeMake(0, 0);
            revealViewController?.frontViewShadowOpacity = 0.0
            revealViewController?.frontViewShadowRadius = 0.0
            Config.MenuIsEnable = SharedDataHelpar.Shared.GetIsPay()
            
            
            menuButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            //  navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    @IBAction func nextTapped(_ sender:UIButton){
        
        Helpar.Shared.PushView(storyboard: StoryboardName.Profile, Context: self, Identifier: ScreenName.ProfileController)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = Translation.Shared.GetVal(Key: "yourservices").uppercased()
        self.view.ShowLoader(Color: self.view.backgroundColor)
         driverList = [UserModel]()
        winchList = [WichModel]()
        getDriver()
        winchConnectionView.reloadData()
    }
}

extension AddComanyController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
  
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.isEqual(winchConnectionView){
            
            return winchList.count + 1
        }
        
        return driverList.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddCell", for: indexPath) as! AddCell
           if collectionView.isEqual(winchConnectionView){
           
            
            if indexPath.row == winchList.count  {
                
                halo = PulsingHaloLayer()
                cell.addButton.isHidden  = false
                cell.addButton.superview?.layer.insertSublayer(halo!, below: cell.addButton.layer)
                halo?.start()
                
                self.halo?.position =  cell.addButton.center
                self.halo?.haloLayerNumber = 6
                self.halo?.radius =  40
                self.halo?.animationDuration = 5.0
                self.halo?.backgroundColor = Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.PrimaryColor, alpha: 1).cgColor
                
                cell.title.text = Translation.Shared.GetVal(Key: "addwinch")
                halo?.isHidden = false
                  cell.image.image = nil
            }else {
                if winchList.count > 0 {
                    
                  //  if halo != nil {
                        
                        halo?.isHidden = true
                    //}
                    
                    cell.addButton.isHidden = true
                    cell.title.text = winchList[indexPath.row].plate_number
                    cell.image.image = #imageLiteral(resourceName: "ic_driver")
                }
            }
            
            return cell

            
            
        }
        
        if indexPath.row == driverList.count  {
         
            halo = PulsingHaloLayer()
           cell.addButton.isHidden  = false
            cell.addButton.superview?.layer.insertSublayer(halo!, below: cell.addButton.layer)
            halo?.start()
            
              self.halo?.position =  cell.addButton.center
            self.halo?.haloLayerNumber = 6
            self.halo?.radius =  40
            self.halo?.animationDuration = 5.0
            self.halo?.backgroundColor = Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.PrimaryColor, alpha: 1).cgColor
            
            cell.title.text = Translation.Shared.GetVal(Key: "adddriver")
            halo?.isHidden = false
            cell.image.image = nil
        }else {
            if driverList.count > 0 {
                halo?.start()

                if halo != nil {
                    
                    halo?.isHidden = true
                }
                
                cell.addButton.isHidden = true
                cell.title.text = driverList[indexPath.row].name_en
                cell.image.image = #imageLiteral(resourceName: "ic_driver")

            }
        }
       
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if (collectionView.isEqual(winchConnectionView)){
            if winchList.count == indexPath.row {
                
                
                Helpar.Shared.PushView(storyboard: StoryboardName.Company, Context: self, Identifier: ScreenName.AddWinch)
                
                return
            }
        
            Helpar.Shared.PushView(storyboard: StoryboardName.Company, Context: self, Identifier: ScreenName.CompanyListController)

            return
        }
        
        if driverList.count == indexPath.row {
            
            
            Helpar.Shared.PushView(storyboard: StoryboardName.Company, Context: self, Identifier: ScreenName.AddDriver)
            
            return
        }
        
         Helpar.Shared.PushView(storyboard: StoryboardName.Company, Context: self, Identifier: ScreenName.DriverList)
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        if collectionView.isEqual(winchConnectionView){
            if winchList.count == 0 {
                let totalCellWidth = 100 * winchList.count
                let totalSpacingWidth = 0 * (winchList.count - 1)
                
                let leftInset = ((UIScreen.main.bounds.width - 16) - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
                let rightInset = leftInset
                
                return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
            }
            return UIEdgeInsetsMake(0, 0,  0, 0)
            
            
        }
        if driverList.count == 0 {
        let totalCellWidth = 100 * driverList.count
        let totalSpacingWidth = 0 * (driverList.count - 1)
        
        let leftInset = ((UIScreen.main.bounds.width - 16) - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset
        
        return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
        }
        return UIEdgeInsetsMake(0, 0,  0, 0)

    }
}

extension AddComanyController:HttpHelparDelegete{
    
    func getDriver(){
        
        let params = ["nLang":"1","nCompany_id":"\(DBMethod.Shared.GetAll(ofType: UserModel.self).first!.iD)","entityId":"0","nCountry":"-1","nStatus":"-1"]
        http.Get(Url: UrlHelpar.GET_DRIVER, parameters: params, Tag: 1)
        
    }
    
    func getWich(){
        
        let params = ["userId":"\(DBMethod.Shared.GetAll(ofType: UserModel.self).first!.iD)"]
        http.Get(Url: UrlHelpar.GET_WINCH, parameters: params, Tag: 2)
        
    }
    func receivedErrorWithMessage(message: String) {
           self.view.dismissLoader()
    }
    
    func receivedResponse(dictResponse: NSDictionary, Status: Bool, Tag: Int) {
        

        if Tag == 2 {
            self.view.dismissLoader()

            
            
            if Status {
                if let result =  dictResponse.object(forKey: "result") as? NSArray{
                    
                    if winchList.count <= 0 {
                        
                        if let layout = driverCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                            layout.scrollDirection = .vertical
                        }
                        self.winchConnectionView.reloadData()
                        
                    }
                
                for res in result {
                    
                    let JSON =  Helpar.Shared.DictToJson(JSON: res as! NSDictionary)
                    let wichModel =  WichModel(JSON: JSON as String)
                    
                    self.winchList.append(wichModel)
                    
                    self.winchConnectionView.reloadData()
                }
                    
                }
                
            }
            
            return 
        }
        
        
        if Status {
            
        if let result =  dictResponse.object(forKey: "result") as? NSArray{
            
            if result.count <= 0 {
                
                if driverList.count <= 0 {
                    
                    if let layout = driverCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                        layout.scrollDirection = .vertical
                    }
                    self.driverCollectionView.reloadData()

                }
            }
            for res in result {
                
                let JSON =  Helpar.Shared.DictToJson(JSON: res as! NSDictionary)
                let driver =  UserModel(JSON: JSON as String)
                
                self.driverList.append(driver)
                
                self.driverCollectionView.reloadData()
            }
            
        }else{
            
            if driverList.count <= 0 {
                
                if let layout = driverCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                    layout.scrollDirection = .vertical
                }
            }

            
            }
        
             getWich()
        }
    }
}

//
//  MenuController.swift
//  inch
//
//  Created by Ahmad Almasri on 4/8/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import UIKit

class MenuController2: UIViewController,UITableViewDelegate,UITableViewDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

  
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return  6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cellIdentifier = ""
        
        switch indexPath.row {
        case 0:
            cellIdentifier = "Empty"
            break
            
        case 1:
            cellIdentifier = "Home"
            break
            
      
            
        case 2:
            cellIdentifier = "Profile"
            break
            
            
        case 3:
            cellIdentifier = "Language"
            break
            
            
//        case 5:
//            cellIdentifier = "Notification"
//            break
//       
            
        case 4:
            cellIdentifier = "Support"
            break
            
        case 5:
            cellIdentifier = "LogOut"
            break
            
        default:
            cellIdentifier = "Empty"
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! MenuCell
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 1 {
            
            selected(StoryboardName.Company, screenName: ScreenName.MyCompany)
   
        }
        
        if indexPath.row == 2  {
           // selected(StoryboardName.MyCar, screenName: ScreenName.MyCar)
            selected(StoryboardName.Profile, screenName: ScreenName.Profile)

        }
        
        if indexPath.row == 3  {
            selected(StoryboardName.Languages, screenName: ScreenName.Languege)

        }
        
        if indexPath.row == 4  {
            
        }
        
        if indexPath.row == 5 {
            
            DBMethod.Shared.Delete(obj: DBMethod.Shared.GetAll(ofType: UserModel.self))
            DBMethod.Shared.Delete(obj: DBMethod.Shared.GetAll(ofType: FuelModel.self))
            
            SharedDataHelpar.Shared.SetIsAdd(IsAdd: false)
            SharedDataHelpar.Shared.SetIsPay(IsPay: false)
            SharedDataHelpar.Shared.SetUserCarId(userCarId: 0)
            SharedDataHelpar.Shared.SetIsOpenMap(IsOpenMap: false)

            
            
            Helpar.Shared.ModelView(storyboard: StoryboardName.Authentication, Context: self, Identifier: ScreenName.Login)
        }

    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == 1 {
           // cell.isSelected = true
        }
        
        
        
    }
    
    
    func selected(_ storyboardName:StoryboardName,screenName:ScreenName){
        
        let storyboard = UIStoryboard(name: storyboardName.rawValue, bundle: nil)
        let rootViewController = storyboard.instantiateViewController(withIdentifier: screenName.rawValue)
        self.revealViewController().setFront(rootViewController, animated: true)
        self.revealViewController().rightRevealToggle(animated: true)
    }
    
}

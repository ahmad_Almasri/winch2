//
//  CacheHelpar.swift
//  inch
//
//  Created by Ahmad Almasri on 1/6/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import Foundation

protocol CacheHelparDelegete {
    func EndCache()
}

class CacheHelpar:HttpHelparDelegete{
    
    var Context:UIViewController?
    
    var Http = HttpHelpar()
    var Delegete:CacheHelparDelegete?
    
    
    func GetCuntry(){
      
        if SharedDataHelpar.Shared.GetIsCache() {
            
            let when = DispatchTime.now() + 0.5 // change 2 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.Delegete?.EndCache()
            }
            
            
        }
        Http.Get(Url: UrlHelpar.COUNTRIES, parameters: ["nLang":"1"], Tag: 1)
    }
    
    func cacheProfile(){
 
        
        self.Http.delegate = self

           if let user = DBMethod.Shared.GetAll(ofType: UserModel.self).first{
            
             let params = ["id":user.iD]
            
            Http.Get(Url: UrlHelpar.GET_PROFILE, parameters: params, Tag: 5)

           }else{
            
            
            GetCuntry()
            
        }
           
        
    
    }
    
    
    
    func GetYear(){
        Http.Get(Url: UrlHelpar.GET_YEAR, parameters: [:], Tag: 3)
        
    }
    
    func GetCarMake(car_type_id:Int){
        self.Http.delegate = self
        let param = ["car_type_id":car_type_id]
        Http.Get(Url: UrlHelpar.CAR_MAKE, parameters: param, Tag: 4)
    }
    
    
    func GetCarModel(){
        self.Http.delegate = self
        Http.Get(Url: UrlHelpar.CAR_MODEL, parameters: [:], Tag: 2)
    }
    

    
    func receivedErrorWithMessage(message: String) {
    Helpar.Logar(msg: message)
    }
    
    func receivedResponse(dictResponse: NSDictionary, Status: Bool, Tag: Int) {
        Helpar.Logar(msg: dictResponse)
        if Tag == 1 {
            
            if Status {
                
                if let result =  dictResponse.object(forKey: "result") as? NSArray{
                    
                    if result.count > 0 {
                        
                     
                        DBMethod.Shared.Delete(obj: DBMethod.Shared.GetAll(ofType: CountriesModel.self))
                        
                    }
                    
                    
                    for i in 0..<result.count{
                        let JSON = Helpar.Shared.DictToJson(JSON: result[i] as! NSDictionary)
                        
                        let  Countries = CountriesModel(JSON: JSON as String)
                    
                        DBMethod.Shared.save(obj: Countries)
                        
                    }
                    
                }
                
                
                
             // GetYear()
            GetCarModel()
            }
        }
        
        if Tag == 2 {
            
            if Status {
                
                
                if let result =  dictResponse.object(forKey: "result") as? NSArray{
                    
                    if result.count > 0 {
                        
                        
                        DBMethod.Shared.Delete(obj: DBMethod.Shared.GetAll(ofType: CarModel.self))
                        DBMethod.Shared.Delete(obj: DBMethod.Shared.GetAll(ofType: MakeModel.self))

                    }
                    
                    for i in 0..<result.count{
                        let JSON = Helpar.Shared.DictToJson(JSON: result[i] as! NSDictionary)
                        
                        let  CarType = CarModel(JSON: JSON as String)
                        
                        if !CarType.make.isEmpty {
                        
                        DBMethod.Shared.save(obj: CarType)
                        
                        GetCarMake(car_type_id: CarType.car_type_id)
                            
                        }
                        
                    }
                    
                }
                
               // self.Delegete?.EndCache()

               GetYear()
                
             // self.Delegete?.EndCache()
                
            }
        }
        
        
        
        if Tag == 3 {
            if Status {
                
                
                if let result =  dictResponse.object(forKey: "result") as? NSArray{
                    
                    if result.count > 0 {
                        
                        
                        DBMethod.Shared.Delete(obj: DBMethod.Shared.GetAll(ofType: YearModel.self))
                        
                    }
                    for i in 0..<result.count{
                        let JSON = Helpar.Shared.DictToJson(JSON: result[i] as! NSDictionary)
                        
                        let Year = YearModel(JSON: JSON as String)
                        
                        DBMethod.Shared.save(obj: Year)
                        
                    }
                    
                }
                
                 self.Delegete?.EndCache()
                
            }
        }
        if Tag == 4 {
            
            if Status {
                
                
                if let result =  dictResponse.object(forKey: "result") as? NSArray{
                    
                  
                    for i in 0..<result.count{
                        let JSON = Helpar.Shared.DictToJson(JSON: result[i] as! NSDictionary)
                        
                        let  CarMake = MakeModel(JSON: JSON as String)
                        
                        if !CarMake.make.isEmpty || !CarMake.model.isEmpty{
                        
                        DBMethod.Shared.save(obj: CarMake)
                        }
                        
                    }
                    
                }
                
                
                //  GetYear()
                
                // self.Delegete?.EndCache()
                
            }
        }
        
        
        if Tag == 5 {
            
            if let result =  dictResponse.object(forKey: "result") as? NSArray{
                
                
                if result.count > 0 {
                    
                    
                    DBMethod.Shared.Delete(obj: DBMethod.Shared.GetAll(ofType: UserModel.self))
                    
                }
                
                
                for i in 0..<result.count{
                    
                    let data = result[i] as! NSDictionary
                    let JSON = Helpar.Shared.DictToJson(JSON: data)
                    
                    let User = UserModel(JSON: JSON as String)
                    
                    if let Id = data.object(forKey: "id") as? Int {
                        
                      let iD =   DBMethod.Shared.update(ofType: User, value: Id as AnyObject, key: "iD")
                    }
                    
                  
                    DBMethod.Shared.Delete(obj: DBMethod.Shared.GetAll(ofType: UserModel.self))
                    
                    DBMethod.Shared.save(obj: User)
                    
                    
                }
                
                
               
                
                
            }
            if SharedDataHelpar.Shared.GetIsCache() {
                
                let when = DispatchTime.now() + 0.5 // change 2 to desired number of seconds
                DispatchQueue.main.asyncAfter(deadline: when) {
                    self.Delegete?.EndCache()
                }
                
                
            }else{
                GetCuntry()
            }
            
            return
        }
        
    }
}

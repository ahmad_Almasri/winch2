//
//  AppDelegate.swift
//  inch
//
//  Created by Ahmad Almasri on 12/29/16.
//  Copyright © 2016 Snapics. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import RealmSwift
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.sharedManager().enable = true
         UINavigationBar.appearance().barTintColor = Helpar.Shared.hexStringToUIColor(hex:  ColorHelpar.NavColor, alpha: 1)

       UINavigationBar.appearance().tintColor = Helpar.Shared.hexStringToUIColor(hex:  ColorHelpar.NavTitleColor, alpha: 1)
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : Helpar.Shared.hexStringToUIColor(hex:  ColorHelpar.NavTitleColor, alpha: 1)]
//
        
        DBMethod.Shared.migration()
        Helpar.Logar(msg:Realm.Configuration.defaultConfiguration.fileURL!)
        GMSServices.provideAPIKey(Config.MAP_API_KEY)
        
     //   DBMethod.Shared.Delete(obj: DBMethod.Shared.GetAll(ofType: UserModel.self))
       // DBMethod.Shared.Delete(obj: DBMethod.Shared.GetAll(ofType: FuelModel.self))
        
       // SharedDataHelpar.Shared.SetIsAdd(IsAdd: false)
        //SharedDataHelpar.Shared.SetIsPay(IsPay: false)
       // SharedDataHelpar.Shared.SetUserCarId(userCarId: 0)
        //SharedDataHelpar.Shared.SetIsOpenMap(IsOpenMap: false)
        
        FirebaseApp.configure()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        
       
    }


}


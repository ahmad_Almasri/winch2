//
//  CarListCell.swift
//  inch
//
//  Created by Ahmad Almasri on 1/10/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
class CarListCell:UITableViewCell{
    @IBOutlet weak var type_label:UILabel!
    @IBOutlet weak var color_label:UILabel!
    @IBOutlet weak var year_label:UILabel!
    @IBOutlet weak var fuel_type_label:UILabel!
    @IBOutlet weak var plate_number_label:UILabel!
    @IBOutlet weak var remove_car_label:UILabel!
    @IBOutlet weak var logoImageView:UIImageView!
    @IBOutlet weak var imageLoader:UIActivityIndicatorView!
    let colorName = DBColorNames()
    
    func FellCell(CarObj:CarListModel){
        //

        let url = UrlHelpar.IMAGES+CarObj.make+".png"
        logoImageView.sd_setImage(with: URL(string:url.addingPercentEscapes(using: String.Encoding.utf8)!)) { (image, error, cache, url) in
            
            self.imageLoader.isHidden = true
           // print(error)
            
        }
        
        type_label.text = "\(CarObj.make.uppercased()) / \(CarObj.model.uppercased())"
        color_label.text = colorName.name(for: Helpar.Shared.hexStringToUIColor(hex: CarObj.car_color, alpha: 1))
        year_label.text = "\(CarObj.year_of_creation)"
        fuel_type_label.text = "\(CarObj.fuel_type)"
        plate_number_label.text = "\(CarObj.plate_number)"
        remove_car_label.text = Translation.Shared.GetVal(Key: "removecar").uppercased()
        remove_car_label.transform = CGAffineTransform(rotationAngle: CGFloat(Double(M_PI * 3/2)));
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
    let  remove_car_labelColor = remove_car_label.backgroundColor
        super.setSelected(selected, animated: animated)
        remove_car_label.backgroundColor = remove_car_labelColor
        if let  footer =   self.viewWithTag(12) {
            footer.backgroundColor = UIColor.white
        }
     //   self.contentView.backgroundColor = UIColor.green
       // self.backgroundColor = UIColor.green
        
    }
    
}

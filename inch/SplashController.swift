//
//  SplashController.swift
//  inch
//
//  Created by Ahmad Almasri on 1/6/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import UIKit
import RealmSwift
import Spring
import Cheetah

class SplashController: UIViewController,CacheHelparDelegete {
    @IBOutlet weak var markerView: UIView!
    
    /// **IBOutlet** Image Logo
    @IBOutlet weak var one_marker_imageview:SpringImageView!
    @IBOutlet weak var tow_marker_imageview:UIImageView!
    @IBOutlet weak var logo_imageview:UIImageView!
    
    /// **Object** from cache engine
    var cache = CacheHelpar()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cache.Delegete=self
        
        
        
        
        
    }
    
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        tow_marker_imageview.cheetah.move(-19, 0).run().completion {
            

            self.markerView.cheetah.move(-40, 0).run().completion({
                self.cache.cacheProfile()

                self.setView(view: self.logo_imageview, hidden: false)
            })

            
        }
       

        
    }
    
    
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: { _ in
            view.isHidden = hidden
        }, completion: nil)
    }
    /**
     It end cache call thes func
     
     ## Important Notes ##
     1. check is login user.
     2. check is pay or not .
     3. check is add car .
     
     */
    func EndCache() {
        SharedDataHelpar.Shared.SetIsCache(IsCache: true)
        
        if DBMethod.Shared.IsEmpty(ofType: UserModel.self) {
            
            Helpar.Shared.ModelView(storyboard: StoryboardName.Authentication, Context: self, Identifier: ScreenName.Login)
            
        }else{
            
            
            let user = DBMethod.Shared.GetAll(ofType: UserModel.self).first!
            
            if user.user_group_id == 3 {
                //CompanyHome
  Helpar.Shared.ModelView(storyboard: StoryboardName.Company, Context: self, Identifier: ScreenName.CompanyHome)                
                return
 
            }
            
            if SharedDataHelpar.Shared.GetIsPay(){
                Helpar.Shared.ModelView(storyboard: StoryboardName.Map, Context: self, Identifier: ScreenName.Map)
            }
            
            if SharedDataHelpar.Shared.GetIsAdd(){
                Helpar.Shared.ModelView(storyboard: StoryboardName.CarList, Context: self, Identifier: ScreenName.CarList)
                
                return
            }
            Helpar.Shared.ModelView(storyboard: StoryboardName.Car, Context: self, Identifier: ScreenName.Cars)
        }
        
    }
    
    
}

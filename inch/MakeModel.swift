//
//  MakeModel.swift
//  inch
//
//  Created by Ahmad Almasri on 4/19/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import Foundation
import RealmSwift

class MakeModel:Object{
    
    dynamic var car_type_id = 0
    dynamic var make = ""
    dynamic var model = ""
    dynamic  var car_details_id =  0
    
    
    convenience init(JSON:String){
        self.init()
        Helpar.Shared.Parser(JSONString: JSON, Obj: self)
    }
}

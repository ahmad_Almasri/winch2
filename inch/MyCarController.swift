//
//  MyCarController.swift
//  inch
//
//  Created by Ahmad Almasri on 4/22/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import UIKit
import PulsingHalo
import SWRevealViewController
class MyCarController: UIViewController {
    
    @IBOutlet weak var car_table_view:UITableView!
    @IBOutlet weak var  menuButton:UIBarButtonItem!
    @IBOutlet weak var add_button:UIButton!

    @IBOutlet weak var addCarLabel: UILabel!
    
    let Http = HttpHelpar()
    
    var halo:PulsingHaloLayer?
    
    var UserCarList = [CarListModel]()
    
    var  PayFort:PayFortController!
    
    
    var merchant_reference = ""
    var merchant_referenceToCheck = ""

    
    var fort_id = ""
    
    var token_name = ""
    
    var priceCount = 1000
    
    var carId = 0 
    override func viewDidLoad() {
        super.viewDidLoad()
        PayFort = PayFortController.init(enviroment: KPayFortEnviromentSandBox)
        Http.delegate = self
        UserCarList = [CarListModel]()
        add_button.roundingUIView(cornerRadiusParam: add_button.frame.size.height/2)
       
        
        customSetup()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.title = Translation.Shared.GetVal(Key: "mycars").uppercased()
        
         halo = PulsingHaloLayer()
        
        addCarLabel.text = Translation.Shared.GetVal(Key: "addnewcar")
        
        add_button.superview?.layer.insertSublayer(halo!, below: add_button.layer)
        halo?.start()
        
        self.halo?.position = self.add_button.center
        self.halo?.haloLayerNumber = 6
        self.halo?.radius = 60
        self.halo?.animationDuration = 5.0
        self.halo?.backgroundColor = Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.PrimaryColor, alpha: 1).cgColor
        self.view.ShowLoader(Color: self.view.backgroundColor)
        UserCarList = [CarListModel]()
        self.FindCarList(UserId: DBMethod.Shared.GetAll(ofType: UserModel.self).first!.iD)
    }

    func customSetup() {
        let revealViewController: SWRevealViewController? = self.revealViewController()
        if revealViewController != nil {
            menuButton.target = self.revealViewController()
            revealViewController?.rightViewRevealWidth = (UIScreen.main.bounds.width-40)
            revealViewController?.frontViewShadowOffset = CGSize(width: 0, height: 0) //CGSizeMake(0, 0);
            revealViewController?.frontViewShadowOpacity = 0.0
            revealViewController?.frontViewShadowRadius = 0.0
            
            
            menuButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            //navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    @IBAction func addCarTapped(_ sender:UIButton){
        
        let storyboard = UIStoryboard.init(name: StoryboardName.Car.rawValue, bundle: nil)
        let pagerCar = storyboard.instantiateViewController(withIdentifier: ScreenName.PagerCar.rawValue)
        as! PagerCarController
        pagerCar.isMyCar = true
        self.navigationController?.pushViewController(pagerCar, animated: true)
       // Helpar.Shared.PushView(storyboard: StoryboardName.Car, Context: self, Identifier: ScreenName.PagerCar)
    }
}

//
//  CarListManager.swift
//  inch
//
//  Created by Ahmad Almasri on 1/10/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import Foundation
extension MyCarController:HttpHelparDelegete{
    
    
    
    func FindCarList(UserId:Int){
        let Param = ["user_id":UserId]
        Http.Get(Url: UrlHelpar.GET_CAR, parameters: Param, Tag: 1)
    }
    
    
    func addPayment(){
        let params = ["merchant_reference":merchant_reference,"fort_id":fort_id,"token_name":token_name]
        Http.Post(Url: UrlHelpar.ADD_CAR_PAYMENT, parameters: params, Tag: 3)
        
    }
    func addActivatePayment(){
        let params = ["isActive":1,"user_car_id":carId]
                Http.Post(Url: UrlHelpar.ADD_CAR_ACTIVATE_PAYMENT, parameters: params, Tag: 6)
        
    }
    
    func receivedErrorWithMessage(message: String) {
        Helpar.Logar(msg: message)
    }
    func receivedResponse(dictResponse: NSDictionary, Status: Bool, Tag: Int) {
        
        
        
        Helpar.Logar(msg: "\(Helpar.Shared.DictToJson(JSON: dictResponse))")
        self.view.dismissLoader()
        if Tag == 1 {
            
            if Status{
           
                
                if let result =  dictResponse.object(forKey: "result") as? NSArray{
                    
                    for i in 0..<result.count{
                        let JSON = Helpar.Shared.DictToJson(JSON: result[i] as! NSDictionary)
                        
                       let User = CarListModel(JSON: JSON as String)
                      //  if !User.is_paid{
                       UserCarList.append(User)
                      //  }
                        
                    }
                    
                    
//                    priceCount = UserCarList.count <  5 ? 150 * UserCarList.count   :  (150 * 5 )
//                    total_price_value_label.text  = "\(priceCount) \(Config.CurrencyCode)"
//                   
//                    if UserCarList.isEmpty{
//                        pay_button.isEnabled = false
//                        pay_button.alpha = 0.5
//                    }
                    car_table_view.reloadData()
                    
                }
                
            }
            
            
        }
        
        if Tag == 3 {
           
            
            SharedDataHelpar.Shared.SetIsPay(IsPay: true)
            UserCarList = [CarListModel]()
            self.viewDidLoad()
            
           // Helpar.Shared.ModelView(storyboard: StoryboardName.Map, Context: self, Identifier: ScreenName.Map)
            
        }
        if(Tag == 4){
            
            if let result =  dictResponse.object(forKey: "result") as? NSArray{
                if(result.count>0){
                    let JSON = Helpar.Shared.DictToJson(JSON: result[0] as! NSDictionary)
                    let User = self.Http.convertToDictionary(text: JSON as String)
                    self.priceCount = User?.value(forKey: "car_fees") as! Int
                    self.startPayment()

                }

                
            }
            

        }
        
        if(Tag == 5){
            let fees =  dictResponse.object(forKey: "fees") as! String
            self.priceCount = Int(fees)!
            self.startPayment()


            
            
        }
        if(Tag == 6){
            self.viewDidLoad()

        }
        
    }
}

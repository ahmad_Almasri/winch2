//
//  ChooseCarLoadTable.swift
//  inch
//
//  Created by Ahmad Almasri on 3/25/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import Foundation
extension ChooseCarController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        Helpar.Shared.emptyTableView(view: tableView, Msg: Translation.Shared.GetVal(Key: "nodatafound"), Count: UserCarList.count)
        return UserCarList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChooseCarCell", for: indexPath) as! ChooseCarCell
        cell.selectionStyle = .none
        if UserCarList.count > 0 {
            
             cell.nameLabel.text = "\(UserCarList[indexPath.row].make.uppercased()) / \(UserCarList[indexPath.row].model.uppercased())"
            
             cell.plotNumberLabel.text =  Translation.Shared.GetVal(Key: "platenumber") + " : " + UserCarList[indexPath.row].plate_number
            
            if UserCarList[indexPath.row].isSelected{
                cell.selectedImageView.image = #imageLiteral(resourceName: "selected")
                
            }else{
                cell.selectedImageView.image = #imageLiteral(resourceName: "unSelected")
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        for i in 0..<UserCarList.count{
            UserCarList[i].isSelected = false
        }
        UserCarList[indexPath.row].isSelected = true
        
        SharedDataHelpar.Shared.SetUserCarId(userCarId: UserCarList[indexPath.row].user_car_id)
        submitButton.isEnabled = true
        car_table_view.reloadData()

    }
    
    
    
   
}

//
//  ColorHelpar.swift
//  inch
//
//  Created by Ahmad Almasri on 12/30/16.
//  Copyright © 2016 Snapics. All rights reserved.
//

import Foundation
class ColorHelpar{
    
    static var ColorLine = "#EFEFF0"
    static var PrimaryColor = "#29b99c"
    static var ColorLineBg = "#B8B8B8"
    static var NavColor = "#ffffff"
    static var NavTitleColor = "#29b99c" //29b99c
    static var NavTitleColorDark = "#145c6e" //29b99c
    static var ServiceColor = "#8C8E8E"
    
}

extension UIColor{
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        
        return NSString(format:"#%06x", rgb) as String
    }
}

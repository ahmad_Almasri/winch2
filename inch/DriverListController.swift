//
//  DriverListController.swift
//  inch
//
//  Created by Macbook Pro on 9/8/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import UIKit
import PulsingHalo
import SWRevealViewController
class DriverListController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var menuButton: UIBarButtonItem!

    @IBOutlet weak var driverListTableView:UITableView!
    @IBOutlet weak var i_agree_the_button:UIButton!
    @IBOutlet weak var terms_and_conditions_label:UILabel!
    @IBOutlet weak var add_button:UIButton!
    @IBOutlet weak var add_new_car_label:UILabel!
    var halo:PulsingHaloLayer?
    var driverList = [UserModel]()
    let http = HttpHelpar()
    var isAgree = true
    func customSetup() {
        let revealViewController: SWRevealViewController? = self.revealViewController()
        if revealViewController != nil {
            menuButton.target = self.revealViewController()
            revealViewController?.rightViewRevealWidth = (UIScreen.main.bounds.width-40)
            revealViewController?.frontViewShadowOffset = CGSize(width: 0, height: 0) //CGSizeMake(0, 0);
            revealViewController?.frontViewShadowOpacity = 0.0
            revealViewController?.frontViewShadowRadius = 0.0
            Config.MenuIsEnable = SharedDataHelpar.Shared.GetIsPay()
            
            
            menuButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            //  navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
customSetup()
       driverListTableView.delegate = self
        driverListTableView.dataSource = self
        http.delegate = self
        
        
          add_button.roundingUIView(cornerRadiusParam: add_button.frame.size.height/2)
        i_agree_the_button.titleLabel?.font = FontHelpar.Regular(Size: 9)
        terms_and_conditions_label.font = FontHelpar.Regular(Size: 9)
        add_new_car_label.font = FontHelpar.Regular(Size: 14)

       

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        halo = PulsingHaloLayer()
        self.view.ShowLoader(Color: self.view.backgroundColor)
        driverList = [UserModel]()
        getDriver()
        add_button.superview?.layer.insertSublayer(halo!, below: add_button.layer)
        halo?.start()
        
        self.halo?.position = self.add_button.center
        self.halo?.haloLayerNumber = 6
        self.halo?.radius = 40
        self.halo?.animationDuration = 5.0
        self.halo?.backgroundColor = Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.PrimaryColor, alpha: 1).cgColor
        
        i_agree_the_button.setTitle(Translation.Shared.GetVal(Key: "iagreethe"), for: .normal)
        terms_and_conditions_label.text = Translation.Shared.GetVal(Key: "termsandconditios")
        add_new_car_label.text = Translation.Shared.GetVal(Key: "adddriver")
        self.navigationItem.title = Translation.Shared.GetVal(Key: "driverteam").uppercased()
    }
    
    @IBAction func addNewTapped(_ sender:UIButton){
     
        Helpar.Shared.PushView(storyboard: StoryboardName.Company, Context: self, Identifier: ScreenName.AddDriver)

    }
    
    @IBAction func iAgreeTapped(_ sender:UIButton){
        isAgree = !isAgree
        if isAgree{
            sender.setImage(#imageLiteral(resourceName: "checkbox"), for: .normal)
            
        }else{
            sender.setImage(#imageLiteral(resourceName: "UnCheckbox"), for: .normal)
           
            
        }
        
    }
    
    func deleteDriverTapped(_ sender:UIGestureRecognizer){

        
        let Param = ["driver_id":driverList[(sender.view?.tag)!].id]
        http.Post(Url: UrlHelpar.DELETE_DRIVER, parameters: Param, Tag: 2)
        self.driverListTableView.beginUpdates()
        self.driverListTableView.deleteRows(at: [IndexPath(item: (sender.view?.tag)!, section: 0)], with: .left)
        self.driverList.remove(at: (sender.view?.tag)!)
        self.driverListTableView.endUpdates()
        self.driverListTableView.reloadData()
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return driverList.count
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DriverListCell") as! DriverListCell
        cell.selectionStyle = .none
         cell.deleteLabel.transform = CGAffineTransform(rotationAngle: CGFloat(Double(M_PI * 3/2)));
        cell.deleteLabel.text = Translation.Shared.GetVal(Key: "removedriver").uppercased()
        cell.driverNameLabel.text = driverList[indexPath.row].name_en
        cell.driverNationalIdLabel.text = driverList[indexPath.row].national_id
        cell.driverPhoneLabel.text = driverList[indexPath.row].phone
        cell.deleteLabel.tag = indexPath.row

        cell.deleteLabel.isUserInteractionEnabled = true
        let removeAction = UITapGestureRecognizer(target: self, action: #selector(self.deleteDriverTapped(_:)))
        cell.deleteLabel.addGestureRecognizer(removeAction)
      AddFottar(cell: cell)
        return cell
    }
    func AddFottar(cell:UIView){
        let separator = UIView(frame: CGRect(x: 0, y: cell.frame.size.height-10, width: UIScreen.main.bounds.width, height: 10))
        separator.backgroundColor = self.view.backgroundColor
        cell.addSubview(separator)
    }
    

}

extension DriverListController:HttpHelparDelegete{
    
    func getDriver(){
        
        let params = ["nLang":"1","nCompany_id":"\(DBMethod.Shared.GetAll(ofType: UserModel.self).first!.iD)","entityId":"0","nCountry":"-1","nStatus":"-1"]
        http.Get(Url: UrlHelpar.GET_DRIVER, parameters: params, Tag: 1)
        
    }
    func receivedErrorWithMessage(message: String) {
        self.view.dismissLoader()
    }
    
    func receivedResponse(dictResponse: NSDictionary, Status: Bool, Tag: Int) {
        
        self.view.dismissLoader()
        
        if Status {
            if let result =  dictResponse.object(forKey: "result") as? NSArray{
                
                if result.count <= 0 {
                    
                    if driverList.count <= 0 {
                        
                        
                    }
                }
                for res in result {
                    
                    let JSON =  Helpar.Shared.DictToJson(JSON: res as! NSDictionary)
                    let driver =  UserModel(JSON: JSON as String)
                    
                    self.driverList.append(driver)
                    
                    self.driverListTableView.reloadData()
                }
                
            }
        }
    }
}

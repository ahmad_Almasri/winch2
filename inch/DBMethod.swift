//
//  DBMethod.swift
//  inch
//
//  Created by Ahmad Almasri on 12/31/16.
//  Copyright © 2016 Snapics. All rights reserved.
//

import Foundation
import RealmSwift
extension Results {
    func toArray<T>(ofType: T.Type) -> [T] {
        var array = [T]()
        for i in 0 ..< count {
            if let result = self[i] as? T {
                array.append(result)
            }
        }
        
        return array
    }
}
class DBMethod{
    static let Shared = DBMethod()
    
    
    func migration(){
        
        Realm.Configuration.defaultConfiguration = Realm.Configuration(
            schemaVersion: 3,
            migrationBlock: { migration, oldSchemaVersion in
                
        })
    }

    
    func update(ofType:Object,value:AnyObject,key:String)->Bool{
        do {
            let realm = try Realm()
            try  realm.write {
                ofType.setValue(value, forKeyPath: key)
            }
            
            return true
        }catch let error as NSError {
            fatalError(error.localizedDescription)
        }
        
        return false
    }
    
    

    
    
    func save(obj:Object) {
        do {
            let realm = try Realm()
            try realm.write {
                realm.add(obj)
            }
        } catch let error as NSError {
            fatalError(error.localizedDescription)
        }
    }
    
    
    func GetAll<T>(ofType: T.Type) -> Results<T> {
        do {
    //        let objects = Realm().objects(SomeObject).toArray(SomeObject) as [SomeObject]

            let realm = try Realm()
            return realm.objects(T.self)
        } catch let error as NSError {
            fatalError(error.localizedDescription)
        }
    }
    
    func IsEmpty<T>(ofType: T.Type) ->Bool {
        do {
         
            let realm = try Realm()
           if realm.objects(T.self as! Object.Type).isEmpty{
            
            
            return true
            }
            return false
        } catch _ as NSError {
            
            return true
           // fatalError(error.localizedDescription)
        }
    }
    
    func Delete<T>(obj:Results<T>){
        
        do {
            let realm = try Realm()
            
            try realm.write {
                realm.delete(obj)
            }

            
        } catch let error as NSError {
            fatalError(error.localizedDescription)
        }
        
    }
    
    
    func GetById<T>(ofType: T.Type,predicate:String) -> Results<T> {
        do {
            
            let realm = try Realm()
            return realm.objects(T.self).filter(predicate)
        } catch let error as NSError {
            fatalError(error.localizedDescription)
        }
    }
    
    func uniq<S : Sequence, T : Hashable>(source: S) -> [T] where S.Iterator.Element == T {
        var buffer = [T]()
        var added = Set<T>()
        for elem in source {
            if !added.contains(elem) {
                buffer.append(elem)
                added.insert(elem)
            }
        }
        return buffer
    }
}

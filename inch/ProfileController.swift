//
//  ProfileController.swift
//  inch
//
//  Created by Ahmad Almasri on 4/21/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import UIKit
import SWRevealViewController
import DropDown
import ElValidator
class ProfileController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var countryButton: UIButton!
    
    @IBOutlet weak var cartitle: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var editButton: UIBarButtonItem!
    @IBOutlet weak var nameTextField: TextFieldValidator!
    @IBOutlet weak var nationalIdTextField: TextFieldValidator!
    @IBOutlet weak var phoneTextFiled: TextFieldValidator!
    @IBOutlet weak var emailTextField: TextFieldValidator!
    @IBOutlet weak var passwordTextField: TextFieldValidator!
    @IBOutlet weak var car_table_view:UITableView!
    @IBOutlet weak var  menuButton:UIBarButtonItem!

    var editCount = 1
    let Http = HttpHelpar()
    var UserCarList = [CarListModel]()
    var User = UserModel()
    let CountriesDropDown = DropDown()
    var Countries = [String]()
    var CuntryId = 0
    
    var validationBlock:((_: [Error]) -> Void)?
    var activeTextField: TextFieldValidator?
    override func viewDidLoad() {
        super.viewDidLoad()

        countryButton.BottomLineView(Color: Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.ColorLine, alpha: 1), Size: 1)
        
        nameTextField.BottomLineView(Color: Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.ColorLine, alpha: 1), Size: 1)
        nationalIdTextField.BottomLineView(Color: Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.ColorLine, alpha: 1), Size: 1)
        phoneTextFiled.BottomLineView(Color: Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.ColorLine, alpha: 1), Size: 1)
        emailTextField.BottomLineView(Color: Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.ColorLine, alpha: 1), Size: 1)
        passwordTextField.BottomLineView(Color: Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.ColorLine, alpha: 1), Size: 1)

        
         Http.delegate = self
        
        self.view.ShowLoader(Color: self.view.backgroundColor)
        
        self.FindCarList(UserId: DBMethod.Shared.GetAll(ofType: UserModel.self).first!.iD)
        self.getProfile(UserId: DBMethod.Shared.GetAll(ofType: UserModel.self).first!.iD)
        Countries =    DBMethod.Shared.GetAll(ofType: CountriesModel.self).map{$0.name_en}
        setupDropDown()
       customSetup()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.title = Translation.Shared.GetVal(Key: "profile").uppercased()
        editButton.title = Translation.Shared.GetVal(Key: "edit")
        cartitle.text = Translation.Shared.GetVal(Key: "payedcar")
    }
    
    
    func valedationView(){
        
        validationBlock = { [weak self] (errors: [Error]) -> Void in
            if let error = errors.first {
                print(error)
                self?.activeTextField?.False()
            } else {
                self?.activeTextField?.True()
            }
        }
        
        emailTextField.delegate = self
        emailTextField.add(validator:PatternValidator(pattern: .mail))
        emailTextField.validationBlock = validationBlock
        
        passwordTextField.delegate = self
        passwordTextField.add(validator: LenghtValidator(validationEvent: .perCharacter, min: 5, max: 20))
        passwordTextField.validationBlock = validationBlock
        
        nameTextField.delegate = self
        nameTextField.add(validator: LenghtValidator(validationEvent: .perCharacter, min: 5, max: 20))
        nameTextField.validationBlock = validationBlock
        
        phoneTextFiled.delegate = self
        phoneTextFiled.add(validator:PatternValidator(pattern: .phone))
        phoneTextFiled.add(validator:PatternValidator(pattern: .phone))
        phoneTextFiled.validationBlock = validationBlock
        
        phoneTextFiled.delegate = self
        phoneTextFiled.add(validator:PatternValidator(pattern: .phone))
        phoneTextFiled.add(validator: LenghtValidator(validationEvent: .perCharacter, min: 10, max: 14))
        phoneTextFiled.validationBlock = validationBlock
        
        nationalIdTextField.delegate = self
        nationalIdTextField.add(validator:PatternValidator(pattern: .numeric))
        nationalIdTextField.add(validator: LenghtValidator(validationEvent: .perCharacter, min: 10, max: 14))
        nationalIdTextField.validationBlock = validationBlock
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField as? TextFieldValidator
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        activeTextField?.resignFirstResponder()
        activeTextField = nil;
        
        return true
    }

    func setupDropDown(){
        CountriesDropDown.anchorView =  countryButton
        
        CountriesDropDown.bottomOffset = CGPoint(x: 0, y: countryButton.bounds.height)
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        CountriesDropDown.dataSource = Countries
        // Action triggered on selection
        CountriesDropDown.selectionAction = { [unowned self] (index, item) in
            self.countryButton.setTitle(item, for: .normal)
            self.CuntryId =  (DBMethod.Shared.GetById(ofType: CountriesModel.self, predicate: "name_en='\(item)'").first!.id)
            Helpar.Logar(msg: self.CuntryId )
            self.countryButton.setTitleColor(UIColor.black, for: .normal)
        }
    }
    @IBAction func changeCountries(_ sender: UIButton) {
        CountriesDropDown.show()
    }
    

    
    @IBAction func editProfileTapped(_ sender:UIBarButtonItem){
        
       
        
        if editCount % 2 == 0 {
            
            
            if CuntryId == 0 {
                
                countryButton.shake()
                
                return
            }
            
            if !nameTextField.isValid(){
                
                nameTextField.shake()
                
                return
            }
            
            if !nationalIdTextField.isValid(){
                
                nationalIdTextField.shake()
                
                return
            }
            
            if !phoneTextFiled.isValid(){
                
                phoneTextFiled.shake()
                
                return
            }
            
            if !emailTextField.isValid(){
                
                emailTextField.shake()
                
                return
            }
            
            if !passwordTextField.isValid(){
                
                passwordTextField.shake()
                
                return
            }
            

            
            sender.title = Translation.Shared.GetVal(Key: "edit")
            
            for view in mainView.subviews {
                view.isUserInteractionEnabled = false
            }
            UpdateProfile(UserId: DBMethod.Shared.GetAll(ofType: UserModel.self).first!.iD)

        }else{
          
            sender.title = Translation.Shared.GetVal(Key: "save")
            
            for view in mainView.subviews {
                
                if view.tag != 55{
                view.isUserInteractionEnabled = true
                }
            }

        }
        
         editCount += 1
    }

    func customSetup() {
        let revealViewController: SWRevealViewController? = self.revealViewController()
        if revealViewController != nil {
            menuButton.target = self.revealViewController()
            revealViewController?.rightViewRevealWidth = (UIScreen.main.bounds.width-40)
            revealViewController?.frontViewShadowOffset = CGSize(width: 0, height: 0) //CGSizeMake(0, 0);
            revealViewController?.frontViewShadowOpacity = 0.0
            revealViewController?.frontViewShadowRadius = 0.0
            
            
            menuButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            //navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
 

}

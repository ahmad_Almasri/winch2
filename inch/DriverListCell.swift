//
//  DriverListCell.swift
//  inch
//
//  Created by Macbook Pro on 9/8/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import Foundation
import UIKit

class DriverListCell:UITableViewCell{
    
    @IBOutlet weak var deleteLabel: UILabel!
    @IBOutlet weak var driverPhoneLabel: UILabel!
    @IBOutlet weak var driverNameLabel: UILabel!
    @IBOutlet weak var driverNationalIdLabel: UILabel!
}

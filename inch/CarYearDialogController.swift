//
//  CarYearDialogController.swift
//  inch
//
//  Created by Ahmad Almasri on 1/9/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import UIKit

class CarYearDialogController: UIViewController,UIPickerViewDelegate ,UIPickerViewDataSource{
    @IBOutlet weak var year_picker_view:UIPickerView!
    @IBOutlet weak var perant_view:UIView!
    @IBOutlet weak var submit_button:UIButton!
    @IBOutlet weak var  title_label:UILabel!
    var YearList = [Int]()
    var year = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        year_picker_view.delegate = self
        year_picker_view.dataSource = self
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.75)
        self.perant_view.roundingUIView(cornerRadiusParam: 4)
        YearList = (DBMethod.Shared.GetAll(ofType: YearModel.self).map{$0.year})
        YearList = YearList.sorted(by: { $0 > $1 })
        title_label.font = FontHelpar.Regular(Size: 15)
        submit_button.titleLabel?.font = FontHelpar.Bold(Size: 15)
       
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        title_label.text = Translation.Shared.GetVal(Key: "choosecreationyear").uppercased()
        
        submit_button.setTitle(Translation.Shared.GetVal(Key: "submit").uppercased(), for: .normal)
        
        year = "\(YearList.first!)"
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return YearList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(YearList[row])"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        Helpar.Logar(msg: YearList[row])
        
        year = "\(YearList[row])"
    }
    
    @IBAction func SubmitTapped(_ sender:UIButton){
        let DataDict:[String: String] = ["year": self.year]
        
        // Post a notification
        NotificationCenter.default.post(name: NSNotification.Name("GetCarType"), object: nil, userInfo: DataDict)
        
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func CancelTapped(sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
}

//
//  UserModel.swift
//  inch
//
//  Created by Ahmad Almasri on 1/8/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import Foundation
import RealmSwift
class UserModel:Object{
    
    dynamic  var iD = 0
    dynamic  var  name_ar	= ""
    dynamic  var  name_en	= ""
    dynamic  var  username	= ""
    dynamic  var  email = ""
    dynamic  var  password	= ""
    dynamic  var  address	= ""
    dynamic  var  phone	= ""
    dynamic var cnt_id = 0
    dynamic var user_group_id = 0
    
    var Country = "" 
    var national_id = ""
    var id = 0
    
    convenience init(JSON:String){
        self.init()
        Helpar.Shared.Parser(JSONString: JSON, Obj: self)
    }
    
    override static func ignoredProperties() -> [String] {
        return ["Country","national_id","id"]
    }
    
}

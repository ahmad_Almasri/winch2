//
//  CarsController.swift
//  inch
//
//  Created by Ahmad Almasri on 12/31/16.
//  Copyright © 2016 Snapics. All rights reserved.
//

import UIKit
import PulsingHalo
import SWRevealViewController
class CarsController: UIViewController {
    @IBOutlet weak var add_button:UIButton!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var agreementLabel:UILabel!
    @IBOutlet weak var codeLabel:UILabel!
    @IBOutlet weak var expiryLabel: UILabel!
    @IBOutlet weak var activateLabel: UILabel!
    @IBOutlet weak var addNewLabel: UILabel!

    var halo:PulsingHaloLayer?
    override func viewDidLoad() {
        super.viewDidLoad()
       
        customSetup()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        let bar:UINavigationBar! =  self.navigationController?.navigationBar
        
        bar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        bar.shadowImage = UIImage()
        bar.backgroundColor = UIColor(red: 0.0, green: 0.3, blue: 0.5, alpha: 0.0)
        bar.tintColor = Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.PrimaryColor, alpha: 1)
        navigationItem.backBarButtonItem?.title = ""
        
        
        
        add_button.roundingUIView(cornerRadiusParam: add_button.frame.width/2)
        self.add_button.backgroundColor = Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.PrimaryColor, alpha: 1)
        halo = PulsingHaloLayer()
        add_button.superview?.layer.insertSublayer(halo!, below: add_button.layer)
        halo?.start()
        
      
        addNewLabel.text = Translation.Shared.GetVal(Key: "addnewcar").uppercased()
        codeLabel.text = "\(Config.Price) \(Config.CurrencyCode) \(Translation.Shared.GetVal(Key: "parcode"))"
        agreementLabel.text = Translation.Shared.GetVal(Key: "aboutpoint")
        expiryLabel.text = "\(Translation.Shared.GetVal(Key: "expirydate")) \(Config.Year) \(Translation.Shared.GetVal(Key: "year"))"
        activateLabel.text = "\(Translation.Shared.GetVal(Key: "activate")) \(Config.Hours) \(Translation.Shared.GetVal(Key: "hours"))"
       
        

    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.halo?.position = self.add_button.center
         self.halo?.haloLayerNumber = 6
        self.halo?.radius = UIScreen.main.bounds.width / 2.5
         self.halo?.animationDuration = 5.0
        self.halo?.backgroundColor = Helpar.Shared.hexStringToUIColor(hex: ColorHelpar.PrimaryColor, alpha: 1).cgColor
    }
    
    @IBAction func AddCarTapped(sender:UIButton){
        Helpar.Shared.PushView(storyboard: StoryboardName.Car, Context: self, Identifier: ScreenName.PagerCar)
    }
    
    func customSetup() {
        let revealViewController: SWRevealViewController? = self.revealViewController()
        if revealViewController != nil {
            menuButton.target = self.revealViewController()
            revealViewController?.rightViewRevealWidth = (UIScreen.main.bounds.width-40)
            revealViewController?.frontViewShadowOffset = CGSize(width: 0, height: 0) //CGSizeMake(0, 0);
            revealViewController?.frontViewShadowOpacity = 0.0
            revealViewController?.frontViewShadowRadius = 0.0
            Config.MenuIsEnable = false

            
            menuButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            //  navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }

}

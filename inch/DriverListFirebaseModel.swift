//
//  DriverListFirebaseModel.swift
//  inch
//
//  Created by alaa  on 12/21/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import UIKit

class DriverListFirebaseModel: NSObject {
    var supportedServices  = ""
    var accept        = 0
    var arrived       = 0
    var companyId     = 0
    var countryId     = 0
    var  dissmis      = 0
    var id            = 0
    var isAvailable   = 0
    var lat           = 0.0
    var lng           = 0.0
    var name          = ""
    var phone         = ""
    var plate         = ""
    var request_id     = 0
    var servicesEnded = 0
    var winchId       = 0
    var winchTypeId   = 0


    
    override init() {
        super.init()
    }
    
    init(JSON:String){
        super.init()
        Helpar.Shared.Parser(JSONString: JSON, Obj: self)
    }
    
}

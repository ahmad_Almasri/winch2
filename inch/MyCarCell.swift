//
//  CarListCell.swift
//  inch
//
//  Created by Ahmad Almasri on 1/10/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
import DateToolsSwift
class MyCarCell:UITableViewCell{
    
    @IBOutlet weak var ActivateNowBtn: UIButton!
    
    @IBOutlet weak var date2Label: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var type_label:UILabel!
    @IBOutlet weak var color_label:UILabel!
    @IBOutlet weak var year_label:UILabel!
    @IBOutlet weak var fuel_type_label:UILabel!
    @IBOutlet weak var plate_number_label:UILabel!
    @IBOutlet weak var remove_car_label:UILabel!
    @IBOutlet weak var paylabel: UILabel!
    @IBOutlet weak var expiryView: UIView!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var expiryLabel: UILabel!
    @IBOutlet weak var payButton: UIButton!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var logoImageView:UIImageView!
    @IBOutlet weak var imageLoader:UIActivityIndicatorView!
    let colorName = DBColorNames()
    
    func FellCell(CarObj:CarListModel){
        //

        let url = UrlHelpar.IMAGES+CarObj.make+".png"
        logoImageView.sd_setImage(with: URL(string:url.addingPercentEscapes(using: String.Encoding.utf8)!)) { (image, error, cache, url) in
            
            self.imageLoader.isHidden = true
           // print(error)
            
        }
        amountLabel.text =   "\(Translation.Shared.GetVal(Key: "amount")) :  \(Config.Price)" + " \(Config.CurrencyCode)"
        type_label.text = "\(CarObj.make.uppercased()) / \(CarObj.model.uppercased())"
        color_label.text = colorName.name(for: Helpar.Shared.hexStringToUIColor(hex: CarObj.car_color, alpha: 1)) + " "  + Translation.Shared.GetVal(Key: "color")
        year_label.text = "\(CarObj.year_of_creation)"
        fuel_type_label.text = Translation.Shared.GetVal(Key: "fueltype")+" : \(CarObj.fuel_type)"
        plate_number_label.text = Translation.Shared.GetVal(Key: "platenumber")+" : \(CarObj.plate_number)"
        
        
        
       
        
        
        if CarObj.is_paid {
          //  amountLabel.isHidden = true
            date2Label.isHidden = false
           expiryView.isHidden = false
            payButton.isHidden = true
            removeButton.isHidden = true
            
            if CarObj.is_active{
                ActivateNowBtn.isHidden = true
                dateLabel.text = "       "
                expiryLabel.text =  "       " 
                    //"\(Translation.Shared.GetVal(Key: "amount")) :  \(Config.Price2)" + " \(Config.CurrencyCode)"
            }else{
                ActivateNowBtn.isHidden = false
                dateLabel.text = "         "
                expiryLabel.text =  "\(Translation.Shared.GetVal(Key: "amount")) :  \(Config.Price2)" + " \(Config.CurrencyCode)"
            }
             
            amountLabel.text = Translation.Shared.GetVal(Key: "expirydate") 
           date2Label.text =   Date(dateString: CarObj.membership_expiry_string, format: "MM/dd/yyyy hh:mm:ss a").format(with: DateFormatter.Style.short)
            
            
        }else{
            amountLabel.isHidden = false
 dateLabel.text = Date(dateString: CarObj.membership_expiry_string, format: "MM/dd/yyyy hh:mm:ss a").format(with: DateFormatter.Style.short)
            expiryLabel.text = Translation.Shared.GetVal(Key: "expirydate")
     
            expiryView.isHidden = true
            date2Label.isHidden = true 
            payButton.isHidden = false
            removeButton.isHidden = false
        }
    }
    
}

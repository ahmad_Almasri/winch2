//
//  WichModel.swift
//  inch
//
//  Created by Macbook Pro on 10/4/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import Foundation
class WichModel:NSObject{
    
    public var status_id  = 0
    public var type_id  = 0
    public var user_id  = 0
    public var plate_number  = ""
    public var id  = 0
    public var plate_image_id  = 0
    public var plate_image_name  = ""
    public var license_image_id  = 0
    public var license_image_name  = ""
    public var insurance_image_id  = 0
    public var insurance_image_name  = ""
    public var insurance_expiry_date  = ""
    public var front_image_id  = 0
    public var front_image_name  = ""
    public var rear_image_id  = 0
    public var rear_image_name  = ""
    public var left_image_id  = 0
    public var left_image_name  = ""
    public var right_image_id  = 0
    public var right_image_name  = ""
    public var status_en  = ""
    public var status_ar  = ""
    public var truck_type_id  = 0
    public var truck_type  = ""
    public var rejection_note  = ""
    public var rejection_reason  = ""
    public var flag_deleted  = false
    public var is_available  = 0
    public var driver_id  = 0
    public var driver_name  = ""
    public var driver_phone  = ""
    
    override init(){
        super.init()
    }
    convenience init(JSON:String){
        self.init()
        Helpar.Shared.Parser(JSONString: JSON, Obj: self)
    }
  
    
    
}

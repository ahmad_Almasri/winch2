//
//  CarListCollection.swift
//  inch
//
//  Created by Ahmad Almasri on 1/10/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import Foundation
extension MyCarController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        Helpar.Shared.emptyTableView(view: tableView, Msg: Translation.Shared.GetVal(Key: "nodatafound"), Count: UserCarList.count)
        return UserCarList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCarCell", for: indexPath) as! MyCarCell
        cell.selectionStyle = .none
        if UserCarList.count > 0 {
        cell.FellCell(CarObj: UserCarList[indexPath.row])
            cell.payButton.tag = indexPath.row
            cell.payButton.addTarget(self, action: #selector(self.payTapped(_:)), for: .touchUpInside)
            
            cell.removeButton.tag = indexPath.row
            cell.removeButton.addTarget(self, action: #selector(self.deleteCar(_:)), for: .touchUpInside)
            
            cell.ActivateNowBtn.tag = indexPath.row
            cell.ActivateNowBtn.addTarget(self, action: #selector(self.payActivateTapped(_:)), for: .touchUpInside)

            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        AddFottar(cell: cell)

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension

    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    
    func AddFottar(cell:UIView){
        let separator = UIView(frame: CGRect(x: 0, y: cell.frame.size.height-10, width: UIScreen.main.bounds.width, height: 10))
        separator.backgroundColor = self.view.backgroundColor
        cell.addSubview(separator)
    }
    
    @IBAction func payTapped(_ sender:UIButton){
        //sender.ShowLoaderSmall(Color:sender.backgroundColor)
        self.view.ShowLoader(Color: self.view.backgroundColor)
        carId = UserCarList[(sender.tag)].user_car_id
        
       self.merchant_reference = "w6-" + "\(carId)"
        let Param = ["country_id":DBMethod.Shared.GetAll(ofType: UserModel.self).first!.cnt_id,"car_model":UserCarList[(sender.tag)].car_details_id]
        Http.Post(Url: UrlHelpar.GET_CARS_FEES, parameters: Param, Tag: 4)
//        self.priceCount = UserCarList[sender.tag].car_service_fees
//        self.startPayment()
    }
    
    @IBAction func payActivateTapped(_ sender:UIButton){
        //sender.ShowLoaderSmall(Color:sender.backgroundColor)
        self.view.ShowLoader(Color: self.view.backgroundColor)
        carId = UserCarList[(sender.tag)].user_car_id
        self.merchant_reference = "w6-" + "\(carId)" + "-a"
        
        let Param = ["cnt_id":1]

        Http.PostFees(Url: UrlHelpar.GET_CARS_ACTIVENOW_FEES, parameters: Param, Tag: 5)
//        self.priceCount = UserCarList[sender.tag].car_service_fees
//        self.startPayment()
    }

    func deleteCar(_ sender:UIButton){
        
        
        let Param = ["user_car_id":UserCarList[(sender.tag)].user_car_id]
        Http.Post(Url: UrlHelpar.DELETE_USER_CARE, parameters: Param, Tag: 2)
        self.car_table_view.beginUpdates()
        self.car_table_view.deleteRows(at: [IndexPath(item: (sender.tag), section: 0)], with: .left)
        self.UserCarList.remove(at: (sender.tag))
        self.car_table_view.endUpdates()
         self.car_table_view.reloadData()
    }
}

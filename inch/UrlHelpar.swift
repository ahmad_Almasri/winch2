//
//  UrlHelpar.swift
//  inch
//
//  Created by Ahmad Almasri on 12/30/16.
//  Copyright © 2016 Snapics. All rights reserved.
//

import Foundation

class UrlHelpar{
    
    static let SERVICE_URL = "https://www.winchhelp.com/api/"
    static let SOKET_URL = "https://212.118.8.109:8888"
    static let PAYMENT = "https://sbpaymentservices.payfort.com/FortAPI/paymentApi"
    static let IMAGES = "https://winchhelp.com/images/cars-logo/"

    
    static let LOGIN = SERVICE_URL + "Login/Login.asmx/UserLogin"
    
    static let COUNTRIES = SERVICE_URL+"winch/countries/countries.asmx/FindAll"
    
    static let SIGN_UP = SERVICE_URL+"Login/Login.asmx/Register"
    
    static let CAR_MODEL = SERVICE_URL+"Winch/Cars/Cars.asmx/FindAllCarMake"
    
    static let CAR_MAKE = SERVICE_URL+"Winch/Cars/Cars.asmx/FindAllCarModelByMake"
    
    static let ADD_CAR = SERVICE_URL+"Winch/UserCars/UserCars.asmx/Insert"
    
    static let GET_YEAR = SERVICE_URL+"winch/cars/cars.asmx/FindAllCreationYears"
    
    static let FIND_FUEL = SERVICE_URL+"Winch/Fuel/Fuel.asmx/FindFuelByCountry"
    
    static let GET_CAR = SERVICE_URL+"Winch/UserCars/UserCars.asmx/FindByUserId"
    
    static let FORGET_PASSWORD = SERVICE_URL+"winch/ResetPassword/ResetPassword.asmx/ResetUserPassword"
    
    static let DELETE_USER_CARE = SERVICE_URL+"Winch/UserCars/UserCars.asmx/DeleteUserCar"
    
    static let ADD_CAR_PAYMENT =  SERVICE_URL+"Winch/UserCars/UserCars.asmx/InsertPayment"
    static let ADD_CAR_ACTIVATE_PAYMENT =  SERVICE_URL+"winch/UserCars/UserCars.asmx/ActivateUserCars"

    
    static let GET_PROFILE = SERVICE_URL+"Winch/Users/Users.asmx/FindByID"
    
    static let GET_CARS_FEES  = SERVICE_URL+"Winch/Cars/Cars.asmx/GetCarsFeesByModel"
    static let GET_CARS_ACTIVENOW_FEES  = SERVICE_URL+"winch/activenow/activenow.asmx/GetFeesByCountry"

    
    static let UPDATE_PROFILE = SERVICE_URL+"Winch/Users/Users.asmx/UpdateUserProfile"
    
    static let ADD_DRIVER = SERVICE_URL + "Winch/Users/Users.asmx/InsertCompDriver"
    
    static let GET_DRIVER = SERVICE_URL + "Winch/Users/Users.asmx/FindAllDrivers"
    static let DELETE_DRIVER = SERVICE_URL + "Winch/Users/Users.asmx/DeleteCompDriver"
    
    static let ADD_WINCH = SERVICE_URL +  "Winch/Trucks/Truck.asmx/Insert"
    
    static let UPLOAD_IMAGE = SERVICE_URL + "Common/Images.asmx/UploadFile"
    
    static let GET_WINCH  = SERVICE_URL + "Winch/Trucks/Truck.asmx/FindByUserId"
    static let DELETE_WINCH = SERVICE_URL + "Winch/Trucks/Truck.asmx/Delete"
    
    static let GetRequestID_WINCH = SERVICE_URL + "Winch/UserRequests/UserRequests.asmx/UserRequestInsert"
    
    static let SendSMSDriverRequest = SERVICE_URL + "Winch/UserRequests/UserRequests.asmx/SentSmSToDriverRequestRecive"
    static let SendSMSDriverDismiss = SERVICE_URL + "Winch/UserRequests/UserRequests.asmx/SentSmSUserDismiss"

    


}


//
//  Translation.swift
//  inch
//
//  Created by Ahmad Almasri on 12/30/16.
//  Copyright © 2016 Snapics. All rights reserved.
//

import Foundation




class Translation:NSObject{
    static var Shared = Translation()

    
 func  GetVal(Key:String)->String {
    
    
        if let path = Bundle.main.path(forResource: SharedDataHelpar.Shared.GetLang() == 0 ? "EN" : "AR", ofType: "json")
        {
            
            do {
                let jsonData : NSData = NSData(contentsOfFile: path)!
                let  allEntries = try JSONSerialization.jsonObject(with: jsonData as Data, options: []) as! NSDictionary
                
                // let quran =  allEntries.object(forKey: "quran") as! NSDictionary
                
                
                //let ayadic = suraList[Id] as! NSDictionary
                // let ayalist = ayadic.object(forKey: "aya") as! NSArray
                
                return allEntries.object(forKey: Key) as? String == nil ? "_"+Key :  allEntries.object(forKey: Key) as! String
                
            }catch _{
                Helpar.Logar(msg: "Translation this key \(Key) not found ")

              return "_"+Key
            }
            
        }
    
      Helpar.Logar(msg: "Translation File Not Found ")
     return "_"+Key
    
    }
    
    
    
}

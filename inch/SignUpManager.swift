//
//  SignUpManager.swift
//  inch
//
//  Created by Ahmad Almasri on 12/31/16.
//  Copyright © 2016 Snapics. All rights reserved.
//

import Foundation
extension SignUpController:HttpHelparDelegete{
    
    
    func SignUp(){
        self.next_button.ShowLoaderSmall(Color: self.next_button.backgroundColor)

        let   JSONString = NSString.localizedStringWithFormat("{\"UsersDetails\":{\"name_en\":\"%@\",\"username\":\"%@\",\"national_id\":\"%@\",\"cr\":\"%@\",\"email\":\"%@\",\"password\":\"%@\",\"user_group_id\":\"%@\",\"company_id\":\"%@\",\"branch_id\":\"%@\",\"address\":\"%@\",\"phone\":\"%@\",\"name_ar\":\"%@\",\"cnt_id\":\"%@\",\"confirm_send\":\"%@\"}}"
            
            , full_name_field.text!,full_name_field.text!,national_id_field.text!,userGroupId == 3 ? national_id_field.text! : "",email_address_field.text!,(password_field.text?.md5())!,"\(userGroupId)","0","0","",phone_number_field.text!,"name","\(self.CuntryId)","\(self.confirm_send)")
        Http.PostWithString(Url: UrlHelpar.SIGN_UP, JSONString: JSONString, Tag: 1)
        
    }
    
    func FindFuel(country_id:Int){
        let Param = ["country_id":country_id]
        Http.Get(Url: UrlHelpar.FIND_FUEL, parameters: Param, Tag: 2)
    }

    func convertToDictionary(text: Data) -> NSDictionary? {
        do {
            return try JSONSerialization.jsonObject(with: text, options: []) as? NSDictionary as! [String : Any]? as NSDictionary?
        } catch {
            print(error.localizedDescription)
        }
        
        return nil
    }
    
    func receivedErrorWithMessage(message: String) {
        Helpar.Logar(msg: message)
        self.next_button.dismissLoader()
        next_button.setTitle(Translation.Shared.GetVal(Key: "next").uppercased(), for: .normal)

    }
    
    func receivedResponse(dictResponse: NSDictionary, Status: Bool, Tag: Int) {
        Helpar.Logar(msg: "\(Helpar.Shared.DictToJson(JSON:dictResponse))")
        self.next_button.dismissLoader()
//        self.next_button.setTitle(Translation.Shared.GetVal(Key: "next").uppercased(), for: .normal)
//        self.next_button.setTitleColor(UIColor.white, for: .normal)
        
        var message = ""
        if Tag == 1 {
            let statusCode = dictResponse.object(forKey: "statusCode") as? NSDictionary
            
            if statusCode != nil {
                
                message =  statusCode?.object(forKey: "message") as! String
            }
            
            
            if Status {
                let result = dictResponse.object(forKey: "result") as? String
                
                if result == Valedation.Exist.rawValue{
                    Helpar.Shared.Alert(Title: Translation.Shared.GetVal(Key: "alert"), Message: message, BtnOk: Translation.Shared.GetVal(Key: "ok"), Context: self, Actions: {
                        self.next_button.dismissLoader()
                        self.next_button.setTitle(Translation.Shared.GetVal(Key: "next").uppercased(), for: .normal)

                    })
                    
                    return
                }
                
                let user = UserModel()
                
                user.iD = Int(result!)!
                
                user.cnt_id = self.CuntryId
                user.email = self.email_address_field.text!
            
                
                 DBMethod.Shared.save(obj: user)

                FindFuel(country_id: self.CuntryId)

  
            }else{
                self.next_button.dismissLoader()
              
                Helpar.Shared.Alert(Title: Translation.Shared.GetVal(Key: "alert"), Message: message, BtnOk: Translation.Shared.GetVal(Key: "ok"), Context: self, Actions: {
                   
                })
                
            }
        }
        
        
        if Tag == 2 {
            
            if Status {
                if let result =  dictResponse.object(forKey: "result") as? NSArray{
                    if result.count > 0 {
                        
                        
                        DBMethod.Shared.Delete(obj: DBMethod.Shared.GetAll(ofType: FuelModel.self))
                        
                    }
                    for i in 0..<result.count{
                        let JSON = Helpar.Shared.DictToJson(JSON: result[i] as! NSDictionary)
                        
                        let  Fuel = FuelModel(JSON: JSON as String)
                        
                        
                        DBMethod.Shared.save(obj: Fuel)
                        
                    }
                   // FindCarList(UserId: User.iD)
                    // Helpar.Shared.ModelView(storyboard: StoryboardName.Car, Context: self, Identifier: ScreenName.Cars)
                    
                    if userGroupId == 3 {
                        
                            //CompanyHome
                            Helpar.Shared.ModelView(storyboard: StoryboardName.Company, Context: self, Identifier: ScreenName.CompanyHome)
                            
                            return
                            
                    
                    }
                    
                    Helpar.Shared.ModelView(storyboard: StoryboardName.Car, Context: self, Identifier: ScreenName.Cars)

                }
            }
        }
        
        
    }
    
    
}

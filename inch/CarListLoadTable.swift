//
//  CarListCollection.swift
//  inch
//
//  Created by Ahmad Almasri on 1/10/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import Foundation
extension CarListController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        Helpar.Shared.emptyTableView(view: tableView, Msg: Translation.Shared.GetVal(Key: "nodatafound").uppercased(), Count: UserCarList.count)
        return UserCarList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CarListCell", for: indexPath) as! CarListCell
      //  cell.selectionStyle = .none
        
        let  view = UIImageView()
       // view.backgroundColor = UIColor.white
        view.image  = UIImage(named:"ted")
        cell.selectedBackgroundView = view
        
        if UserCarList.count > 0 {
        cell.FellCell(CarObj: UserCarList[indexPath.row])
             cell.remove_car_label.isUserInteractionEnabled = true
            let removeAction = UITapGestureRecognizer(target: self, action: #selector(CarListController.deleteCar(_:)))
            cell.remove_car_label.tag = indexPath.row
            cell.remove_car_label.addGestureRecognizer(removeAction)
        }
        AddFottar(cell: cell)
        return cell 
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("select item")
        
        UserCarPayList.append(UserCarList[indexPath.row])
        
        addPrice(isAdd: true )

        
        
        
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        print("deselect itenm")
        
        if let index = UserCarPayList.index(of: UserCarList[indexPath.row]) {
            
            UserCarPayList.remove(at: index)
            addPrice(isAdd: false)
        }
    }
    
    func addPrice(isAdd:Bool ){
        
        
        priceCount = ( isAdd ?  priceCount + Config.Price :  priceCount -   Config.Price )
        
        
        total_price_value_label.text  =  priceCount > 0 ?  "\(priceCount) \(Config.CurrencyCode)" : ""
    
    }
    
    func AddFottar(cell:UIView){
        let separator = UIView(frame: CGRect(x: 0, y: cell.frame.size.height-10, width: UIScreen.main.bounds.width, height: 10))
        separator.backgroundColor = self.view.backgroundColor
        separator.tag = 12
        cell.addSubview(separator)
    }
    
    
    func deleteCar(_ sender:UIGestureRecognizer){
        
        
        let Param = ["user_car_id":UserCarList[(sender.view?.tag)!].user_car_id]
        Http.Post(Url: UrlHelpar.DELETE_USER_CARE, parameters: Param, Tag: 2)
        self.car_table_view.beginUpdates()
        self.car_table_view.deleteRows(at: [IndexPath(item: (sender.view?.tag)!, section: 0)], with: .left)
        self.UserCarList.remove(at: (sender.view?.tag)!)
        self.car_table_view.endUpdates()
         self.car_table_view.reloadData()
        
        
        
        if UserCarList.count > 0 {
        priceCount = UserCarList.count <  5 ? UserCarList[0].car_service_fees * UserCarList.count   :  (UserCarList[0].car_service_fees * 5 )
        total_price_value_label.text  = "\(priceCount) \(Config.CurrencyCode)"
            
        }else{
            total_price_value_label.text  = "\(0) \(Config.CurrencyCode)"
        }
    }
}

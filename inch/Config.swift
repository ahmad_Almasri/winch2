//
//  Config.swift
//  inch
//
//  Created by Ahmad Almasri on 1/23/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import Foundation

func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
    DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
        completion()
    }
}


class Config{
    static let MAP_API_KEY = "AIzaSyDG89RY3NewINmtYcxvA9NNvU-vHK3HJF4"
    static let Price = 150
    static let Price2 = 50
    static let Year = 1
    static let Hours = 48
    
    static let CurrencyCode = getCurrencyCode()
    static var MenuIsEnable = false
     static func getCurrencyId()->Int{

          if let countryId = DBMethod.Shared.GetAll(ofType: UserModel.self).first?.cnt_id{
            
            return countryId
        }
        return 0
    }
    private static func getCurrencyCode()->String{
        
        if let countryId = DBMethod.Shared.GetAll(ofType: UserModel.self).first?.cnt_id{
            
            if let currency_code = DBMethod.Shared.GetById(ofType: CountriesModel.self, predicate: "id=\(countryId)").first?.currency_code{
                
                return  currency_code
                
            }
            
        }
        
        return ""
    }
}

//
//  CarColorDialogController.swift
//  inch
//
//  Created by Ahmad Almasri on 1/6/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import UIKit
import  HSVColorPicker

class CarColorDialogController: UIViewController ,HSVColorPickerDelegate{
    @IBOutlet weak var perant_view:UIView!
    @IBOutlet weak var color_view:UIView!
    @IBOutlet weak var selected_color_view:UIView!
    @IBOutlet weak var submit_button:UIButton!
    @IBOutlet weak var  title_label:UILabel!
    
    var color = "#ffffff"

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.75)
        self.perant_view.roundingUIView(cornerRadiusParam: 4)
        let colorPicker = HSVColorPicker(frame: CGRect(x: 0, y: 0, width: color_view.frame.width, height: color_view.frame.height))
        
        selected_color_view.roundingUIView(cornerRadiusParam: selected_color_view.frame.width/2)
        selected_color_view.AddBordar(Color: UIColor.black, Width: 2)
        colorPicker.delegate = self
        
        self.color_view.addSubview(colorPicker)
        title_label.font = FontHelpar.Regular(Size: 15)
        submit_button.titleLabel?.font = FontHelpar.Bold(Size: 15)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title_label.text = Translation.Shared.GetVal(Key: "choosecarcolor").uppercased()
        submit_button.setTitle(Translation.Shared.GetVal(Key: "submit").uppercased(), for: .normal)

    }
    
    func colorPicker(_ colorPicker: HSVColorPicker!, changedColor color: UIColor!) {
        selected_color_view.backgroundColor = color
        self.color = color.toHexString()
        
    }
    
    
    
    @IBAction func SubmitTapped(_ sender:UIButton){
        
      
        
        let DataDict:[String: String] = ["color": self.color]
        
        // Post a notification
        NotificationCenter.default.post(name: NSNotification.Name("GetCarType"), object: nil, userInfo: DataDict)
        
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func CancelTapped(sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
}

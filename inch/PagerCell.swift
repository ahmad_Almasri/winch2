//
//  PagerCell.swift
//  inch
//
//  Created by Ahmad Almasri on 1/5/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import Foundation
import UIKit
class PagerCell: UICollectionViewCell {
    @IBOutlet weak var icon_view: UIView!
    @IBOutlet weak var icon_image:UIImageView!
    @IBOutlet weak var title_label:UILabel!
}

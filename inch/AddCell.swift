//
//  AddCell.swift
//  inch
//
//  Created by Macbook Pro on 9/3/17.
//  Copyright © 2017 Snapics. All rights reserved.
//

import Foundation
import UIKit

class AddCell:UICollectionViewCell{
    
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var image:UIImageView!
    @IBOutlet weak var title:UILabel!
}
